# Nextflow queuing system (NQS)

NQS is a queuer for Nextflow including both backend and frontend components.

NQS let users submit Nextflow jobs and retrieve the outputs, reports and logs via an API and/or a UI.

## API

A rich API gives programmatic access to the service.
You can access the API doc by visiting `/docs/api` at your NQS server endpoint (ex: http://localhost:8080/api/docs).

## Frontend

Here is a glimpse into what the user interface looks like:

![Job list view](resources/images/job_list_view.png)  
*Job list view*

![Job view](resources/images/job_view.png)  
*Job view*

![Job logs view](resources/images/job_view_logs.png)  
*Job view (logs tab)*

## Documentation

Please consult the [Wiki pages](https://gitlab.com/uit-sfb/nextflow-queuer/-/wikis/home) for more details.