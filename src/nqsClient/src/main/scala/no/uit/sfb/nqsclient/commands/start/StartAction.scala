package no.uit.sfb.nqsclient.commands.start

import akka.actor.ActorSystem
import akka.stream.SystemMaterializer
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.extras.Configuration
import no.uit.sfb.nqsclient.utils.WsClient

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.DurationInt
import scala.jdk.CollectionConverters.IterableHasAsScala
import scala.util.{Failure, Random, Success, Try}
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.circe.syntax._
import no.uit.sfb.nqsclient.logic.{ProcessHolder, ProximityDetector}
import no.uit.sfb.nqsclient.logic.execution.{ProximityExec, RemoteExec}
import no.uit.sfb.nqsshared.jsonapi.{MultipleResponse, SingleResponse}
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.utils.FileUtils
import play.api.libs.ws.DefaultBodyWritables._

import java.nio.file.{Files, Paths}

object StartAction extends LazyLogging {
  implicit protected val ec = ExecutionContext.global

  implicit protected val customConfig: Configuration =
    Configuration.default.withDefaults

  // Create Akka system for thread and streaming management
  implicit protected val system = ActorSystem()
  /*system.scheduler.scheduleWithFixedDelay(100.millis, 5.seconds)(new Runnable {
    override def run(): Unit = println("TTT")
  })*/
  system.registerOnTermination {
    System.exit(0)
  }
  //TODO: add a termination hook to kill process. sys.addShutdownHook{p.destroy()}

  implicit protected val materializer = SystemMaterializer(system).materializer

  protected val downloadTimeout = 1.hour
  protected val uploadTimeout = 1.hour
  implicit protected lazy val ws = WsClient.defaultClient

  protected lazy val pd = new ProximityDetector(ws)
  protected lazy val ph = ProcessHolder

  protected def workDir(c: Config) = {
    Paths.get(c.getString("app.workdir"))
  }

  protected def configDir(c: Config) = {
    Paths.get(c.getString("app.configdir"))
  }

  //Allowed to fail
  protected def detectOrphans(c: Config) = {
    val f = Future.sequence(endpoints(c).map { endpoint =>
      val url =
        s"$endpoint/api/v1/exec/orphans?clientId=${c.getString("app.id")}"
      ws.url(url)
        .addHttpHeaders("Content-Type" -> "application/json")
        .get()
        .map { _ =>
          ()
        }
    })
    Await.ready(f, 30.seconds)
  }

  //Should never fail
  protected def checkCancelled(job: JobManifest): Future[Boolean] = {
    val url = s"${job.server}/api/v1/exec/is-canceled?jobId=${job._id}"
    ws.url(url)
      .get()
      .map { response =>
        if (response.status < 400) {
          response.body == "true"
        } else
          false
      }
      .recover(_ => false)
  }

  protected def destroyJob(job: JobManifest): Future[Unit] = {
    val url =
      s"${job.server}/api/v1/exec/update-state?jobId=${job.jobId}&state=deleted"
    ws.url(url)
      .get()
      .map { _ =>
        ()
      }
  }

  //Note: Async
  protected def markTimeout(job: JobManifest): Unit = {
    val url =
      s"${job.server}/api/v1/exec/update-state?jobId=${job.jobId}&state=failed"
    ws.url(url).get().recover(_ => ())
  }

  protected def endpoints(c: Config) = {
    val tmp = c.getStringList("app.servers")
    //We randomize the endpoint list to not prioritize always the same endpoint
    tmp.asScala.toSeq.sortBy(_ => Random.nextInt())
  }

  //ATTENTION: it is important to not use futures here as to not multithread
  protected def logic(c: Config) = {
    //We read the config each time in order to follow updates
    val workdir = workDir(c)
    val configdir = configDir(c)
    //Delete deleted jobs
    Await.ready(Future.sequence(endpoints(c).map {
      endpoint =>
        val url =
          s"$endpoint/api/v1/exec/deleted?clientId=${c.getString("app.id")}"
        ws.url(url)
          .get()
          .flatMap { response =>
            if (response.status < 400) {
              val toDelete =
                decode[MultipleResponse[JobManifest]](response.body).toOption
                  .map { _.data.map { _.attributes } }
                  .getOrElse(Seq())
              Future.sequence(toDelete.map { jm =>
                FileUtils.deleteDirIfExists(workdir.resolve(jm.jobPath))
                destroyJob(jm)
              })
            } else
              Future.successful(Seq())
          }
    }), 1.minute)
    //Start by asking the permission to pop a job.
    if (ph.isPopAllowed(c, checkCancelled, markTimeout)) {
      endpoints(c)
        .flatMap { endpoint =>
          val url =
            s"$endpoint/api/v1/exec/pop?clientId=${c.getString("app.id")}"
          val f: Future[Option[JobManifest]] = ws.url(url).post("").map {
            response =>
              if (response.status >= 400) {
                logger.warn(
                  s"POST $url returned with status '${response.statusText}' (${response.status})"
                )
                None
              } else if (response.status == 200) {
                decode[SingleResponse[JobManifest]](response.body).toTry match {
                  case Success(job) =>
                    Some(job.data.map {
                      _.attributes
                    }).flatten
                  case Failure(e) =>
                    logger.warn(
                      s"Failed decoding response due to: '${e.getMessage}'\n${response.body}"
                    )
                    None
                }
              } else
                None //Includes 204
          }
          Try {
            Await.result(f, 1.minute).map { job =>
              //We always replace the server endpoint for robustness purpose
              job.copy(server = endpoint)
            }
          }.recover {
            case e =>
              logger.warn(e.getMessage)
              None
          }.get
        }
        .find(_ => true) match {
        case Some(job) =>
          logger.info(s"Received job $job")

          val exec =
            if (pd.isSharedVolume(job.server, workdir)) {
              logger.info(s"Proximity mode")
              new ProximityExec
            } else {
              logger.info(s"Remote mode")
              new RemoteExec
            }
          val (fProc, fRes) = exec(job, workdir, configdir)
          ph.add(job, fProc, fRes)
        case None =>
          Thread.sleep(5.seconds.toMillis)
      }
    } else {
      Thread.sleep(5.seconds.toMillis)
    }
  }

  def apply(cfg: StartConfig, config: Config): Unit = {
    logger.info(s"Servers: ${endpoints(config).mkString(", ")}")
    try {
      val workdir = workDir(config)
      Files.createDirectories(workdir)
      detectOrphans(config)
      logger.info("Starting logic loop")
      while (true) {
        logic(config)
      }
    } catch {
      case e: Throwable =>
        logger.error("Client must stop due to the following error:", e)
    } finally {
      ws.close()
      System.exit(1) //Force exit (attempt to kill all threads)
    }
  }
}
