package no.uit.sfb.nqsclient.commands.test

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.nqsclient.drivers.nextflow.NextflowDriver

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}

object TestAction extends LazyLogging {
  implicit val ec = ExecutionContext.global

  def apply(cfg: TestConfig, config: Config): Unit = {
    val (_, f) = NextflowDriver
      .cmd(
        s"run -ansi-log false ${cfg.pipeline}".split(" ").toIndexedSeq,
        //s"ls erty".split(" ").toIndexedSeq,
        "/tmp"
      )
    Await.result(f, 15.seconds)
  }
}
