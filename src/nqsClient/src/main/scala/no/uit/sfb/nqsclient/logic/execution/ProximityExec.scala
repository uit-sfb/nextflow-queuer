package no.uit.sfb.nqsclient.logic.execution

import akka.stream.Materializer
import no.uit.sfb.nqsclient.drivers.nextflow.NextflowDriver
import no.uit.sfb.nqsshared.com.error.InternalServerErrorError
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.utils.FileUtils
import play.api.libs.ws.StandaloneWSClient

import java.nio.file.Path
import scala.concurrent.Future
import play.api.libs.ws.DefaultBodyWritables._

class ProximityExec(implicit val mat: Materializer, val ws: StandaloneWSClient)
    extends JobExecLike {

  override protected def getInputs(job: JobManifest,
                                   workDir: Path): Future[Unit] =
    Future.successful(())
  override protected def pushFiles(paths: Map[String, Path],
                                   job: JobManifest): Future[Unit] = {
    //Unfortunately cannot use a multi-part here as WS standalone does not implement FilePart
    val fs = paths.map {
      case (key, _) =>
        //Requires 'import play.api.libs.ws.DefaultBodyWritables._'
        val url2 =
          s"${job.server}/api/v1/exec/upload-output?jobId=${job.jobId}&key=$key"
        ws.url(url2)
          .get() //We do not send the file
          .map { r2 =>
            if (r2.status < 400)
              ()
            else
              throw InternalServerErrorError(
                s"GET '$url2' failed with status ${r2.statusText} (${r2.status})."
              )
          }
    }
    Future.sequence(fs).map { _ =>
      ()
    }
  }
}
