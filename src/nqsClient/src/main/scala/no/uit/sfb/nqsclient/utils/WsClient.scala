package no.uit.sfb.nqsclient.utils

import akka.stream.Materializer
import play.api.libs.ws.ahc.StandaloneAhcWSClient

object WsClient {
  // Create the standalone WS client
  // no argument defaults to a AhcWSClientConfig created from
  // "AhcWSClientConfigFactory.forConfig(ConfigFactory.load, this.getClass.getClassLoader)"
  def defaultClient(implicit mat: Materializer) = StandaloneAhcWSClient()(mat)
}
