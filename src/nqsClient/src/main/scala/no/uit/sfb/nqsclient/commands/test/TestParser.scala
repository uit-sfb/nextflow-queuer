package no.uit.sfb.nqsclient.commands.test

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object TestParser extends LazyLogging {

  lazy val builder = OParser.builder[TestConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Run test pipeline."),
      help('h', "help")
        .text("Prints this usage text"),
      opt[String]('p', "pipeline")
        .text("Pipeline URL (default: 'nextflow-io/hello')")
        .action((arg, cfg) => cfg.copy(pipeline = arg))
    )
  }
}
