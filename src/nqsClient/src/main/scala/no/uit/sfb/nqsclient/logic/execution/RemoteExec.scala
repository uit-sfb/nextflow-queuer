package no.uit.sfb.nqsclient.logic.execution

import akka.stream.Materializer
import akka.stream.scaladsl.{FileIO, Sink}
import akka.util.ByteString
import no.uit.sfb.nqsshared.com.error.InternalServerErrorError
import no.uit.sfb.nqsshared.models.{InputFileHandle, JobManifest}
import no.uit.sfb.nqsshared.utils.FileUtils
import play.api.libs.ws.StandaloneWSClient
import play.api.libs.ws.DefaultBodyWritables._

import java.nio.file.{Files, Path}
import scala.concurrent.Future

class RemoteExec(implicit val mat: Materializer, val ws: StandaloneWSClient)
    extends JobExecLike {

  override protected def getInputs(job: JobManifest,
                                   workDir: Path): Future[Unit] = {
    Files.createDirectories(workDir.resolve(job.filesPath))
    Files.createDirectories(workDir.resolve(job.inputsPath))
    //Download all files
    val fs = job.inputs.map {
      case InputFileHandle(k, originUrl) =>
        val url = job.fileUrl(k)
        val dst = workDir.resolve(job.filePath(k))
        if (!Files.isRegularFile(dst)) {
          val sink = FileIO.toPath(dst)
          ws.url(url).withMethod("GET").stream().flatMap { response =>
            response.bodyAsSource
              .runWith(sink)
          }
        } else
          Future
            .successful(()) //If file already exists, we do not do anything (retry)
    }
    Future.sequence(fs).map { _ =>
      ()
    }
  }

  override protected def pushFiles(paths: Map[String, Path],
                                   job: JobManifest): Future[Unit] = {
    //Unfortunately cannot use a multi-part here as WS standalone does not implement FilePart
    val fs = paths.map {
      case (key, p) =>
        val source = FileIO.fromPath(p)
        //Requires 'import play.api.libs.ws.DefaultBodyWritables._'
        val url2 =
          s"${job.server}/api/v1/exec/upload-output?jobId=${job.jobId}&key=$key"
        ws.url(url2)
          .post(source)
          .map { r2 =>
            if (r2.status < 400)
              ()
            else
              throw InternalServerErrorError(
                s"POST '$url2' failed with status ${r2.statusText} (${r2.status})."
              )
          }
    }
    Future.sequence(fs).map { _ =>
      ()
    }
  }
}
