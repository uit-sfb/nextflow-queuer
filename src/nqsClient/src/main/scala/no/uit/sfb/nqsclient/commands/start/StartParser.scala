package no.uit.sfb.nqsclient.commands.start

import com.typesafe.scalalogging.LazyLogging
import scopt.OParser

object StartParser extends LazyLogging {

  lazy val builder = OParser.builder[StartConfig]

  lazy val parser = {
    import builder._
    OParser.sequence(
      head("Start NQS client."),
      help('h', "help")
        .text("Prints this usage text"),
    )
  }
}
