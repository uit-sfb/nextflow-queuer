package no.uit.sfb.nqsclient.logic

import com.typesafe.config.Config
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.utils.Date

import java.time.LocalDateTime
import scala.collection.mutable
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.sys.process.Process

object ProcessHolder {
  protected val processes =
    mutable.Map[JobManifest, (Future[Process], Future[Unit], LocalDateTime)]()

  protected val nbCpu = Runtime.getRuntime.availableProcessors()

  def add(job: JobManifest,
          fProc: Future[Process],
          fRes: Future[Unit]): Unit = {
    processes += (job -> (fProc, fRes, Date.now()))
  }

  //Return true if one more process may be added.
  def isPopAllowed(
    c: Config,
    isCancelled: JobManifest => Future[Boolean],
    markTimeout: JobManifest => Unit
  )(implicit ec: ExecutionContext): Boolean = {
    val maxParallel = {
      val tmp = c.getInt("app.limit.parallel")
      if (tmp < 0)
        nbCpu
      else
        tmp
    }
    //Clean processes that are complete / failed / canceled
    processes.filterInPlace { case (_, (_, fRes, _)) => !fRes.isCompleted }
    //Clean processes that timed out
    val now = Date.now()
    processes.filterInPlace {
      case (job, (_, _, start)) =>
        val timeout = job.spec.timeout
        if (timeout > 0) {
          val timeoutDate = start.plusSeconds(job.spec.timeout)
          if (timeoutDate.isBefore(now)) {
            markTimeout(job)
            false
          } else
            true
        } else
          true
    }
    //Clean canceled jobs
    val fCancel = Future
      .sequence(processes.map {
        case (job, _) =>
          isCancelled(job).map { res =>
            job -> res
          }
      })
      .map { s =>
        val canceled = s.collect { case (job, true) => job }
        canceled.foreach { job =>
          processes.get(job).map {
            case (fProc, _, _) =>
              fProc.map { proc =>
                proc.destroy()
              }
          }
        }
        processes --= canceled
      }
      .map { _ =>
        maxParallel - processes.size > 0
      }
    Await.result(fCancel, 30.seconds)
  }
}
