package no.uit.sfb.nqsclient.logic

import no.uit.sfb.nqsshared.models.JobManifest
import play.api.libs.ws.StandaloneWSClient

import java.nio.file.{Files, Path}
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext}
import scala.util.Random

class ProximityDetector(ws: StandaloneWSClient)(implicit ec: ExecutionContext) {
  //Todo: Use cache
  def isSharedVolume(endpoint: String, workdir: Path): Boolean = {
    val key = Random.alphanumeric.take(10).mkString
    val jm = JobManifest("proximity")
    val p = workdir.resolve(jm.filesPath)
    Files.createDirectories(p)
    //We create a random file...
    Files.createFile(p.resolve(key))
    //... and transmit the key as parameter
    val f = ws.url(s"$endpoint/detect/shared-volume?key=$key").get().map {
      resp =>
        resp.body == "true"
    }
    //We wait for the response from the server (which is simply checking that the file exists using the provided key)
    try {
      Await.result(f, 30.seconds) //We do not return a future as the client is single threaded
    } finally {
      //We can now delete the random file
      Files.deleteIfExists(p.resolve(key))
    }
  }
}
