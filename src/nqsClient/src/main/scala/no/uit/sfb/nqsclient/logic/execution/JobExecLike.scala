package no.uit.sfb.nqsclient.logic.execution

import akka.stream.Materializer
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.nqsclient.drivers.nextflow.NextflowDriver
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.utils.FileUtils
import play.api.libs.ws.StandaloneWSClient

import java.io.PrintWriter
import java.nio.file.{Files, Path, Paths}
import scala.concurrent.Future
import scala.sys.process.Process

trait JobExecLike extends LazyLogging {
  protected def ws: StandaloneWSClient
  protected implicit def mat: Materializer
  protected implicit lazy val ec = mat.executionContext

  protected def updateState(job: JobManifest,
                            newState: String): Future[Unit] = {
    val url =
      s"${job.server}/api/v1/exec/update-state?jobId=${job.jobId}&state=$newState"
    ws.url(url).get().map { response =>
      if (response.status >= 400) {
        logger.warn(
          s"Could not update job state. GET '$url' returned with status '${response.status}'."
        )
        if (response.status == 409)
          throw new Exception(
            s"Inconsistent state for job '${job.jobId}' (may be cause be cancel)."
          )
        else
          throw new Exception(s"Error while updating job '${job.jobId}'")
      } else ()
    }
  }

  protected def getInputs(job: JobManifest, workDir: Path): Future[Unit]

  protected def initialize(job: JobManifest, workDir: Path): Future[Unit] = {
    //No need to update to initializing since it is done by the api when the job is pulled
    //updateState(job, "initializing").flatMap { _ =>
    Files.createDirectories(workDir.resolve(job.outputsPath))
    Files.createDirectories(workDir.resolve(job.reportsPath)) //At least this one is needed
    val pw = new PrintWriter(workDir.resolve(job.configPath).toFile)
    pw.println(job.spec.config)
    pw.close() //PrintWriter is buffered and will silently fail, so no need for try-finally
    if (job.spec.pipeline.isEmpty) { //When pipeline is empty, mainScript contains the content of a script
      val pw2 = new PrintWriter(workDir.resolve(job.mainScriptPath).toFile)
      pw2.println(job.spec.customScript)
      pw2.close() //PrintWriter is buffered and will silently fail, so no need for try-finally
    }
    getInputs(job, workDir)
    //}
  }

  protected def run(job: JobManifest,
                    workDir: Path,
                    configDir: Path): Future[(Process, Future[String])] = {
    updateState(job, "running").map { _ =>
      val command = Seq(
        "-log",
        workDir.resolve(job.logPath).toString,
        "run",
        "-ansi-log",
        "false",
        "-latest",
        "-w",
        Paths.get(job.workPath).getFileName.toString,
        "-c",
        workDir.resolve(job.configPath).toString,
        "-name",
        job.runName,
        "-r",
        job.spec.version
      ) ++ {
        //Config override using project as config name (+ ".config" extension)
        val p = configDir.resolve(s"${job.project}.config").toString
        if (Files.exists(Paths.get(p)))
          Seq("-c", p)
        else
          Seq()
      } ++
        (if (job.resume)
           Seq("-resume")
         else
           Seq()) ++
        (if (job.spec.profiles.nonEmpty)
           Seq("-profile", job.spec.profiles.mkString(","))
         else
           Seq()) ++
        Seq(
          if (job.spec.pipeline.isEmpty) "." else job.spec.pipeline,
          "-with-dag",
          workDir.resolve(job.dagPath).toString,
          "-with-timeline",
          workDir.resolve(job.timelinePath).toString,
          "-with-report",
          workDir.resolve(job.reportPath).toString
        ) ++
        (if (job.spec.collectEvents)
           //Note: For some reason, only complete/failed events show all the trace attributes. And they are all there by default so no need to configure the trace further than that.
           Seq("-with-weblog", s"${job.server}/api/v1/events")
         else Seq())
      logger.info(command.toString)
      NextflowDriver.cmd(command, workDir.resolve(job.jobPath).toString)
    }
  }

  protected def pushFiles(paths: Map[String, Path],
                          job: JobManifest): Future[Unit]

  protected def cleanup(job: JobManifest, workDir: Path): Unit = {
    val (_, f) = NextflowDriver.cmd(
      Seq("clean", "-f"),
      workDir.resolve(job.jobPath).toString
    )
    //Since cached tasks seem to not been cleared, we do it manually
    f.map { _ =>
      FileUtils.deleteDirIfExists(workDir.resolve(job.workPath))
    }
  }

  protected def finalize(job: JobManifest, workDir: Path, failed: Boolean) = {
    updateState(job, "finalizing").flatMap { _ =>
      //Generate trace file
      val traceTemplate =
        """
          |-----
          |
          |> process: $name
          |> exit status: $exit
          |
          |> stdout:
          |$stdout
          |
          |> stderr:
          |$stderr""".stripMargin
      val pw = new PrintWriter(workDir.resolve(job.traceTemplatePath).toFile)
      pw.write(traceTemplate)
      pw.close()
      val f = NextflowDriver
        .toFile(
          Seq(
            "log",
            job.runName,
            "-t",
            workDir.resolve(job.traceTemplatePath).toString
          ),
          workDir.resolve(job.jobPath).toString,
          workDir.resolve(job.tracePath)
        )
        .recover { _ =>
          ()
        } //We do not want finalize to fail
      //Push all output and report files
      f.flatMap { _ =>
        val files: Seq[Path] = {
          val pathsToUpload =
            Seq(
              workDir.resolve(job.outputsPath),
              workDir.resolve(job.reportsPath)
            )
          pathsToUpload.foldLeft(Seq[Path]()) {
            case (acc, p) =>
              acc ++ (if (FileUtils.exists(p))
                        FileUtils.filesUnder(p)
                      else
                        Seq())
          }
        }
        val (toRemove, paths) = files
          .map { p =>
            workDir.resolve(job.filesPath).relativize(p).toString -> p
          }
          .partition {
            case (k, p) =>
              val reg = "reports/\\w+\\.html\\.\\d+".r
              reg.matches(k)
          }
        //We remove rolling report files
        toRemove.foreach {
          case (_, p) =>
            Files.deleteIfExists(p)
        }
        pushFiles(paths.toMap, job)
          .map { _ =>
            failed
          }
          .recover { case e => true }
          .flatMap { isFailed =>
            val newState = if (isFailed) "failed" else "completed"
            if (!isFailed)
              cleanup(job, workDir)
            updateState(job, newState)
          }
      }
    }
  }

  def apply(job: JobManifest,
            workDir: Path,
            configDir: Path): (Future[Process], Future[Unit]) = {
    val fObj = initialize(job, workDir)
      .flatMap { _ =>
        Files.createDirectories(workDir.resolve(job.inputsPath))
        val inputs = FileUtils.filesUnder(workDir.resolve(job.inputsPath))
        logger.info(s"Inputs found:\n${inputs.mkString("\n")}")
        logger.info(s"Executing in ${workDir.resolve(job.jobPath)}")
        run(job, workDir, configDir)
      }
      .map {
        case (process, f) =>
          val fRes = f
            .map { out =>
              logger.info(s"Job '${job._id}' succeeded.")
              false
            }
            .recover {
              case e =>
                logger.warn(s"Job failed with '${e.getMessage}'")
                true
            }
            .flatMap { failed =>
              finalize(job, workDir, failed)
            }
          process -> fRes
      }
    fObj.map { _._1 } -> fObj.flatMap { _._2 }
  }
}
