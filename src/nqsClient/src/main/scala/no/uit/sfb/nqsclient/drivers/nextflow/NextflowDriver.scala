package no.uit.sfb.nqsclient.drivers.nextflow

import com.typesafe.scalalogging.LazyLogging

import java.io.File
import java.nio.file.Path
import scala.concurrent.{ExecutionContext, Future, blocking}
import scala.sys.process.{Process, ProcessLogger}

//Nextflow must be in the PATH
object NextflowDriver extends LazyLogging {

  protected def newLogger(logStdErr: Boolean = true) = {
    new ProcessLogger {
      private val messages = new StringBuilder()
      def lines() = messages.toString()

      def buffer[T](f: => T): T = {
        messages.clear()
        f
      }
      def err(s: => String): Unit = {
        messages.append(s + "\n"); logger.warn(s)
      }
      def out(s: => String): Unit = {
        if (logStdErr) {
          messages.append(s + "\n")
          logger.info(s)
        }
      }
    }
  }

  //For more options, see here: https://www.nextflow.io/docs/latest/cli.html
  //For killing the precess, call destroy()
  def cmd(args: Seq[String], path: String = ".")(
    implicit ec: ExecutionContext
  ): (Process, Future[String]) = {
    import scala.language.reflectiveCalls
    val p = Process("nextflow" +: args, new File(path))
    val pl = newLogger()
    val bgProcess = p.run(pl)
    bgProcess -> Future {
      blocking {
        val res = bgProcess.exitValue() //Blocking
        if (res != 0)
          throw new Exception(s"Process exited with status '$res'.")
        pl.lines()
      }
    }
  }

  def toFile(args: Seq[String], path: String = ".", dst: Path)(
    implicit ec: ExecutionContext
  ): Future[Unit] = {
    val p = Process("nextflow" +: args, new File(path))
    Future {
      blocking {
        (p #> dst.toFile).!!(newLogger(false))
      }
    }
  }
}
