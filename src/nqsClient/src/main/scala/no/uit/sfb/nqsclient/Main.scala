package no.uit.sfb.nqsclient

import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.info.nqsclient.BuildInfo
import no.uit.sfb.nqsclient.commands.test._
import no.uit.sfb.nqsclient.commands.start._
import scopt.OParser

import java.io.File
import java.net.URL
import scala.concurrent.ExecutionContext
import scala.jdk.CollectionConverters.SeqHasAsJava
import scala.util.Try
import scala.util.control.NonFatal

object Main extends App with LazyLogging {
  val name = BuildInfo.name
  val ver = BuildInfo.version
  val gitCommitId = BuildInfo.gitCommit

  implicit val ec = ExecutionContext.global

  try {
    val mainBuilder = OParser.builder[String]

    val parserMain = {
      import mainBuilder._
      OParser.sequence(
        programName(name),
        head(name, ver),
        head(s"Git: $gitCommitId"),
        head(s"Description: NQS Client"),
        head(
          "The configuration file is set with -Dconfig.file. When not provided, the default application.conf found in the classpath is used."
        ),
        head(
          "To override the configuration: -Dattribute.path=value. For arrays, separate with a comma: -Darray.path=v1,v2,v3"
        ),
        head(
          "When using the docker image, it is important to place arrays AFTER '--'."
        ),
        help('h', "help")
          .text("Prints this usage text"),
        version('v', "version")
          .text("Prints the version"),
        cmd("start")
          .action((_, _) => "start")
          .text("Start client."),
        cmd("test")
          .action((_, _) => "test")
          .text("Run test pipeline."),
      )
    }

    val idx = args.indexWhere(!_.startsWith("-D"))

    val dArgs = {
      args
        .take(idx)
        .map { a =>
          val splt = a.drop(2).split("=")
          splt.head -> splt.tail.mkString("=")
        }
        .toMap
    }

    val config = {
      val tmp = dArgs.get("config.file") match {
        case Some(config) =>
          Try {
            ConfigFactory.parseURL(new URL(config))
          }.toOption
            .getOrElse(ConfigFactory.parseFile(new File(config)))
        case None =>
          ConfigFactory.load()
      }
      dArgs.foldLeft(tmp) {
        case (acc, ("config.file", _)) => acc
        case (acc, ("app.servers", v)) =>
          acc.withValue(
            "app.servers",
            ConfigValueFactory.fromIterable(v.split(",").toSeq.asJava)
          )
        case (acc, (k, v)) =>
          acc.withValue(k, ConfigValueFactory.fromAnyRef(v))
      }
    }
    val realArgs = args.drop(idx)
    (
      OParser.parse(parserMain, realArgs.headOption.toSeq, ""),
      if (realArgs.nonEmpty) realArgs.tail else Array("-h")
    ) match {
      case (Some("start"), arg) =>
        OParser.parse(StartParser.parser, arg, StartConfig()) match {
          case Some(cfg) => StartAction(cfg, config)
          case _         => throw new IllegalArgumentException()
        }
      case (Some("test"), arg) =>
        OParser.parse(TestParser.parser, arg, TestConfig()) match {
          case Some(cfg) => TestAction(cfg, config)
          case _         => throw new IllegalArgumentException()
        }
      case _ =>
        // arguments are bad, error message will have been displayed
        throw new IllegalArgumentException(
          "Please run -h to see the available options."
        )
    }
  } catch {
    case NonFatal(e) =>
      logger.error(e.toString, e)
      println(s"Failed with $e")
      sys.exit(1)
  }
}
