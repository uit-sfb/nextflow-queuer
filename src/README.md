# Nextflow queuing system

## Getting started

### Starting the server

- `cd src`
- `sbt "nqsServerJVM / run"`

### Starting the client

- `cd src`
- `sbt "nqsClient / run start"`