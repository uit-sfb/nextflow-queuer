package no.uit.sfb.nqs.frontend.framework.comp

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, ScalaComponent}
import no.uit.sfb.facade.bootstrap.form.{FormControl, FormControlFeedback}

import scala.scalajs.js.{UndefOr, undefined}

/**
  * For some reason, uncontrolled elements are not suitable when working with movable items (controlled elements move properly, while uncontrolled elements get messed-up)
  * See here: https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
  * In the meantime it is possible to use this wrapper essentially implementing the behavior of an uncontrolled FormControl.
  * NOTE: in React onChange is mapped to onInput. Here onChange will be trigger in the HTML normal meaning (i.e. at blurring)
  */
object UncontrolledFormControl {

  case class Props(value: String,
                   onChange: String => Callback,
                   id: UndefOr[String],
                   placeholder: String,
                   disabled: Boolean,
                   validate: Option[String => Option[String]] = None,
                   `type`: String = "",
                   as: String = "input")

  case class State(internalValue: String,
                   error: Option[String] = None,
                   isValidating: Boolean = false) {
    lazy val isValid = false
    lazy val isInvalid = isValidating && error.nonEmpty
  }

  class Backend($ : BackendScope[Props, State]) {

    val update = (v: String) =>
      $.toModStateWithPropsFn.modState {
        case (_, p) =>
          State(v, p.validate.flatMap { _(v) }, p.validate.nonEmpty)
    }

    def render(p: Props, s: State): VdomElement = {
      <.div(
        FormControl(
          id = p.id,
          value = s.internalValue,
          placeholder = p.placeholder,
          disabled = p.disabled,
          onChange = e => update(e.currentTarget.value),
          //onFocus = _ => update(s.internalValue),
          onBlur = e => p.onChange(s.internalValue),
          isValid = s.isValid,
          isInvalid = s.isInvalid,
          `type` = p.`type`,
          as = p.as
        )(),
        if (s.isInvalid)
          FormControlFeedback()(s.error.get)
        else
          <.div()
      )
    }
  }

  private lazy val component = ScalaComponent
    .builder[Props]
    .initialStateFromProps(
      p =>
        State(p.value, p.validate.flatMap { _(p.value) }, p.validate.nonEmpty)
    )
    .renderBackend[Backend]
    .componentDidUpdate(lf => {
      val p = lf.currentProps
      if (p.value != lf.prevProps.value)
        lf.modState(
          _ =>
            State(
              p.value,
              p.validate.flatMap { _(p.value) },
              p.validate.nonEmpty
          )
        )
      else
        Callback.empty
    })
    .build

  def apply(value: String,
            id: UndefOr[String] = undefined,
            onChange: String => Callback,
            placeholder: String = "",
            disabled: Boolean = false,
            validate: Option[String => Option[String]] = None,
            `type`: String = "",
            as: String = "input") =
    component(
      Props(value, onChange, id, placeholder, disabled, validate, `type`, as)
    )
}
