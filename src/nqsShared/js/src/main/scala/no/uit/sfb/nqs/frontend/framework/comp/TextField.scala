package no.uit.sfb.nqs.frontend.framework.comp

import japgolly.scalajs.react.Callback

case class TextField(
  value: String,
  label: String = "",
  column: Int = 0,
  //id: UndefOr[String] = undefined,
  placeholder: String = "",
  info: String = "",
  smallPrint: String = "",
  disabled: Option[String] = None, //None: enabled, Some: disabled and str is displayed as popup (overriding tooltip field)
  onChange: String => Callback = _ => Callback.empty,
  validate: Option[String => Option[String]] = None,
  `type`: String = "",
  as: String = "input"
) extends FieldLike {

  val disabledFlag = disabled.nonEmpty
  val tooltip = info

  val fieldComp = UncontrolledFormControl(
    //id = p.id,
    value = value,
    placeholder = placeholder,
    disabled = disabledFlag,
    onChange = newValue => onChange(newValue),
    validate = validate,
    `type` = `type`,
    as = as
  )

  def apply() = render(label, tooltip, smallPrint)
}
