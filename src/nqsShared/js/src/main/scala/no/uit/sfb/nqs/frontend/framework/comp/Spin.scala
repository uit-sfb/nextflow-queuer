package no.uit.sfb.nqs.frontend.framework.comp

import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.Spinner

object Spin {
  def apply() = {
    <.div(^.textAlign.center, Spinner()())
  }
}
