package no.uit.sfb.nqs.frontend.framework

sealed trait CompStateLike

case object CompStateLoading extends CompStateLike
case object CompStateSuccess extends CompStateLike
case object CompStateFailed extends CompStateLike
