package no.uit.sfb.nqs.frontend.framework.comp

import japgolly.scalajs.react.vdom.html_<^._

case class LabelField(label: String,
                      column: Int = 0,
                      //id: UndefOr[String],
                      disabled: Option[String] = None,
                      info: String = "",
                      smallPrint: String = "")
    extends FieldLike {
  val tooltip = info

  protected val fieldComp = {
    <.div()
  }

  def apply() = render(label, tooltip, smallPrint)
}
