package no.uit.sfb.nqs.frontend.framework.comp

import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.form.{FormLabel, FormText}
import no.uit.sfb.facade.bootstrap.grid.{Col, Row}
import no.uit.sfb.facade.bootstrap.overlay.{OverlayTrigger, Tooltip}
import no.uit.sfb.facade.icon.Glyphicon

trait FieldLike {
  def column: Int //Set to 0 to not use inline mode, otherwise it represents the with of the column
  lazy val inline = column > 0
  protected lazy val labelColumnSize =
    if (!inline)
      12 //We set to 12 so that it will always display the label and the field in separate lines (i.e. not inline)
    else
      column
  protected def helpIcon = Glyphicon("Help")
  protected def fieldComp: VdomElement
  protected def tooltipComp(tooltip: String) = {
    if (tooltip.nonEmpty)
      OverlayTrigger(overlay = Tooltip.text(tooltip))(
        <.span(^.paddingLeft := "5px"),
        helpIcon
      )
    else <.span()
  }

  //For some reason it doesn't work to factorize render method (by defining a PropLike trait)
  def render(label: String, tooltip: String, smallPrint: String): VdomElement =
    Row()(
      FormLabel(column = true, md = labelColumnSize)(
        label,
        tooltipComp(tooltip)
      ),
      Col()(fieldComp, FormText()(smallPrint))
    )
}
