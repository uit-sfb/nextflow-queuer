package no.uit.sfb.nqs.frontend.framework

import japgolly.scalajs.react.component.Scala.BackendScope
import japgolly.scalajs.react.{Callback, CallbackTo, Reusable, ~=>}
import no.uit.sfb.nqs.frontend.framework.comp.{ErrorToast, ToastLike}
import no.uit.sfb.nqsshared.com.error.ErrorLike

//Re-implement as top component using Children
//if you're using renderBackend then just add a PropsChildren argument to your render method. If you're using render_P or whatever then add a C to it like render_PC. If you're using just render($ => ... then you'll find it on $.propsChildren

trait ToasterStateLike {
  def state: CompStateLike
  def toasts: Seq[ToastLike]
  final def addToast(newToast: ToastLike): Seq[ToastLike] =
    if (toasts.exists(_.id == newToast.id)) toasts else newToast +: toasts
  final def removeToast(rmToast: String): Seq[ToastLike] = toasts.filter {
    _.id != rmToast
  }
  def newState(st: CompStateLike, ts: Seq[ToastLike]): this.type
}

trait ToasterLike[P, S <: ToasterStateLike] {
  def $ : BackendScope[P, S]

  lazy val loading: Callback = {
    $.modStateOption { s =>
      if (s.state == CompStateLoading)
        None
      else
        Some(s.newState(CompStateLoading, s.toasts))
    }
  }

  lazy val success: Callback = {
    $.modStateOption { s =>
      if (s.state == CompStateSuccess)
        None
      else
        Some(s.newState(CompStateSuccess, s.toasts))
    }
  }

  def error(fatal: Boolean = false) = Reusable.fn(
    (err: Throwable) =>
      $.modState(s => {
        val toast = err match {
          case e: ErrorLike =>
            ErrorToast(e.title, e.details)
          case e =>
            ErrorToast(
              "Oops, something went wrong.",
              e.getMessage //"Please retry in a few seconds and let us know if the problem persists."
            )
        }
        if (fatal)
          s.newState(CompStateFailed, s.addToast(toast))
        else
          s.newState(s.state, s.addToast(toast))
      })
  )

  lazy val addToast: ToastLike ~=> Callback = Reusable.fn(
    (toast: ToastLike) =>
      $.modState(s => s.newState(s.state, s.addToast(toast)))
  )

  lazy val deleteToast = Reusable.fn((toastId: String) => {
    $.modState(s => {
      s.newState(s.state, s.removeToast(toastId))
    })
  })
}
