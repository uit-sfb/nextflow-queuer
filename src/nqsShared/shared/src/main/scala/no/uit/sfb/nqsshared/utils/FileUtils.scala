package no.uit.sfb.nqsshared.utils

import java.io.IOException
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{
  FileVisitResult,
  Files,
  LinkOption,
  NoSuchFileException,
  Path,
  SimpleFileVisitor
}
import scala.jdk.CollectionConverters.IterableHasAsScala
import scala.util.{Failure, Success, Try}

object FileUtils {

  def exists(path: Path, followSymbolicLink: Boolean = true): Boolean = {
    //We use a random method that do throw IOException (in contrary to exists, notExists, isDirectory, isRegularFile, ...)
    val options =
      if (followSymbolicLink)
        Seq()
      else
        Seq(LinkOption.NOFOLLOW_LINKS)
    Try { Files.getLastModifiedTime(path, options: _*) } match {
      case Success(_)                                        => true
      case Failure(e) if e.isInstanceOf[NoSuchFileException] => false
      case Failure(e)                                        => throw e
    }
  }

  /**
    * Exists and is a file
    * Throws NoSuchFileException if not exist
    */
  def isFile(path: Path, followSymbolicLink: Boolean = true): Boolean = {
    //Do not use isRegularFile directly because it would not throw IOException
    val options =
      if (followSymbolicLink)
        Seq()
      else
        Seq(LinkOption.NOFOLLOW_LINKS)
    Files.getAttribute(path, "isRegularFile", options: _*).asInstanceOf[Boolean]
  }

  /**
    * Exists and is a symbolic link
    * Throws NoSuchFileException if not exist
    */
  def isSymbolicLink(path: Path): Boolean = {
    //Do not use isSymbolicLink directly because it would not throw IOException
    //We need to use LinkOption.NOFOLLOW_LINKS otherwise getAttribute resolves the symbolic link and always answers negatively!
    Files
      .getAttribute(path, "isSymbolicLink", LinkOption.NOFOLLOW_LINKS)
      .asInstanceOf[Boolean]
  }

  /**
    * Exists and is a directory
    * Throws NoSuchFileException if not exist
    */
  def isDirectory(path: Path, followSymbolicLink: Boolean = true): Boolean = {
    //Do not use isDirectory directly because it would not throw IOException
    val options =
      if (followSymbolicLink)
        Seq()
      else
        Seq(LinkOption.NOFOLLOW_LINKS)
    Files.getAttribute(path, "isDirectory", options: _*).asInstanceOf[Boolean]
  }

  /**
    * List all (directories, files) in a directory
    * If `path` is a file, returns itself.
    * Note: In case a broken symlink is found NoSuchFileException is thrown
    */
  def ls(path: Path,
         followSymbolicLinks: Boolean = true): (Seq[Path], Seq[Path]) = {
    if (FileUtils.isFile(path, followSymbolicLinks))
      (Seq(), Seq(path))
    else {
      val ds: Iterable[Path] = Files.newDirectoryStream(path).asScala
      val (directories, files) =
        ds.partition(d => isDirectory(d, followSymbolicLinks))
      directories.toSeq -> files.toSeq
    }
  }

  /**
    * List all files recursively under the provided directory
    * If `path` is a file, returns itself.
    */
  def filesUnder(path: Path, followSymbolicLinks: Boolean = true): Seq[Path] = {
    val (dirs, files) = ls(path, followSymbolicLinks)
    dirs.foldLeft(files) {
      case (acc, dir) => acc ++ filesUnder(dir, followSymbolicLinks)
    }
  }

  /**
    * Delete directory recursively (or file)
    * Throws NoSuchFileException if not exist
    * If `keepRoot` is set to true, the root directory is kept (=emptying)
    */
  def deleteDir(path: Path, keepRoot: Boolean = false): Unit = {
    Files.walkFileTree(
      path,
      new SimpleFileVisitor[Path] {
        override def visitFile(
          f: Path,
          basicFileAttributes: BasicFileAttributes
        ): FileVisitResult = {
          Files.delete(f)
          FileVisitResult.CONTINUE
        }

        override def postVisitDirectory(d: Path,
                                        e: IOException): FileVisitResult = {
          if (e == null) {
            if (!keepRoot || d != path)
              Files.delete(d)
            FileVisitResult.CONTINUE
          } else {
            // directory iteration failed
            throw e
          }
        }
      }
    )
  }

  /**
    * Delete directory recursively (or file) if exist
    * No action if not exist
    */
  def deleteDirIfExists(path: Path): Unit = {
    if (exists(path))
      deleteDir(path)
  }
}
