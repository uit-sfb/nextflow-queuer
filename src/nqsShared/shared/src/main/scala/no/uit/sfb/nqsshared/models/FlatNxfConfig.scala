package no.uit.sfb.nqsshared.models

import scala.collection.immutable.ArraySeq

/**
  * Nextflow config in flattened form (stored as key-value pairs)
  * When a key is present multiple times, it is the last instance that counts
  *
  * @param attributes List of flattened nextflow config attributes (key-value pairs)
  * The value is encoded as it is in the nextflow config:
  *   - boolean and int: as is
  *   - array: [...]
  *   - string: '...'
  */
case class FlatNxfConfig(attributes: Seq[(String, String)]) {
  lazy val cfg = attributes.map { case (k, v) => s"$k = $v" }.mkString("\n")

  def get(key: String, default: String = ""): String = {
    attributes
      .filter { case (k, _) => k == key }
      .lastOption
      .map {
        case (_, v) =>
          val tmp =
            if (v.startsWith("'"))
              v.drop(1)
            else v
          if (tmp.endsWith("'"))
            tmp.dropRight(1)
          else tmp
      }
      .getOrElse(default)
  }

  def getParam(param: String, default: String = ""): String = {
    get(s"params.$param", default)
  }

  protected def updateImpl(key: String, prepValue: String): FlatNxfConfig = {
    val attrs = (attributes
      .filterNot { case (k, _) => k == key } :+ (key -> prepValue)).sorted
    FlatNxfConfig(attrs)
  }

  def update(key: String, value: Boolean): FlatNxfConfig = {
    val prepValue = value.toString
    updateImpl(key, prepValue)
  }

  def update(key: String, value: Int): FlatNxfConfig = {
    val prepValue = value.toString
    updateImpl(key, prepValue)
  }

  def update(key: String, value: String): FlatNxfConfig = {
    val v = value.trim
    val prepValue =
      if (v.startsWith("[") && v.endsWith("]"))
        v
      else
        s"'$v'"
    updateImpl(key, prepValue)
  }

  def delete(key: String): FlatNxfConfig = {
    val attrs = attributes.filterNot { case (k, _) => k == key }.sorted
    FlatNxfConfig(attrs)
  }

  def updateParam(param: String, value: Boolean): FlatNxfConfig = {
    update(s"params.$param", value)
  }

  def updateParam(param: String, value: Int): FlatNxfConfig = {
    update(s"params.$param", value)
  }

  def updateParam(param: String, value: String): FlatNxfConfig = {
    update(s"params.$param", value)
  }

  def deleteParam(param: String): FlatNxfConfig = {
    delete(s"params.$param")
  }

  def overrideWith(other: FlatNxfConfig): FlatNxfConfig = {
    other.attributes.foldLeft(this) {
      case (acc, (k, v)) =>
        acc.updateImpl(k, v)
    }
  }
}

object FlatNxfConfig {
  def apply(str: String): FlatNxfConfig = {
    val arr = str
      .split('\n')
      .map {
        _.trim
      }
      .filterNot { _.isEmpty }
      .map { l =>
        val splt = l.trim.split('=')
        splt.head.trim -> splt.tail.mkString("=").trim
      }
    FlatNxfConfig(ArraySeq.unsafeWrapArray(arr))
  }
}
