package no.uit.sfb.nqsshared.utils

import java.time.{LocalDateTime, ZoneId}

object Date {

  def now() = LocalDateTime.now(ZoneId.of("UTC"))

  def parse(timestamp: String) =
    LocalDateTime.parse(timestamp.dropRight(1))

  def nowDate(): String = {
    //We need to specify ZoneId UTC, otherwise it uses the system one which breaks in Scalajs since timezones must be downloaded as a separate library
    LocalDateTime.now(ZoneId.of("UTC")).toString.take(10)
  }
  def nowDateTime(): String = {
    //We need to specify ZoneId UTC, otherwise it uses the system one which breaks in Scalajs since timezones must be downloaded as a separate library
    LocalDateTime.now(ZoneId.of("UTC")).toString.take(19) + "Z"
  }

  def prettyPrint(ldt: LocalDateTime): String =
    s"${ldt.getMonth.toString.toLowerCase.capitalize
      .take(3)} ${ldt.getDayOfMonth}, ${ldt.getYear} at ${ldt.getHour}:${ldt.getMinute}:${ldt.getSecond}"

  def prettyPrint(timestamp: String): String = prettyPrint(parse(timestamp))
}
