package no.uit.sfb.nqsshared.models

case class FileDef(key: String, selected: Boolean = false)
