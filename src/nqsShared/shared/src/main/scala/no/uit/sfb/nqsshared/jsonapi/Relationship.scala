package no.uit.sfb.nqsshared.jsonapi

case class Relationship(id: String, `type`: String)
