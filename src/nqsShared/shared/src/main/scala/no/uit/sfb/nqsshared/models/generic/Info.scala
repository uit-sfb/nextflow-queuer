package no.uit.sfb.nqsshared.models.generic

case class Info(name: String, ver: String, git: String, `DB conn. OK`: Boolean)
