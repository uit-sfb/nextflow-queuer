package no.uit.sfb.nqsshared.models

case class StateChangeWithDuration(
  state: String,
  timestamp: String,
  duration: Long //in minutes (toSeconds is not implemented in io.github.cquiroz's java.time)
)
