package no.uit.sfb.nqsshared.jsonapi

case class Err(status: String, title: String, detail: String)
