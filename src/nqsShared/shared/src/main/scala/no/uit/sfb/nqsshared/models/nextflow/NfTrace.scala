package no.uit.sfb.nqsshared.models.nextflow

case class NfTrace(task_id: Int,
                   status: String,
                   hash: String,
                   name: String,
                   exit: Long,
                   submit: Long,
                   start: Long,
                   process: String,
                   tag: Option[String],
                   attempt: Int,
                   workdir: String,
                   //native_id: Option[String] Can be both a string or an int
)
