package no.uit.sfb.nqsshared.models.auth

case class UserInfo(_id: String, email: String = "", role: String = "none")
