package no.uit.sfb.nqsshared.models

case class UrlHandle(url: String, bearerToken: Option[String] = None)
