package no.uit.sfb.nqsshared.models

import no.uit.sfb.nqsshared.utils.{Date, FileUtils}

import java.nio.file.Path
import java.time._

case class JobManifest(_id: String,
                       spec: JobSpec = JobSpec(),
                       // key/file name -> Path or URL (we use String instead of File for serialization purpose.)
                       //All keys should start with inputs/ prefix
                       //Note: inputs need to be in manifest and NOT spec, as this object should not be modified by update-spec.
                       inputs: Seq[InputFileHandle] = Seq(),
                       //Contains both outputs and reports
                       outputs: Seq[String] = Seq(),
                       project: String = "default",
                       server: String = "http://localhost:9000",
                       submitter: String = "anonymous",
                       submittedFrom: String = "http://localhost:9002/ui/jobs",
                       timeline: Seq[StateChange] = Seq(),
                       currentState: String = "draft",
                       lastClient: Option[String] = None) {
  assert(
    timeline.lastOption.map { _.state }.getOrElse("draft") == currentState,
    s"Mismatch between currentState and timeline's last event."
  )
  lazy val jobId = _id
  lazy val name = if (spec.label.isEmpty) jobId else spec.label
  lazy val isDraft = currentState == "draft"
  lazy val runNumber: Int = timeline.count(_.state == "pending")
  lazy val hasBeenPulled: Boolean = timeline.exists(_.state == "initializing")
  lazy val runName = Seq(jobId, runNumber.toString).mkString("_")
  lazy val resume: Boolean = runNumber > 1
  lazy val submissionDate: Option[String] =
    timeline.find(_.state == "pending").map { _.timestamp }

  lazy val isStoppable = Seq("pending", "initializing", "running", "finalizing")
    .contains(currentState)

  lazy val isDeletable =
    Seq("draft", "completed", "failed", "canceled").contains(currentState)

  lazy val isResumable = Seq("canceled").contains(currentState)

  lazy val isRetryable = Seq("failed").contains(currentState)

  lazy val isStartable = isDraft

  lazy val isEditable = isResumable || isRetryable

  lazy val timelineWithDurations = {
    val now = Date.now()
    timeline.foldLeft(Seq[StateChangeWithDuration]()) {
      case (acc, sc) =>
        val init =
          if (acc.isEmpty)
            acc
          else
            acc.init :+ acc.last.copy(
              duration = Duration
                .between(
                  Date.parse(acc.last.timestamp),
                  Date.parse(sc.timestamp)
                )
                .toMinutes
            )
        init :+ StateChangeWithDuration(
          sc.state,
          sc.timestamp,
          if (Seq("completed", "failed", "cancelled").contains(sc.state))
            0L
          else
            Duration
              .between(Date.parse(sc.timestamp), now)
              .toMinutes
        )
    }
  }

  lazy val runs =
    timelineWithDurations.foldLeft(Seq[Seq[StateChangeWithDuration]]()) {
      case (acc, sc) =>
        if (sc.state == "pending")
          acc :+ Seq(sc)
        else
          acc.init :+ (acc.last :+ sc)
    }

  private def cumulTime(states: Seq[String]) = {
    timelineWithDurations.foldLeft(0L) {
      case (acc, sc) =>
        if (states.contains(sc.state))
          acc + sc.duration
        else
          acc
    }
  }

  lazy val active = cumulTime(Seq("initializing", "running", "finalizing"))
  lazy val idle = cumulTime(Seq("pending"))

  //Only for the executors use
  def fileUrl(key: String) =
    s"$server/api/v1/jobs/files/inputs?jobId=$jobId&key=$key"

  //Paths
  lazy val jobPath = jobId
  lazy val workPath = Seq(jobPath, "work").mkString("/")
  lazy val filesPath = Seq(jobPath, "files").mkString("/")
  lazy val inputsPath = Seq(filesPath, "inputs").mkString("/")
  lazy val outputsPath = Seq(filesPath, "outputs").mkString("/")
  lazy val reportsPath = Seq(filesPath, "reports").mkString("/")
  def filePath(key: String) = Seq(filesPath, key).mkString("/")
  def tmpFilePath(key: String) = Seq(filesPath, s"_$key").mkString("/")
  lazy val configPath = filePath("nextflow.config")
  lazy val mainScriptPath = filePath("main.nf")
  lazy val dagPath = filePath(s"reports/$runName.dag.html")
  lazy val timelinePath = filePath(s"reports/$runName.timeline.html")
  lazy val reportPath = filePath(s"reports/$runName.report.html")
  lazy val logPath = filePath(s"reports/$runName.log")
  lazy val tracePath = filePath(s"reports/$runName.stdout")
  lazy val traceTemplatePath = Seq(jobPath, "traceTemplate.txt").mkString("/")

  //action is executed only when newState is different from currentState
  //and acts on the current job manifest (i.e. before update)
  def update(newState: String, action: JobManifest => Unit): JobManifest = {
    if (currentState != newState) {
      action(this)
      copy(
        currentState = newState,
        timeline = timeline :+ StateChange(newState, Date.nowDateTime())
      )
    } else
      this
  }

  def readyForPush(workdir: Path): Boolean = {
    !inputs.exists { fh =>
      FileUtils.exists(workdir.resolve(tmpFilePath(fh.key))) ||
      !FileUtils.exists(workdir.resolve(filePath(fh.key)))
    }
  }

  def belongsTo(userId: Option[String]) =
    submitter == userId.getOrElse("anonymous")
}
