package no.uit.sfb.nqsshared.models

case class StateChange(state: String, timestamp: String)
