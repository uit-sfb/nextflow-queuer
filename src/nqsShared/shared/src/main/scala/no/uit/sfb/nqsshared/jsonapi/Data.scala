package no.uit.sfb.nqsshared.jsonapi

import io.circe.Encoder

case class Data[T: Encoder](`type`: String,
                            id: String = "",
                            attributes: T,
                            relationships: Map[String, Relationships] = Map())
