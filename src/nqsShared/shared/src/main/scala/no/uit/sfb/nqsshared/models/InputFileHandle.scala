package no.uit.sfb.nqsshared.models

case class InputFileHandle(key: String, originUrl: Option[String] = None)
