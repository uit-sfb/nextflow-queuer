package no.uit.sfb.nqsshared.models

case class JobSpec(label: String = "",
                   description: String = "",
                   pipeline: String = "", //Repo or empty for custom script
                   version: String = "master", //branch or revision
                   profiles: Seq[String] = Seq(),
                   config: String = "",
                   customScript: String = "", //Only used when pipeline is empty
                   timeout: Int = 0, //In seconds. <=0 to disable timeout
                   collectEvents: Boolean = true)
