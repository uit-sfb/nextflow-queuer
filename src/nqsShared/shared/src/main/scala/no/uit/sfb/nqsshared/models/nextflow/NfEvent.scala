package no.uit.sfb.nqsshared.models.nextflow

case class NfEvent(runId: String,
                   event: String,
                   runName: String,
                   utcTime: String,
                   jobId: String,
                   trace: Option[NfTrace] = None,
                   workflow: Option[NfMetadata] = None)
