import com.typesafe.sbt.packager.docker.Cmd
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport.{dockerBaseImage, dockerRepository, dockerUsername}
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Level._
import sbt.Keys.publishConfiguration
import sbtbuildinfo.BuildInfoPlugin.autoImport.{buildInfoKeys, buildInfoPackage}
import sbtcrossproject.CrossProject

ThisBuild / scalaVersion := "2.13.6"
ThisBuild / organization     := "no.uit.sfb"
ThisBuild / organizationName := "uit-sfb"
ThisBuild / maintainer := "mma227@uit.no"
ThisBuild / logLevel := {
  Configurator.setAllLevels(LogManager.getRootLogger.getName, INFO)
  sbt.util.Level.Info
}

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
//lazy val isRelease = settingKey[Boolean]("Is release")
lazy val dockerRegistry = settingKey[String]("Docker registry")
lazy val dockerImageName = settingKey[String]("Docker image name")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown")}
ThisBuild / gitRefName := { sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value) }
//ThisBuild / isRelease := { sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty}
ThisBuild / dockerRegistry := { sys.env.getOrElse("CI_REGISTRY", s"registry.gitlab.com") }
ThisBuild / dockerImageName := {
  s"${sys.env.getOrElse("CI_REGISTRY_IMAGE", s"${dockerRegistry.value}/${organizationName.value}/nextflow-queuer")}"
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / scalacOptions ++= Seq(
  "-feature",
  "-deprecation"
)
ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

lazy val commonDep = Seq("org.scalatest" %% "scalatest" % "3.2.9" % Test)
val circeVersion = "0.14.1"
val pac4jVersion = "5.1.5"
val nextflowVersion = "21.09.1-edge"

lazy val root = project.in(file("."))
  .settings(
    publish := {},
    publishLocal  := {}
  )
  .aggregate(nqsClient, nqsServerRoot, nqsSharedRoot)

lazy val nqsSharedRoot = project.in(file("nqsShared"))
  .aggregate(nqsShared.jvm, nqsShared.js)
  .settings(
    publish := {},
    publishLocal := {},
  )

lazy val nqsShared = crossProject(JSPlatform, JVMPlatform).in(file("nqsShared"))
  .settings(
    libraryDependencies ++= commonDep ++ Seq(
      "io.circe" %% "circe-core" % circeVersion,
      "io.github.cquiroz" %%% "scala-java-time" % "2.3.0"
    ),
    publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
    useCoursier := false,
  )
  .jsSettings(
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "1.1.0",
      "com.github.japgolly.scalajs-react" %%% "extra" % "1.7.7",
      "no.uit.sfb" %%% "scalajs-bindings" % "master"
    ),
  )

lazy val nqsClient = project.in(file("nqsClient"))
  .enablePlugins(BuildInfoPlugin, JavaAppPackaging, DockerPlugin)
  .settings(
    libraryDependencies ++= commonDep ++ Seq(
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
      "ch.qos.logback" % "logback-classic" % "1.2.5",
      "com.github.scopt" %% "scopt" % "4.0.1",
      "com.typesafe" % "config" % "1.4.1",
      "com.typesafe.play" %% "play-ahc-ws-standalone" % "2.1.3",
      "com.typesafe.play" %% "play-ws-standalone-json" % "2.1.3",
      "io.circe" %% "circe-parser" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-generic-extras" % circeVersion
    ),
    //javacOptions ++= Seq("-source", "8", "-target", "8"),
    //scalacOptions += "-target:8",
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"),
    Docker / dockerRepository := Some(dockerRegistry.value),
    Docker / dockerUsername := Some(dockerImageName.value.split("/").tail.mkString("/")),
    dockerBaseImage := s"nextflow/nextflow:$nextflowVersion",
    Docker / daemonUser := "sfb",
    Docker / maintainer := (ThisBuild / maintainer).value,
    dockerCommands := dockerCommands.value.flatMap{
      case cmd @ Cmd("RUN", _*) if cmd.args.mkString(" ").startsWith("id -u") =>
        Seq(
          Cmd("RUN", "yum install shadow-utils -y"),
          cmd,
          Cmd("RUN", "rm /.nextflow/dockerized")
        )
      case cmd @ Cmd("WORKDIR", "/opt/docker") =>
        Seq(
            Cmd("WORKDIR", "/.nextflow"),
          Cmd("RUN", "chown", "-R", "1001:0", "/.nextflow"),
          cmd
        )
      case cmd => Seq(cmd)
    } :+ Cmd("WORKDIR", s"/home/${(Docker / daemonUser).value}"),
    publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
      buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    useCoursier := false,
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}",
    //buildInfoOptions += BuildInfoOption.BuildTime
  )
  .dependsOn(nqsShared.jvm)

lazy val nqsServerRoot = project.in(file("nqsServer"))
  .aggregate(nqsServer.js, nqsServer.jvm)
  .settings(
    publish := {},
    publishLocal := {},
  )

lazy val nqsServer: CrossProject = crossProject(JSPlatform, JVMPlatform).in(file("nqsServer"))
  .settings(
    libraryDependencies ++= Seq(
    "io.circe" %%% "circe-parser" % circeVersion,
    "io.circe" %%% "circe-generic" % circeVersion,
    "io.circe" %%% "circe-generic-extras" % circeVersion
    ),
    publishConfiguration := publishConfiguration.value.withOverwrite(true), //Always allow overwrite since Scala API documentation gets published by docker:publish somehow
    useCoursier := false
  )
  .jvmSettings(
    libraryDependencies ++= commonDep ++ Seq(
      guice,
      caffeine,
      ws,
      specs2 % Test,
      "com.vmunier" %% "scalajs-scripts" % "1.2.0",
      "org.webjars" % "swagger-ui" % "3.51.1",
      "com.lightbend.akka" %% "akka-stream-alpakka-file" % "3.0.3",
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test,
      "com.typesafe.play" %% "play-json" % "2.9.2", //needed at runtime for SwaggerPlugin
      "com.typesafe" % "config" % "1.4.1",
      "org.kie.modules" % "org-apache-commons-compress" % "6.5.0.Final",
      "org.jetbrains.kotlinx" % "kotlinx-datetime" % "0.2.1",
      "org.mongodb.scala" %% "mongo-scala-driver" % "4.3.0",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
      "ch.qos.logback" % "logback-classic" % "1.2.5",
      "com.typesafe.play" %% "play-mailer-guice" % "8.0.1",
      "org.pac4j" %% "play-pac4j" % "11.0.0-PLAY2.8",
      "org.pac4j" % "pac4j-http" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-jwt" % pac4jVersion exclude("com.fasterxml.jackson.core", "jackson-databind"),
      "org.pac4j" % "pac4j-oidc" % pac4jVersion exclude("commons-io", "commons-io") exclude("com.fasterxml.jackson.core", "jackson-databind"), //Used by JWT
    ),
    //javacOptions ++= Seq("-source", "8", "-target", "8"),
    //scalacOptions += "-target:8",
    dockerCommands := dockerCommands.value.flatMap{
      case cmd @ Cmd("RUN", _*) if cmd.args.mkString(" ").startsWith("id -u") =>
        Seq(
          Cmd("RUN", "yum install shadow-utils -y"),
          cmd,
          Cmd("RUN", "rm /.nextflow/dockerized")
        )
      case cmd => Seq(cmd)
    },
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"),
    dockerLabels := Map("gitCommit" -> s"${gitCommit.value}@${gitRefName.value}"), //Do NOT add in Docker
    Docker / dockerRepository := Some(dockerRegistry.value),
    Docker / dockerUsername := Some(dockerImageName.value.split("/").tail.mkString("/")),
    dockerBaseImage := s"nextflow/nextflow:$nextflowVersion", //Do NOT add "in Docker"
    //dockerChmodType in Docker := DockerChmodType.UserGroupWriteExecute,
    //dockerPermissionStrategy in Docker := DockerPermissionStrategy.CopyChown,
    //daemonGroup in Docker := "0",
    Docker / daemonUser := "sfb", //"in Docker" needed for this parameter,
    Docker / maintainer := (ThisBuild / maintainer).value,
    scalaJSProjects := Seq(nqsServer.js),
    Assets / pipelineStages := Seq(scalaJSPipeline),
    pipelineStages := Seq(digest, gzip),
    // triggers scalaJSPipeline when using compile or continuous compilation
    Compile / compile := ((Compile / compile) dependsOn scalaJSPipeline).value,
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      scalaVersion,
      sbtVersion,
      gitCommit
    ),
    buildInfoPackage := s"${organization.value}.info.${name.value.toLowerCase.replace('-', '_')}",
    swaggerPrettyJson := true,
    swaggerV3 := true,
    swaggerDomainNameSpaces := Seq("models")
    //buildInfoOptions += BuildInfoOption.BuildTime
  )
  .jvmConfigure(_.dependsOn(nqsShared.jvm)
    .enablePlugins(PlayScala, SwaggerPlugin, DockerPlugin, JavaAppPackaging, BuildInfoPlugin, WebScalaJSBundlerPlugin)
  )
  .jsSettings(
    scalaJSUseMainModuleInitializer := false,
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), //https://stackoverflow.com/a/64317357/4965515
    webpack / version := "4.46.0", //Cannot update to 5+
    startWebpackDevServer / version := "3.11.2",
    libraryDependencies ++= Seq("io.lemonlabs" %%% "scala-uri" % "3.6.0"),
    //webpackConfigFile := Some(baseDirectory.value / "webpack.config.js"),
    Compile / npmDependencies ++= Seq(
      "react" -> "17.0.1",
      "react-dom" -> "17.0.1",
      "react-bootstrap" -> "1.6.0",
      "react-icons" -> "4.2.0",
      "jsdom" -> "16.5.3"
    ),
    Test / npmDependencies ++= Seq(),
    Test / requireJsDomEnv := true
  )
  .jsConfigure(_.dependsOn(nqsShared.js)
  .enablePlugins(ScalaJSBundlerPlugin))

//Automatic reload
Global / onChangedBuildSource := ReloadOnSourceChanges