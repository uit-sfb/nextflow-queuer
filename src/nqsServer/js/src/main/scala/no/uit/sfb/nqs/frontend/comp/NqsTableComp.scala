package no.uit.sfb.nqs.frontend.comp

import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.nqs.frontend.framework.comp.Spin
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqs.frontend.utils.com.apis.{DraftsApi, JobsApi}
import no.uit.sfb.nqs.frontend.utils.comp.ActionLike
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.utils.Date
import org.scalajs.dom

import scala.concurrent.duration.DurationInt
import scala.scalajs.js
import scala.scalajs.js.timers.SetIntervalHandle

object NqsTableComp {

  case class Props(project: String,
                   apiCall: ApiCall,
                   changeJobId: Option[String] ~=> Callback,
                   error: Boolean => Throwable ~=> Callback,
                   success: Callback)
      extends JobsApi
      with DraftsApi
      with ActionLike

  case class State(jobs: Option[Seq[JobManifest]] = None,
                   timer: Option[SetIntervalHandle] = None)

  class Backend(val $ : BackendScope[Props, State]) {

    val loadJobsAsync = {
      $.props.asAsyncCallback.flatMap { p =>
        p.loadJobs()(p.error(false))
          .flatMap { resp =>
            $.modStateAsync { s =>
              val jobs = resp.data.map {
                _.attributes
              }
              s.copy(jobs = Some(jobs))
            }
          }
          .flatMap { _ =>
            p.success.asAsyncCallback
          }
      }
    }

    def render(p: Props, s: State): VdomNode = {
      s.jobs match {
        case Some(jobs) =>
          <.div(
            <.div(
              ^.textAlign.center,
              Button(
                onClick = _ =>
                  p.newDraft(p.project, dom.window.location.href)(
                      p.error(false)
                    )
                    .flatMap {
                      _.data match {
                        case Some(job) =>
                          p.changeJobId(Some(job.id)).asAsyncCallback
                        case None =>
                          p.error(false)(new Exception("No draft created."))
                            .asAsyncCallback
                      }
                    }
                    .toCallback
              )("New job")
            ),
            <.br(),
            Table(hover = true)(
              <.thead(
                <.tr(
                  <.th("Label"),
                  <.th("Pipeline"),
                  <.th("Submission date"),
                  <.th("State")
                )
              ),
              <.tbody(
                jobs.reverse
                  .map(
                    job =>
                      <.tr(
                        ^.onClick ==> { _ =>
                          p.changeJobId(Some(job.jobId))
                        },
                        <.td(job.name),
                        <.td(job.spec.pipeline),
                        <.td(
                          job.submissionDate
                            .map { Date.prettyPrint }
                            .getOrElse("-"): String
                        ), //Type needed for some reason
                        <.td(
                          p.action(
                            job,
                            p.error(false),
                            loadJobsAsync.toCallback,
                            true
                          )
                        )
                    )
                  ): _*
              )
            )
          )
        case None =>
          Spin()
      }
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val s = lf.state
        //We pull the data straight away
        lf.backend.loadJobsAsync.handleError { err =>
          p.error(true)(err).asAsyncCallback
        }.toCallback >> lf.toModStateWithPropsFn
          .modState { //and start the timer which unfortunately does not take a Callback, so we need to use runNow()
            case (s, p) =>
              s.copy(
                timer =
                  Some(js.timers.setInterval(10.seconds.toMillis.toDouble) {
                    lf.backend.loadJobsAsync
                      .runNow()
                  })
              )
          }
      })
      .componentWillUnmount(
        lf =>
          Callback(lf.state.timer.map { timer =>
            js.timers.clearInterval(timer)
          })
      )
      .build

  def apply(project: String = "default",
            apiCall: ApiCall,
            changeJobId: Option[String] ~=> Callback,
            error: Boolean => Throwable ~=> Callback,
            success: Callback) = {
    component(Props(project, apiCall, changeJobId, error, success))
  }
}
