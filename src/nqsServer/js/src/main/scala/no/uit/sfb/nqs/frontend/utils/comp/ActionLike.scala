package no.uit.sfb.nqs.frontend.utils.comp

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{Callback, CallbackTo, ~=>}
import no.uit.sfb.facade.bootstrap.dropdown.{DropdownButton, DropdownItem}
import no.uit.sfb.nqs.frontend.utils.com.apis.{DraftsApi, JobsApi}
import no.uit.sfb.nqsshared.models.JobManifest
import org.scalajs.dom

trait ActionLike {
  this: JobsApi with DraftsApi =>

  protected def changeJobId: Option[String] ~=> Callback

  def action(job: JobManifest,
             onError: Throwable => Callback,
             refresh: Callback,
             withViewMenuItem: Boolean) = {
    lazy val view = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          changeJobId(Some(job.jobId))
    )("View")
    lazy val clone = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          newDraftFromJob(job.jobId)(onError).flatMap {
            _.data match {
              case Some(job) =>
                changeJobId(Some(job.id)).asAsyncCallback
              case None =>
                onError(new Exception("No draft created.")).asAsyncCallback
            }
          }.toCallback
    )("Clone")
    lazy val stop = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          CallbackTo {
            dom.window.confirm(
              s"""Cancelling the run will immediately stop any running processes. It will still be possible to resume the job skipping the successful processes.
                 |Are you sure you want to proceed?""".stripMargin
            )
          }.flatMap { proceed =>
            if (proceed)
              cancelJob(job.jobId)(onError).flatMap { _ =>
                refresh.asAsyncCallback
              }.toCallback
            else
              Callback.empty
        }
    )("Cancel")
    lazy val delete = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          CallbackTo {
            dom.window.confirm(
              s"""Deleting this job will PERMANENTLY delete the job configuration, outputs, reports, and any cached processes.
               |Are you sure you want to proceed?""".stripMargin
            )
          }.flatMap { proceed =>
            if (proceed)
              deleteJob(job.jobId)(onError).flatMap { _ =>
                refresh.asAsyncCallback
              }.toCallback
            else
              Callback.empty
        }
    )("Delete")
    lazy val start = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          submit(job.jobId)(onError).flatMap { _ =>
            refresh.asAsyncCallback
          }.toCallback
    )("Submit")
    lazy val edit = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          CallbackTo {
            dom.window.confirm(
              s"""Editing this job will OVERWRITE any output of all processes affected by the configuration change.
                 |Are you sure you want to proceed?""".stripMargin
            )
          }.flatMap { proceed =>
            if (proceed)
              editJob(job.jobId)(onError).flatMap {
                _.data match {
                  case Some(job) =>
                    changeJobId(Some(job.id)).asAsyncCallback
                  case None =>
                    onError(new Exception("Conversion to draft failed.")).asAsyncCallback
                }
              }.toCallback
            else
              Callback.empty
        }
    )("Edit")
    lazy val resume = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          retryJob(job.jobId)(onError).flatMap { _ =>
            refresh.asAsyncCallback
          }.toCallback
    )("Resume")
    lazy val retry = DropdownItem(
      onClick = e =>
        e.stopPropagationCB >>
          retryJob(job.jobId)(onError).flatMap { _ =>
            refresh.asAsyncCallback
          }.toCallback
    )("Retry")
    DropdownButton(
      id = s"${job.jobId}-action-dropdown",
      title = job.currentState.capitalize,
      variant = StateBadge.variant(job.currentState),
      size = "sm",
      onClick = _.stopPropagationCB
    )(
      Seq(
        if (withViewMenuItem)
          Some(view)
        else
          None,
        if (job.isStartable)
          Some(start)
        else None,
        if (job.isEditable)
          Some(edit)
        else None,
        if (job.isResumable)
          Some(resume)
        else None,
        if (job.isRetryable)
          Some(retry)
        else None,
        if (job.isStoppable)
          Some(stop)
        else None,
        Some(clone),
        if (job.isDeletable)
          Some(delete)
        else None
      ).flatten: _*
    )
  }
}
