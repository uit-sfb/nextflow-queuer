package no.uit.sfb.nqs.frontend.comp

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.Spinner
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqs.frontend.utils.com.apis.DraftsApi
import no.uit.sfb.nqsshared.models.{FlatNxfConfig, JobManifest}
import org.scalajs.dom

import scala.scalajs.js

class DraftJobComp(val apiCall: ApiCall) extends DraftsApi {

  case class Props(
    origJob: JobManifest,
    customDraftRendering: (JobManifest,
                           JobManifest => CallbackTo[Unit],
                           (String, js.Any) => AsyncCallback[Unit],
                           String => Callback,
                           Throwable => Callback,
                           Set[String]) => VdomNode,
    onError: Throwable => Callback,
    refresh: Callback,
    configOverride: String
  )

  case class State(job: JobManifest, uploading: Set[String] = Set()) {
    lazy val isUploading = uploading.nonEmpty
  }

  class Backend(val $ : BackendScope[Props, State]) {

    val updateJob: JobManifest ~=> CallbackTo[Unit] =
      Reusable.fn(
        (jm: JobManifest) =>
          $.toModStateWithPropsFn.modState {
            case (s, p) =>
              val overriddenJm =
                if (p.configOverride.isEmpty)
                  jm
                else {
                  jm.copy(
                    spec = jm.spec.copy(
                      config = FlatNxfConfig(jm.spec.config)
                        .overrideWith(FlatNxfConfig(p.configOverride))
                        .cfg
                    )
                  )
                }
              s.copy(job = overriddenJm)
        }
      )

    val addUploadingKey: String ~=> CallbackTo[Unit] =
      Reusable.fn(
        (key: String) => $.modState(s => s.copy(uploading = s.uploading + key))
      )

    val removeUploadingKey: String ~=> CallbackTo[Unit] =
      Reusable.fn(
        (key: String) => $.modState(s => s.copy(uploading = s.uploading - key))
      )

    protected def uploadFile(job: JobManifest,
                             key: String,
                             onError: Throwable => Callback)(body: js.Any) = {
      Callback.empty.asAsyncCallback.flatMap { _ => //Needed otherwise, even though it is a def, the body is computed when the component is being mounted.
        uploadInput(job.jobId, key)(onError)(body)
      }
    }

    protected def deleteFileCb(
      job: JobManifest,
      onError: Throwable => Callback
    )(key: String): Callback = {
      deleteInput(job.jobId, key)(onError).flatMap { resp =>
        val jm = resp.data.get.attributes
        updateJob(job.copy(inputs = jm.inputs)).asAsyncCallback
      }.toCallback
    }

    protected def uploadFileCb(
      job: JobManifest,
      onError: Throwable => Callback
    )(key: String, body: js.Any): AsyncCallback[Unit] = {
      addUploadingKey(key).asAsyncCallback >>
        uploadFile(job, key, onError)(body).flatMap { resp =>
          val jm = resp.data.get.attributes
          (updateJob(job.copy(inputs = jm.inputs)) >> removeUploadingKey(key)).asAsyncCallback
        }
    }

    def render(p: Props, s: State): VdomNode = {
      val job = s.job
      val jobSpec = job.spec
      val changed = job.spec != p.origJob.spec
      val submissionUi: VdomNode =
        p.customDraftRendering(
          job,
          updateJob,
          uploadFileCb(job, p.onError),
          deleteFileCb(job, p.onError),
          p.onError,
          s.uploading
        )
      <.div(
        Seq(
          <.div(^.width := "98%", submissionUi), //For some reason it is needed to prevent a scroll bar from appearing
          <.br(),
          <.br(),
          <.div(
            ^.textAlign.center,
            if (changed) {
              <.div(
                Button(
                  variant = "outline-secondary",
                  disabled = s.isUploading,
                  onClick = _ => p.refresh
                )("Discard changes"),
                <.span("   "),
                Button(
                  variant = "primary",
                  disabled = s.isUploading,
                  onClick = _ =>
                    updateSpec(job.jobId, jobSpec)(p.onError).flatMap { _ =>
                      p.refresh.asAsyncCallback
                    }.toCallback
                )("Save ", if (s.isUploading) Spinner(size = "sm")() else "")
              )
            } else
              Button(
                variant = "primary",
                disabled = s.isUploading,
                onClick = _ =>
                  updateSpec(job.jobId, jobSpec)(p.onError).flatMap { _ =>
                    submit(job.jobId)(p.onError).flatMap { _ =>
                      p.refresh.asAsyncCallback
                    }
                  }.toCallback
              )(
                <.div(
                  "Submit ",
                  if (s.isUploading) Spinner(size = "sm")()
                  else Glyphicon("Send")
                )
              )
          )
        ): _*
      )
    }
  }

  protected lazy val comp =
    ScalaComponent
      .builder[Props]
      .initialStateFromProps { p =>
        val jm = p.origJob
        val overriddenJm =
          if (p.configOverride.isEmpty)
            jm
          else {
            jm.copy(
              spec = jm.spec.copy(
                config = FlatNxfConfig(jm.spec.config)
                  .overrideWith(FlatNxfConfig(p.configOverride))
                  .cfg
              )
            )
          }
        State(overriddenJm)
      }
      .renderBackend[Backend]
      .build

  def apply(job: JobManifest,
            customDraftRendering: (JobManifest,
                                   JobManifest => CallbackTo[Unit],
                                   (String, js.Any) => AsyncCallback[Unit],
                                   String => Callback,
                                   Throwable => Callback,
                                   Set[String]) => VdomNode,
            onError: Throwable => Callback,
            refresh: Callback,
            configOverride: String) = {
    comp(Props(job, customDraftRendering, onError, refresh, configOverride))
  }
}
