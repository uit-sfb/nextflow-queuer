package no.uit.sfb.nqs.frontend.utils.com

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.lemonlabs.uri.Url
import japgolly.scalajs.react.Callback
import japgolly.scalajs.react.extra.Ajax
import japgolly.scalajs.react.extra.Ajax.{Step2, isStatusSuccessful}
import no.uit.sfb.nqsshared.com.error.ErrorLike
import no.uit.sfb.nqsshared.jsonapi.{MultipleResponse, SingleResponse}
import org.scalajs.dom

import scala.concurrent.duration.Duration
import scala.scalajs.js
import scala.scalajs.js.URIUtils

class ApiCall(apiUrl: String, accessToken: Option[String]) {

  /**
    * ATTENTION: Do NOT use >> as it runs AT THE SAME TIME as the previous callback (therefore ignoring errors)
    *  The successCallback and errorCallback, or, if desired the following pattern:
    *  run(...).asAsyncCallback.void //.void only if we do not care about xhr
    *    .map { _ => //Success
          ...
        }
        .handleError{ t => //Failure
          AsyncCallback.pure(...)
        }
        .map { _ => //Always run this
          ...
        }
    */
  protected def call(
    method: String,
    path: String,
    body: js.Any = js.undefined,
    contentType: Option[String] = None,
    timeout: Option[Long] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2,
    successCallback: Callback,
    errorCallback: Throwable => Callback
  ): Step2 = {
    val scheme = if (apiUrl.isEmpty) "http" else apiUrl.trim.split(':').head
    val t1 = Ajax(method, s"$apiUrl$path")
      .setRequestHeader("X-Requested-With", "XMLHttpRequest")
      .setRequestHeader("X-Forwarded-Proto", scheme)
    val t2 = accessToken match {
      case Some(at) => t1.setRequestHeader("Authorization", s"Bearer $at")
      case _        => t1
    }
    val t3 = contentType match {
      case Some(ct) =>
        t2.setRequestHeader("Content-Type", ct)
      case None =>
        t2
    }
    val t4 = t3.send(body)
    val t5 = timeout match {
      case Some(t) =>
        t4.withTimeout(t.toDouble, xhr => {
          val e = new Exception(s"AJAX query timed out")
          errorCallback(e) >> Callback.throwException(e)
        })
      case None =>
        t4
    }
    validation(
      successCallback,
      e =>
        Callback(println(e)) >> errorCallback(e) >> Callback.throwException(e)
    )(t5)
  }

  def get(path: String,
          contentType: Option[String] = None,
          timeout: Option[Duration] = None,
          validation: (Callback, Throwable => Callback) => Step2 => Step2 =
            withUnauthorizeFallback,
          successCallback: Callback = Callback.empty,
          errorCallback: Throwable => Callback = _ => Callback.empty) =
    call(
      "GET",
      path,
      contentType = contentType,
      timeout = timeout.map { _.toMillis },
      validation = validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def patch(
    path: String,
    contentType: Option[String] = None, //MUST be set to "application/json" if sending json
    timeout: Option[Duration] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2 =
      withUnauthorizeFallback,
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = _ => Callback.empty
  )(body: js.Any) =
    call(
      "PATCH",
      path,
      body,
      contentType = contentType,
      timeout = timeout.map { _.toMillis },
      validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def post(
    path: String,
    contentType: Option[String] = None, //MUST be set to "application/json" if sending json
    timeout: Option[Duration] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2 =
      withUnauthorizeFallback,
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = _ => Callback.empty
  )(body: js.Any) =
    call(
      "POST",
      path,
      body,
      contentType = contentType,
      timeout = timeout.map { _.toMillis },
      validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def delete(path: String,
             contentType: Option[String] = None,
             timeout: Option[Duration] = None,
             validation: (Callback, Throwable => Callback) => Step2 => Step2 =
               withUnauthorizeFallback,
             successCallback: Callback = Callback.empty,
             errorCallback: Throwable => Callback = _ => Callback.empty) =
    call(
      "DELETE",
      path,
      contentType = contentType,
      timeout = timeout.map { _.toMillis },
      validation = validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  def send(
    path: String,
    as: String,
    contentType: Option[String] = None, //MUST be set to "application/json" if sending json
    timeout: Option[Duration] = None,
    validation: (Callback, Throwable => Callback) => Step2 => Step2 =
      withUnauthorizeFallback,
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = _ => Callback.empty
  )(body: js.Any) =
    call(
      as,
      path,
      body,
      contentType = contentType,
      timeout = timeout.map { _.toMillis },
      validation,
      successCallback = successCallback,
      errorCallback = errorCallback
    ).asAsyncCallback

  protected def withUnauthorizeFallback(
    successCallback: Callback = Callback.empty,
    errorCallback: Throwable => Callback = Callback.throwException
  )(s: Step2): Step2 = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    s.onComplete { xhr =>
      val parsedUrl = Url.parse(dom.window.location.href)
      val isRetry = parsedUrl.query.params.exists {
        case (k, _) => k == "retry"
      }
      val status = xhr.status
      if (isStatusSuccessful(status))
        successCallback //Note: we do not check if the data contains JsonAPI errors, but if needed be, it should be done here.
      else if (status == 401 && !isRetry) {
        Callback {
          dom.window.location
            .assign(parsedUrl.addParam("retry", None).toStringRaw)
        }
      } else {
        errorCallback(
          decode[SingleResponse[Unit]](xhr.responseText).toTry.toOption
            .orElse(
              decode[MultipleResponse[Unit]](xhr.responseText).toTry.toOption
            )
            .map {
              _.errors
            } match {
            case Some(err :: _) => //We only take the first error
              new ErrorLike {
                def statusCode: Int = err.status.toInt
                def title: String = err.title
                def details: String = err.detail
              }
            case _ =>
              new ErrorLike {
                def statusCode: Int = xhr.status
                def title: String = xhr.statusText
                def details: String = {
                  xhr.responseURL.toOption
                    .map { url =>
                      val decoded = URIUtils.decodeURIComponent(url)
                      s"While accessing ${if (decoded.isEmpty) url else decoded}"
                    }
                    .getOrElse("")
                }
              }
          }
        )
      }
    }
  }
}
