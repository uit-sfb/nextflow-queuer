package no.uit.sfb.nqs.frontend.utils

object DurationUtils {
  def toDHM(minutes: Long): String = {
    val h = minutes / 60
    val days = h / 24
    val hours = h - days * 24
    val min = minutes - h * 60
    val daysStr =
      if (days == 0)
        ""
      else
        s"$days days "
    s"$daysStr ${hours}h ${min}m"
  }
}
