package no.uit.sfb.nqs.frontend.utils.comp

import io.lemonlabs.uri.Url
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.dropdown._
import no.uit.sfb.nqsshared.models.auth.UserInfo
import org.scalajs.dom

/**
  * Either use:
  *  - Login.button(...) to insert a login/logout button,
  *  - or Login.loginButton()/Login.logoutButton() for only the login/logout buttons,
  *  - or add Login.overlay manually and use the Login.action in an onClick callback.
  */
class Login(loginUrl: Option[String], logoutUrl: Option[String]) {
  protected val loginAction = Callback {
    val url = loginUrl.getOrElse {
      val parsedUrl = Url.parse(dom.window.location.href)
      parsedUrl.addParam("secure", Some(true)).toStringRaw
    }
    dom.window.location
      .assign(url)
  }

  protected val logoutAction = Callback {
    val url = logoutUrl.getOrElse(
      s"/logout?url=" + dom.window.location.pathname + dom.window.location.search
    )
    dom.window.location
      .assign(url)
  }

  //Do not wrap in a div or you'll struggle to get buttons next to each other
  def loginButton(variant: String = "success", size: String = "sm") =
    Button(variant = variant, size = size, onClick = _ => loginAction)("Log in")

  def logoutButton(variant: String = "secondary") = {
    Button(variant = variant, onClick = _ => logoutAction)("Log out")
  }

  def button(userInfo: Option[UserInfo],
             windowMarker: String = "",
             switchOnModalCallback: CallbackTo[Unit] = Callback.empty,
             unsubscribeCallback: CallbackTo[Unit] = Callback.empty) = {
    //Do not wrap in a div or you'll struggle to get buttons next to each other
    userInfo match {
      case Some(info) if info._id.nonEmpty && info._id != "anonymous" =>
        val variant = "secondary"
        val dropDownItemList = Seq[Option[VdomElement]](
          Some(DropdownItem(onClick = _ => logoutAction)("Log out")),
          Some(
            DropdownItem(
              onClick = _ => Callback(dom.window.alert(s"""id: ${info._id}
                                                          |email: ${info.email}
                                                          |role: ${info.role}
                                                          |""".stripMargin))
            )("User info")
          ),
          windowMarker match { //request access for a member, pending role is not empty
            case s: String if s.contains("browser") =>
              Some(
                DropdownItem(onClick = _ => switchOnModalCallback)(
                  "Request access"
                )
              )
            case _ => None
          },
          windowMarker match {
            case s: String if s.contains("browser") =>
              Some(
                DropdownItem(onClick = _ => unsubscribeCallback)("Unsubscribe")
              )
            case _ => None
          },
        )

        Dropdown()(
          ButtonGroup()(
            logoutButton(variant),
            DropdownToggle(split = true, variant = variant)(),
            DropdownMenu()(dropDownItemList.flatMap { e =>
              e

            }: _*)
          )
        )
      case _ =>
        val variant = "success"
        Dropdown()(
          ButtonGroup()(
            loginButton(variant),
            DropdownToggle(split = true, variant = variant)(),
            DropdownMenu()(
              DropdownItem(onClick = _ => loginAction)("Login"),
              DropdownItem(href = "https://elixir-europe.org/register")(
                "Register"
              )
            )
          )
        )
    }
  }
}
