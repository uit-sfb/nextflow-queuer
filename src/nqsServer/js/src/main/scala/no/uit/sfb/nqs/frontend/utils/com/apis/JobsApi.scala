package no.uit.sfb.nqs.frontend.utils.com.apis

import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import japgolly.scalajs.react.{AsyncCallback, Callback}
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqsshared.jsonapi.{MultipleResponse, SingleResponse}
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.models.nextflow.NfEvent

import scala.concurrent.duration.DurationInt

trait JobsApi {
  def apiCall: ApiCall

  def loadJobs()(
    onError: Throwable => Callback
  ): AsyncCallback[MultipleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/jobs",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[MultipleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def loadJob(jobId: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/jobs?jobId=$jobId",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def editJob(jobId: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/jobs/edit?jobId=$jobId",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def retryJob(jobId: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/jobs/retry?jobId=$jobId",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def cancelJob(jobId: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/jobs/cancel?jobId=$jobId",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def jobEvents(jobId: String, runName: Option[String])(
    onError: Throwable => Callback
  ): AsyncCallback[MultipleResponse[NfEvent]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/events?jobId=$jobId" + (runName match {
          case Some(n) => s"&runName=$n"
          case None    => ""
        }),
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[MultipleResponse[NfEvent]](xhr.responseText).toTry.get
      }
  }

  def deleteJob(
    jobId: String
  )(onError: Throwable => Callback): AsyncCallback[Unit] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .delete(
        s"/api/v1/jobs?jobId=$jobId",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        ()
      }
  }
}
