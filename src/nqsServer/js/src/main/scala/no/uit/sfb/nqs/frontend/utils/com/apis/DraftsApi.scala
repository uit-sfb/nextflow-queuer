package no.uit.sfb.nqs.frontend.utils.com.apis

import io.circe.generic.auto._
import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import io.circe.syntax._
import japgolly.scalajs.react.{AsyncCallback, Callback}
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqsshared.jsonapi.SingleResponse
import no.uit.sfb.nqsshared.models.{JobManifest, JobSpec}

import scala.concurrent.duration.DurationInt
import scala.scalajs.js

trait DraftsApi {
  def apiCall: ApiCall

  def newDraft(project: String, submittedFrom: => String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .post(
        s"/api/v1/drafts?project=$project&submittedFrom=$submittedFrom",
        contentType = Some("application/json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )(())
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def newDraftFromJob(jobId: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .post(
        s"/api/v1/drafts/from-job?jobId=$jobId",
        contentType = Some("application/json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )(())
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def updateSpec(jobId: String, spec: JobSpec)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .patch(
        s"/api/v1/drafts/update-spec?jobId=$jobId",
        contentType = Some("application/json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )(spec.asJson.noSpaces)
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def submit(jobId: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get(
        s"/api/v1/drafts/submit?jobId=$jobId",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def deleteInput(jobId: String, key: String)(
    onError: Throwable => Callback
  ): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .delete(
        s"/api/v1/drafts/inputs?jobId=$jobId&key=$key",
        contentType = Some("application/vnd.api+json"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }

  def uploadInput(jobId: String, key: String)(
    onError: Throwable => Callback
  )(body: js.Any): AsyncCallback[SingleResponse[JobManifest]] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .post(
        s"/api/v1/drafts/inputs?jobId=$jobId&key=$key",
        contentType = Some("application/octet-stream"),
        errorCallback = onError
      )(body)
      .map { xhr =>
        decode[SingleResponse[JobManifest]](xhr.responseText).toTry.get
      }
  }
}
