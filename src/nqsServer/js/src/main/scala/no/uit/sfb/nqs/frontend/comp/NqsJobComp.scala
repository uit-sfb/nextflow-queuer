package no.uit.sfb.nqs.frontend.comp

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.navs.{Nav, NavItem, NavLink}
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.nqs.frontend.framework.comp._
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqs.frontend.utils.com.apis.{DraftsApi, JobsApi, PresignedApi}
import no.uit.sfb.nqs.frontend.utils.comp.ActionLike
import no.uit.sfb.nqsshared.models.JobManifest

import scala.concurrent.duration.DurationInt
import scala.scalajs.js
import scala.scalajs.js.timers.SetIntervalHandle

class NqsJobComp(
  customDraftRendering: (JobManifest,
                         JobManifest => CallbackTo[Unit],
                         (String, js.Any) => AsyncCallback[Unit],
                         String => Callback,
                         Throwable => Callback,
                         Set[String]) => VdomNode
) {

  case class Props(jobId: String,
                   apiCall: ApiCall,
                   changeJobId: Option[String] ~=> Callback,
                   error: Boolean => Throwable ~=> Callback,
                   success: Callback,
                   configOverride: String)
      extends JobsApi
      with DraftsApi
      with ActionLike
      with PresignedApi

  case class State(job: Option[Option[JobManifest]] = None,
                   activeTab: String = "_overview",
                   htmlUrl: Option[String] = None,
                   timer: Option[SetIntervalHandle] = None)

  class Backend(val $ : BackendScope[Props, State]) {

    lazy val loadJobAsync = {
      $.props.asAsyncCallback.flatMap { p =>
        p.loadJob(p.jobId)(p.error(false))
          .flatMap { resp =>
            $.modStateAsync { s =>
              val job = resp.data.map {
                _.attributes
              }
              s.copy(job = Some(job))
            }
          }
          .flatMap { _ =>
            p.success.asAsyncCallback
          }
      }
    }

    def changeTab(tabName: String, htmlUrl: Option[String]) =
      $.modState(s => s.copy(activeTab = tabName, htmlUrl = htmlUrl))

    protected def jobRendering(job: JobManifest,
                               activeTab: String,
                               htmlUrl: Option[String],
                               p: Props) = {
      val htmlTabs = job.outputs.collect {
        case x if x.startsWith("outputs/") && x.endsWith(".html") =>
          x.drop(8)
      }
      <.div(
        Nav(
          variant = "tabs",
          activeKey = activeTab,
          onSelect = key => {
            val urlAsync =
              if (key.endsWith(".html"))
                p.newPresignedUrl(job.jobId, "outputs", Seq(key))(
                    p.error(false)
                  )
                  .map[Option[String]] {
                    Some(_)
                  } else
                CallbackTo { None }.asAsyncCallback
            urlAsync.flatMap { oUrl =>
              changeTab(key, oUrl).asAsyncCallback
            }.toCallback
          }
        )(("_overview" +: htmlTabs :+ "_reports").map { tName =>
          NavItem()(
            NavLink(eventKey = tName, active = activeTab == tName)(
              if (tName.endsWith(".html"))
                tName.split('/').last.dropRight(5).capitalize
              else
                tName.drop(1).capitalize
            )
          )
        }: _*),
        activeTab match {
          case "_overview" =>
            OverviewComp(
              job,
              p.apiCall,
              p.changeJobId,
              p.error(false),
              loadJobAsync.toCallback
            )
          case "_reports" =>
            ReportsComp(job, p.apiCall, p.error(false))
          case html =>
            htmlUrl match {
              case Some(url) =>
                <.div(
                  ^.className := "container-auto",
                  <.embed(^.src := s"$url?inline=true")
                )
              case None =>
                <.em(s"'$html' not found.")
            }
        }
      )
    }

    def render(p: Props, s: State): VdomNode = {
      s.job match {
        case Some(oJob) =>
          oJob match {
            case Some(job) =>
              <.div(
                Button(onClick = _ => p.changeJobId(None), variant = "primary")(
                  <.div("View all ", Glyphicon("ViewList"))
                ),
                <.br(),
                <.br(),
                if (job.isDraft)
                  new DraftJobComp(p.apiCall)(
                    job,
                    customDraftRendering,
                    p.error(false),
                    loadJobAsync.toCallback,
                    p.configOverride
                  )
                else
                  jobRendering(job, s.activeTab, s.htmlUrl, p)
              )
            case None =>
              <.div(
                <.p(<.em("The data you requested does not exist.")),
                <.div(
                  ^.textAlign.center,
                  Button(
                    onClick = _ => p.changeJobId(None),
                    variant = "primary"
                  )(<.div("Back ", Glyphicon("ViewList")))
                )
              )
          }
        case None =>
          Spin()
      }
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        val s = lf.state
        lf.backend.loadJobAsync.handleError { err =>
          p.error(true)(err).asAsyncCallback
        }.toCallback
      })
      .componentDidUpdate(lf => {
        val p = lf.currentProps
        val s = lf.currentState

        if (s.timer.isEmpty) {
          if (s.job.nonEmpty && s.job.get.exists { _.currentState != "draft" })
            lf.modState { //and start the timer which unfortunately does not take a Callback, so we need to use runNow()
              _.copy(
                timer =
                  Some(js.timers.setInterval(5.seconds.toMillis.toDouble) {
                    lf.backend.loadJobAsync
                      .runNow()
                  })
              )
            } else
            Callback.empty
        } else {
          if (s.job.nonEmpty && s.job.get.exists { _.currentState != "draft" })
            Callback.empty
          else
            lf.modState { s =>
              js.timers.clearInterval(s.timer.get)
              s.copy(timer = None)
            }
        }
      })
      .componentWillUnmount(
        lf =>
          Callback(lf.state.timer.map { timer =>
            js.timers.clearInterval(timer)
          })
      )
      .build

  //config override should only be used in contexts where we are sure that the config string is flattened nxf
  def apply(jobId: String,
            configOverride: String,
            apiCall: ApiCall,
            changeJobId: Option[String] ~=> Callback,
            error: Boolean => Throwable ~=> Callback,
            success: Callback) = {
    val decodedConfigOverride = configOverride.replace("&#x27;", "'")
    component(
      Props(jobId, apiCall, changeJobId, error, success, decodedConfigOverride)
    )
  }
}
