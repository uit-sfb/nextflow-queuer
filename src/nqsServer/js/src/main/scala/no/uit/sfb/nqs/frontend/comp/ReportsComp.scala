package no.uit.sfb.nqs.frontend.comp

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.dropdown.{DropdownButton, DropdownItem}
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.facade.bootstrap.navs.{Nav, NavItem, NavLink}
import no.uit.sfb.facade.bootstrap.{ProgressBar, Table}
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.nqs.frontend.framework._
import no.uit.sfb.nqs.frontend.framework.comp._
import no.uit.sfb.nqs.frontend.utils.DurationUtils
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqs.frontend.utils.com.apis.{DraftsApi, JobsApi, PresignedApi}
import no.uit.sfb.nqs.frontend.utils.comp.{ActionLike, StateBadge}
import no.uit.sfb.nqsshared.models.nextflow.NfEvent
import no.uit.sfb.nqsshared.models.{JobManifest, JobSpec}
import no.uit.sfb.nqsshared.utils.Date
import org.scalajs.dom

import scala.concurrent.duration.DurationInt
import scala.scalajs.js
import scala.scalajs.js.timers.SetIntervalHandle

object ReportsComp {

  case class Props(job: JobManifest,
                   apiCall: ApiCall,
                   onError: Throwable => Callback)
      extends PresignedApi

  case class State(run: Int,
                   activeTab: String = "logs",
                   embeddedUrl: Option[String] = None)

  class Backend(val $ : BackendScope[Props, State]) {

    def changeRunOrTab(run: Option[Int], tabName: Option[String]) = {
      $.props
        .zip($.state)
        .asAsyncCallback
        .flatMap {
          case (p, s) =>
            val r = run.getOrElse(s.run)
            val t = tabName.getOrElse(s.activeTab)
            val runName = s"${p.job.jobId}_$r"
            val cb = t match {
              case report
                  if p.job.outputs.contains(s"reports/$runName.$report.html") =>
                p.newPresignedUrl(
                    p.job.jobId,
                    "reports",
                    Seq(s"$runName.$report.html")
                  )(p.onError)
                  .map[Option[String]] {
                    Some(_)
                  }
              case _ =>
                CallbackTo[Option[String]] {
                  None
                }.asAsyncCallback
            }
            //We make sure to fall back to logs tab in case the report is not present
            cb.flatMap { oUrl =>
              val tab = oUrl match {
                case Some(_) =>
                  t
                case None =>
                  "logs"
              }
              $.modStateAsync(
                _.copy(run = r, activeTab = tab, embeddedUrl = oUrl)
              )
            }
        }
        .toCallback
    }

    protected def downloadFile(p: Props,
                               `type`: String //inputs, outputs or reports
    )(jobId: String, keys: Seq[String], name: String) = {
      Button(
        variant = "outline-primary",
        onClick = _ =>
          p.newPresignedUrl(jobId, `type`, keys)(p.onError)
            .flatMap { url =>
              Callback {
                dom.window.location
                  .assign(url)
              }.asAsyncCallback
            }
            .toCallback
      )(<.div(s"$name ", Glyphicon("CloudDownload")))
    }

    protected def displayEmbeddedUrl(oUrl: Option[String]) = {
      oUrl match {
        case Some(url) =>
          <.div(
            ^.className := "container-auto",
            <.embed(^.src := s"$url?inline=true")
          )
        case None =>
          <.div()
      }
    }

    def render(p: Props, s: State): VdomNode = {
      val job = p.job
      val stateChanges = job.runs(s.run - 1)
      val runName = s"${job.jobId}_${s.run}"
      lazy val logs = <.div(
        Table()(
          <.thead(<.tr(<.th("State"), <.th("Date"), <.th("Duration"))),
          <.tbody(stateChanges.map { sc =>
            <.tr(
              <.td(StateBadge(sc.state)),
              <.td(Date.prettyPrint(sc.timestamp)),
              <.td(DurationUtils.toDHM(sc.duration))
            )
          }: _*)
        ),
        <.br,
        if (job.outputs
              .contains(s"reports/$runName.stdout"))
          downloadFile(p, "reports")(
            job.jobId,
            Seq(s"$runName.stdout"),
            "Stdout"
          )
        else
          <.div(),
        if (job.outputs
              .contains(s"reports/$runName.log"))
          downloadFile(p, "reports")(
            job.jobId,
            Seq(s"$runName.log"),
            "Nextflow logs"
          )
        else
          <.div(),
      )
      lazy val tabs = Seq(
        Some("logs"),
        if (job.outputs.contains(s"reports/$runName.report.html"))
          Some("report")
        else None,
        if (job.outputs.contains(s"reports/$runName.timeline.html"))
          Some("timeline")
        else None,
        if (job.outputs.contains(s"reports/$runName.dag.html"))
          Some("dag")
        else None
      ).flatten
      lazy val nav = <.div(
        Nav(
          variant = "pills",
          //className = "flex-column",
          activeKey = s.activeTab,
          onSelect = key => {
            changeRunOrTab(None, Some(key))
          }
        )(tabs.map { tName =>
          NavItem()(
            NavLink(eventKey = tName, active = s.activeTab == tName)(
              tName.capitalize
            )
          )
        }: _*),
        s.activeTab match {
          case "logs" => logs
          case report =>
            displayEmbeddedUrl(s.embeddedUrl)
        }
      )
      Container(fluid = true)(
        Row()(
          Col(sm = 1)(
            DropdownButton(
              id = "run-select-dropdown",
              title = s"Run #${s.run}"
            )(1.to(job.runNumber).reverse.map { r =>
              DropdownItem(onClick = _ => changeRunOrTab(Some(r), None))(
                s"Run #$r"
              )
            }: _*)
          ),
          Col()(nav)
        )
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialStateFromProps(p => State(run = p.job.runNumber))
      .renderBackend[Backend]
      .build

  def apply(job: JobManifest,
            apiCall: ApiCall,
            onError: Throwable => Callback) = {
    component(Props(job, apiCall, onError))
  }
}
