package no.uit.sfb.nqs.frontend.comp

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.buttons.{Button, ButtonGroup}
import no.uit.sfb.facade.bootstrap.dropdown.{Dropdown, DropdownItem, DropdownMenu, DropdownToggle}
import no.uit.sfb.facade.bootstrap.form.{Form, FormGroup}
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.bootstrap.Table
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.nqs.frontend.framework.comp.SwitchField
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqs.frontend.utils.com.apis.{DraftsApi, JobsApi, PresignedApi}
import no.uit.sfb.nqs.frontend.utils.comp.ActionLike
import no.uit.sfb.nqsshared.models.nextflow.NfEvent
import no.uit.sfb.nqsshared.models.{FileDef, InputFileHandle, JobManifest}
import org.scalajs.dom
import scala.concurrent.duration.DurationInt
import scala.scalajs.js
import scala.scalajs.js.timers.SetIntervalHandle

object OverviewComp {

  case class Props(job: JobManifest,
                   apiCall: ApiCall,
                   changeJobId: Option[String] ~=> Callback,
                   onError: Throwable => Callback,
                   refresh: Callback)
      extends ActionLike
      with JobsApi
      with DraftsApi
      with PresignedApi

  case class State(events: Seq[NfEvent] = Seq(),
                   timer: Option[SetIntervalHandle] = None,
                   showModal: Boolean = false,
                   fileSeq: Seq[FileDef] = Seq(),
                   fileType: String = "",
                  )

  class Backend(val $ : BackendScope[Props, State]) {

    val switchOnModal = $.modState(_.copy(showModal = true))

    val switchOffModal = $.modState(_.copy(showModal = false))

    val setFileDefs =
      Reusable.fn((fileDefs: Seq[FileDef]) => {
      $.modState(
        _.copy(
          fileSeq = fileDefs
        )
      )
    })

    val setType =
      Reusable.fn((t: String) => {
        $.modState(
          _.copy(
            fileType = t
          )
        )
      })

    def downloadFile(p: Props,
                     `type`: String, //inputs, outputs or reports
                    )(jobId: String, fileDefs: Seq[FileDef], name: String) = {
      Dropdown()(
        ButtonGroup()(
          Button(
            variant = "outline-primary",
            onClick = _ =>
              p.newPresignedUrl(jobId, `type`)(p.onError).flatMap{
                url =>
                  Callback {
                    dom.window.location
                      .assign(
                        url
                      )
                  }.asAsyncCallback
              }.toCallback
          )(<.div(s"$name ", Glyphicon("CloudDownload"))),
          DropdownToggle(split = true, variant = "outline-primary")(),
          DropdownMenu()(
            DropdownItem(onClick = _ => setFileDefs(fileDefs) >> setType(`type`) >> switchOnModal)("Custom archive"),
            DropdownItem(onClick = _ => p.newPresignedUrl(jobId, `type`, Seq(), 0, 0)(p.onError).flatMap {
              url =>
                Callback{
                  dom.window.navigator.clipboard.writeText(url)
                  dom.window.alert("A shareable link has been copied to your clipboard!")
                }.asAsyncCallback
            }.toCallback)("Permalink")
          ),
        )
      )
    }

    //create file definition with only filename as a key discarding folder name to create proper url for location.assign
    def getFileDefs(files: Seq[Any]): Seq[FileDef] = {
      val unorderedFiles = files map {
        case f: InputFileHandle =>
          FileDef(f.key.split("/").tail.mkString("/"))
        case f: String =>
          FileDef(f.split("/").tail.mkString("/"))
        case f =>
          throw new Exception(s"Expected InputFileHandle or String, but got ${f.getClass}")
      }
      unorderedFiles.sortBy{_.key}
    }

    protected def settings(job: JobManifest, p: Props) = {
      val props: Seq[Option[(String, VdomNode)]] = Seq(
        if (job.spec.label.nonEmpty) Some("Job Id" -> job.jobId) else None,
        if (job.spec.description.nonEmpty)
          Some("Description" -> job.spec.description)
        else None,
        Some("Pipeline" -> job.spec.pipeline),
        Some("Version" -> job.spec.version),
        Some(
          "Profiles" -> (if (job.spec.profiles.isEmpty) "-"
                         else job.spec.profiles.mkString(", "))
        ),
        Some(
          "Inputs" -> (if (job.inputs.nonEmpty)
                         downloadFile(p, "inputs")(
                           job.jobId,
                           getFileDefs(job.inputs),
                           "Inputs"
                         )
                       else
                         "-")
        ),
        Some("Config" -> <.div(^.className := "multiline", job.spec.config)),
        Some(
          "Timeout" -> (if (job.spec.timeout <= 0) "-"
                        else s"${job.spec.timeout} seconds")
        )
      )
      Table(striped = false, bordered = false)(<.tbody(props.flatten.map {
        case (k, v) => <.tr(<.td(<.b(k)), <.td(v))
      }: _*))
    }

    def render(p: Props, s: State): VdomNode = {
      val job = p.job
      val documentModal = Modal(
        animation = false,
        size = "m",
        show = s.showModal,
        onHide = _ => switchOffModal
      )(
        ModalBody()(
          <.h4("Select files to include in the archive"),
          Form(onSubmit = _.preventDefaultCB)(
            FormGroup("inputFile")(
              <.div(
                s.fileSeq.map{ f =>
                  SwitchField(
                    label = f.key,
                    checked = f.selected,
                    onChange = newValue =>
                      setFileDefs(
                        s.fileSeq.map{ fd =>
                          if (fd.key != f.key) fd
                          else fd.copy(selected = newValue)
                        }
                      )
                  )()
                }: _*
              )
            ),
          )
        ),
        ModalFooter()(
          Button(onClick = _ => switchOffModal, variant = "outline-secondary")("Close"),
          Button(variant = "outline-primary", onClick = _ =>
            if (s.fileSeq.exists(_.selected))
              p.newPresignedUrl(p.job.jobId, s.fileType, s.fileSeq.collect{ case f if f.selected => f.key}, 0, 0)(p.onError).flatMap{
                url =>
                  Callback {
                    dom.window.navigator.clipboard.writeText(url)
                    dom.window.alert("A shareable link has been copied to your clipboard!")
                  }.asAsyncCallback
              }.toCallback >> switchOffModal
            else Callback(dom.window.alert("No files selected!"))
          )
          ("Permalink"),
          Button(onClick = _ =>
            if (s.fileSeq.exists(_.selected))
              p.newPresignedUrl(p.job.jobId, s.fileType, s.fileSeq.collect{ case f if f.selected => f.key})(p.onError).flatMap{
                url =>
                  Callback {
                    dom.window.location
                      .assign(url)
                  }.asAsyncCallback
              }.toCallback >> switchOffModal
            else Callback(dom.window.alert("No files selected!"))
          )
          ("Download")
        )
      )
      <.div(
        <.h2(job.name),
        p.action(job, p.onError, p.refresh, false),
        TasksComp(job, s.events),
        <.div(
          ^.textAlign.center,
          if (job.outputs.exists { _.startsWith("outputs/") })
            downloadFile(p, "outputs")(job.jobId, getFileDefs(job.outputs.filter{_.startsWith("outputs")}), "Outputs")
          else
            <.em("No outputs available")
        ),
        <.hr,
        documentModal,
        settings(job, p)
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .componentDidMount(lf => {
        val p = lf.props
        lazy val loadEventsAsync =
          p.jobEvents(p.job.jobId, None)(p.onError)
            .flatMap { resp =>
              lf.modStateAsync { s =>
                val events = resp.data.map {
                  _.attributes
                }
                s.copy(events = events)
              }
            }

        loadEventsAsync.handleError { err =>
          p.onError(err).asAsyncCallback
        }.toCallback
      })
      .componentDidUpdate(lf => {
        val p = lf.currentProps
        val s = lf.currentState
        lazy val loadJobAsync =
          p.jobEvents(p.job.jobId, None)(p.onError)
            .flatMap { resp =>
              lf.modStateAsync { s =>
                val events = resp.data.map {
                  _.attributes
                }
                s.copy(events = events)
              }
            }

        if (s.timer.isEmpty)
          lf.toModStateWithPropsFn
            .modState { //and start the timer which unfortunately does not take a Callback, so we need to use runNow()
              case (s, p) =>
                s.copy(
                  timer =
                    Some(js.timers.setInterval(5.seconds.toMillis.toDouble) {
                      loadJobAsync
                        .handleError { err =>
                          p.onError(err).asAsyncCallback
                        }
                        .runNow()
                    })
                )
            } else
          Callback.empty
      })
      .componentWillUnmount(
        lf =>
          Callback(lf.state.timer.map { timer =>
            js.timers.clearInterval(timer)
          })
      )
      .build

  def apply(job: JobManifest,
            apiCall: ApiCall,
            changeJobId: Option[String] ~=> Callback,
            onError: Throwable => Callback,
            refresh: Callback) = {
    component(Props(job, apiCall, changeJobId, onError, refresh))
  }
}
