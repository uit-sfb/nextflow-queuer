package no.uit.sfb.nqs.frontend.utils.comp

import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.facade.bootstrap.Badge

object StateBadge {
  def apply(currentState: String): VdomNode = {
    Badge(variant = variant(currentState))(currentState.capitalize)
  }

  def variant(currentState: String): String = {
    currentState match {
      case "draft"        => "light"
      case "pending"      => "secondary"
      case "initializing" => "info"
      case "running"      => "primary"
      case "finalizing"   => "info"
      case "completed"    => "success"
      case "canceled"     => "warning"
      case "failed"       => "danger"
      case _              => "dark"
    }
  }
}
