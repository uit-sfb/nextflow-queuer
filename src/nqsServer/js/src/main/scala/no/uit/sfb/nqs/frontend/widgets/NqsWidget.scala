package no.uit.sfb.nqs.frontend.widgets

import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.nqs.frontend.comp.{NqsJobComp, NqsTableComp}
import no.uit.sfb.nqs.frontend.custom.GenericDraftComp
import no.uit.sfb.nqs.frontend.framework._
import no.uit.sfb.nqs.frontend.framework.comp.{Spin, ToastLike, ToastsComp}
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqs.frontend.utils.comp.SecureLike
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.models.auth.UserInfo
import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("NqsWidget")
class NqsWidget(val loginUrl: String, val logoutUrl: String)
    extends SecureLike {

  case class Props(project: String,
                   configOverride: String,
                   apiUrl: String,
                   accessToken: Option[String],
                   userInfo: Option[UserInfo]) {
    lazy val apiCall =
      new ApiCall(apiUrl, accessToken)
  }

  case class State(state: CompStateLike = CompStateLoading,
                   toasts: Seq[ToastLike] = Seq(),
                   userInfo: Option[UserInfo] = None,
                   jobId: Option[String] = None)
      extends ToasterStateLike {
    def newState(st: CompStateLike, ts: Seq[ToastLike]) =
      copy(state = st, toasts = ts).asInstanceOf[this.type]
  }

  def draftComp: (JobManifest,
                  JobManifest => Callback,
                  (String, js.Any) => AsyncCallback[Unit],
                  String => Callback,
                  Throwable => Callback,
                  Set[String]) => VdomNode = GenericDraftComp.apply _

  class Backend(val $ : BackendScope[Props, State])
      extends ToasterLike[Props, State] {

    val changeJobId: Option[String] ~=> Callback =
      Reusable.fn(
        (jobId: Option[String]) => $.modState(s => s.copy(jobId = jobId))
      )

    def render(p: Props, s: State): VdomNode = {
      lazy val comp: TagMod = s.jobId match {
        case Some(jobId) =>
          new NqsJobComp(draftComp)(
            jobId,
            p.configOverride,
            p.apiCall,
            changeJobId,
            error,
            success
          )
        case None =>
          NqsTableComp(p.project, p.apiCall, changeJobId, error, success)
      }
      secureRender(s.userInfo)(
        <.div(ToastsComp(s.toasts, deleteToast), s.state match {
          case CompStateLoading =>
            Spin()
          case CompStateSuccess =>
            comp
          case CompStateFailed =>
            <.em("Data could not be retrieved.")
        })
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialStateFromProps { p =>
        State(userInfo = p.userInfo)
      }
      .renderBackend[Backend]
      .componentDidMount { lf =>
        val p = lf.props
        val s = lf.state
        if (s.userInfo.isEmpty || s.userInfo.get._id.isEmpty)
          loadUserInfo(p.apiCall)
            .flatMap { userInfo =>
              lf.modStateAsync(_.copy(userInfo = Some(userInfo)))
            }
            .flatMap { _ =>
              lf.backend.success.asAsyncCallback
            }
            .toCallback
        else
          lf.backend.success
      }
      .build

  def apply(project: String,
            configOverride: String,
            apiUrl: String,
            accessToken: Option[String],
            userInfo: Option[UserInfo]) = {
    component(Props(project, configOverride, apiUrl, accessToken, userInfo))
  }

  //userId and email are only used if an access_token is present
  @JSExport("renderInto")
  def renderInto(elemId: String,
                 project: String,
                 configOverride: String,
                 apiUrl: String,
                 accessToken: String,
                 userId: String,
                 email: String) = {
    val at =
      if (accessToken.isEmpty)
        None
      else
        Some(accessToken)
    val userInfo = at.map { _ =>
      UserInfo(userId, email)
    }
    apply(project, configOverride, apiUrl, at, userInfo)
      .renderIntoDOM(dom.document.getElementById(elemId))
  }
}
