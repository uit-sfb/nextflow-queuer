package no.uit.sfb.nqs.frontend.custom

import japgolly.scalajs.react.Ref.Simple
import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react._
import no.uit.sfb.facade.bootstrap.{Spinner, Table}
import no.uit.sfb.facade.bootstrap.buttons.Button
import no.uit.sfb.facade.bootstrap.form.{Form, FormControl, FormFile, FormGroup}
import no.uit.sfb.facade.bootstrap.modal.{Modal, ModalBody, ModalFooter}
import no.uit.sfb.facade.icon.Glyphicon
import no.uit.sfb.nqs.frontend.framework.comp.{Spin, SwitchField, TextField}
import no.uit.sfb.nqsshared.models.JobManifest
import org.scalajs.dom
import org.scalajs.dom.{html, window}
import org.scalajs.dom.raw.HTMLInputElement

import scala.scalajs.js

object GenericDraftComp {

  case class Props(jobManifest: JobManifest,
                   updateJob: JobManifest => Callback,
                   uploadInput: (String, js.Any) => AsyncCallback[Unit],
                   deleteInput: String => Callback,
                   uploading: Set[String])

  case class State(repoMode: Boolean = true, showModal: Boolean = false)

  def seqFieldRendering(
    ref: Simple[html.Input],
    fieldName: String,
    fieldAccessor: JobManifest => Seq[String],
    fieldCopy: (JobManifest, Seq[String]) => JobManifest,
    updateJob: JobManifest => Callback
  )(job: JobManifest) = {
    <.div(
      <.p(fieldName),
      Table()(
        <.tbody(
          (fieldAccessor(job).map { k =>
            <.tr(
              <.td(k),
              <.td(
                Button(
                  size = "sm",
                  variant = "danger",
                  onClick = _ =>
                    updateJob(fieldCopy(job, fieldAccessor(job).filterNot {
                      kk =>
                        kk == k
                    }))
                )(Glyphicon("Trash"))
              )
            )
          } :+ <.tr(
            <.td(
              FormGroup(s"${fieldName}KeyInput")(
                FormControl.uncontrolled(ref, placeholder = "key")()
              )
            ),
            <.td(
              Button(
                size = "sm",
                onClick = _ =>
                  CallbackTo {
                    dom.document
                      .getElementById(s"${fieldName}KeyInput")
                      .asInstanceOf[HTMLInputElement]
                      .value
                  }.flatMap { key =>
                    updateJob(fieldCopy(job, fieldAccessor(job) :+ key))
                }
              )(Glyphicon("Add"))
            )
          )): _*
        )
      )
    )
  }

  class Backend(val $ : BackendScope[Props, State]) {
    private val ref: Simple[html.Input] = Ref[html.Input]

    protected val column = 2

    val showModal =
      Reusable.fn((show: Boolean) => $.modState(s => s.copy(showModal = show)))

    val updateRepoMode =
      Reusable.fn((b: Boolean) => $.modState(s => s.copy(repoMode = b)))

    def inputsRendering(p: Props, s: State) = {
      val job = p.jobManifest
      <.div(
        <.p("Inputs"),
        Table()(<.tbody(job.inputs.map { fh =>
          <.tr(
            <.td(fh.originUrl match {
              case Some(href) =>
                <.a(^.href := href, fh.key)
              case None =>
                fh.key
            }),
            <.td(
              Button(
                size = "sm",
                variant = "danger",
                onClick = _ => p.deleteInput(fh.key)
              )(Glyphicon("Trash"))
            )
          )
        }: _*)),
        if (p.uploading.nonEmpty)
          <.em(s"Uploading input... ", Spinner(size = "sm")())
        else
          Button(onClick = _ => showModal(true))(Glyphicon("Add"))
      )
    }

    def render(p: Props, s: State): VdomNode = {
      val job = p.jobManifest
      val uploadModal = Modal(
        animation = false,
        size = "sm",
        show = s.showModal,
        onHide = _ => showModal(false)
      )(
        ModalBody()(if (p.uploading.nonEmpty) {
          <.div(
            <.h4("Uploading..."),
            <.div(
              s"You may close this message, but please do not reload the page."
            ),
            Spin()
          )
        } else {
          <.div(
            <.h4("Input file"),
            Form(onSubmit = _.preventDefaultCB)(
              FormGroup("newInputNameInput")(
                FormControl.uncontrolled(
                  ref,
                  placeholder = "Input name",
                  required = true
                )()
              ),
              FormGroup("inputFile")(
                FormFile(label = "Config file", required = true)()
              )
            )
          )
        }),
        ModalFooter()(
          Button(
            onClick = _ => showModal(false),
            variant = "outline-secondary"
          )("Close"),
          Button(
            disabled = p.uploading.nonEmpty,
            onClick = _ =>
              CallbackTo {
                val elem = dom.document
                  .getElementById("newInputNameInput")
                  .asInstanceOf[HTMLInputElement]
                val tmp = elem.value
                //We reset the input
                elem.value = elem.defaultValue
                s"inputs/$tmp"
              }.asAsyncCallback.flatMap { //check if input file name is not empty
                case key: String if key.split("inputs/").isEmpty =>
                  CallbackTo(dom.window.alert("File name cannot be empty!")).asAsyncCallback

                case key: String =>
                  val body = {
                    //Note: It was clumsy to work with FormData (new FormData(formElement) did not work)
                    //In addition it created a multipart which was not really required since we only wanted to send one file
                    //Instead we simply return the file alone.
                    val elem = dom.document
                      .getElementById("inputFile")
                      .asInstanceOf[HTMLInputElement]
                    //Alternatively:
                    /*val elem = e.target
                       .asInstanceOf[HTMLFormElement]
                       .elements(0)
                       .asInstanceOf[HTMLInputElement]*/
                    elem.files(0)
                  }
                  p.uploadInput(key, body)
                    .flatMap { _ =>
                      showModal(false).asAsyncCallback
                    }

              }.toCallback
          )("Upload")
        )
      )
      <.div(
        Seq(
          uploadModal,
          TextField(
            job.jobId,
            "Job id",
            column = column,
            disabled = Some("")
          )(),
          TextField(
            job.spec.label,
            "Label",
            column = column,
            onChange =
              str => p.updateJob(job.copy(spec = job.spec.copy(label = str)))
          )(),
          TextField(
            job.spec.description,
            "Description",
            column = column,
            onChange = str =>
              p.updateJob(job.copy(spec = job.spec.copy(description = str)))
          )(),
          SwitchField(
            s.repoMode,
            "Repo mode",
            onChange = updateRepoMode,
            column = column
          )()
        ) ++
          (if (s.repoMode)
             Seq(
               TextField(
                 job.spec.pipeline,
                 "Pipeline",
                 column = column,
                 onChange = str =>
                   p.updateJob(job.copy(spec = job.spec.copy(pipeline = str))),
                 validate =
                   if (s.repoMode)
                     Some(
                       str =>
                         if (str.isEmpty)
                           Some("Pipeline repository cannot be empty.")
                         else None
                     )
                   else
                     None
               )(),
               TextField(
                 job.spec.version,
                 "Version",
                 column = column,
                 onChange = str =>
                   p.updateJob(job.copy(spec = job.spec.copy(version = str)))
               )()
             )
           else
             Seq(
               TextField(
                 job.spec.customScript,
                 "Script",
                 column = column,
                 onChange = str =>
                   p.updateJob(
                     job.copy(spec = job.spec.copy(customScript = str))
                 ),
                 as = "textarea"
               )()
             )) ++
          Seq(
            TextField(
              job.spec.config,
              "Config",
              column = column,
              onChange = str =>
                p.updateJob(job.copy(spec = job.spec.copy(config = str))),
              as = "textarea"
            )(),
            seqFieldRendering(
              ref,
              "Profiles",
              _.spec.profiles,
              (jm, s) => jm.copy(spec = jm.spec.copy(profiles = s)),
              p.updateJob
            )(job),
            inputsRendering(p, s),
            TextField(
              job.spec.timeout.toString,
              "Timeout",
              column = column,
              `type` = "number",
              onChange = str =>
                p.updateJob(
                  job.copy(spec = job.spec.copy(timeout = str.toInt))
              ),
              validate = Some(
                str =>
                  str.toIntOption match {
                    case Some(x) if x < 0 =>
                      Some(s"Must be a positive integer or zero.")
                    case None => Some(s"Must be an integer.")
                    case _    => None
                }
              )
            )()
          ): _*
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .build

  def apply(jobManifest: JobManifest,
            updateJob: JobManifest => Callback,
            uploadInput: (String, js.Any) => AsyncCallback[Unit],
            deleteInput: String => Callback,
            onError: Throwable => Callback,
            uploading: Set[String]): VdomNode = {
    component(
      Props(jobManifest, updateJob, uploadInput, deleteInput, uploading)
    )
  }
}
