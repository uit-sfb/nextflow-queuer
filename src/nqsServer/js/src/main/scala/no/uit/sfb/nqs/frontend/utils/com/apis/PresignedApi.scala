package no.uit.sfb.nqs.frontend.utils.com.apis

import japgolly.scalajs.react.{AsyncCallback, Callback}
import no.uit.sfb.nqs.frontend.utils.com.ApiCall

import scala.concurrent.duration.DurationInt

trait PresignedApi {
  def apiCall: ApiCall

  def newPresignedUrl(
    jobId: String,
    `type`: String,
    keys: Seq[String] = Seq(),
    timeout: Int = 60,
    instances: Int = 1
  )(onError: Throwable => Callback): AsyncCallback[String] = {
    apiCall
      .post(
        s"/api/v1/presigned?jobId=$jobId&type=${`type`}&${keys
          .map { k =>
            s"key=$k"
          }
          .mkString("&")}&timeout=$timeout&instances=$instances",
        contentType = Some("text/plain"),
        timeout = Some(30.seconds),
        errorCallback = onError
      )(())
      .map { xhr =>
        xhr.responseText
      }
  }
}
