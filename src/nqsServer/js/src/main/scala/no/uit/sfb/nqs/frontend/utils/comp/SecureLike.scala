package no.uit.sfb.nqs.frontend.utils.comp

import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import io.circe.generic.extras.auto._
import japgolly.scalajs.react.AsyncCallback
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.nqs.frontend.framework.comp.Spin
import no.uit.sfb.nqs.frontend.utils.com.ApiCall
import no.uit.sfb.nqsshared.models.auth.UserInfo

trait SecureLike {
  def loginUrl: String
  def logoutUrl: String

  def secureRender(userInfo: Option[UserInfo])(render: VdomNode): VdomNode = {
    val login = new Login(
      if (loginUrl.isEmpty) None else Some(loginUrl),
      if (logoutUrl.isEmpty) None else Some(logoutUrl)
    )
    userInfo match {
      case Some(ui) if ui._id.nonEmpty =>
        //If loginUrl is empty -> we use pac4j for auth -> we want to display logout button
        if (loginUrl.isEmpty)
          <.div(<.div(^.textAlign.right, login.button(userInfo)), render)
        else
          render
      case Some(_) =>
        <.div(
          ^.textAlign.center,
          <.p("Please log in"),
          login.loginButton(size = "lg"),
          <.p(^.paddingTop := "40px", "You do not have an Elixir account yet?"),
          <.a(
            ^.href := "https://elixir-europe.org/register",
            ^.target := "_blank",
            <.img(
              ^.src := "https://www.elixir-europe.org/sites/default/files/images/register-button-orange.png",
              ^.width := "150px",
              ^.title := "Register",
              ^.className := "img-link no-bg"
            )
          )
        )
      case None =>
        Spin()
    }
  }

  def loadUserInfo(apiCall: ApiCall): AsyncCallback[UserInfo] = {
    implicit val customConfig: Configuration =
      Configuration.default.withDefaults
    apiCall
      .get("/user-info", Some("application/json"))
      .map { xhr =>
        decode[UserInfo](xhr.responseText).toTry.get
      }
  }
}
