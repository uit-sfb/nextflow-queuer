package no.uit.sfb.nqs.frontend.comp

import japgolly.scalajs.react.vdom.VdomNode
import japgolly.scalajs.react._
import no.uit.sfb.facade.bootstrap.ProgressBar
import no.uit.sfb.facade.bootstrap.grid.{Col, Container, Row}
import no.uit.sfb.nqsshared.models.nextflow.NfEvent
import japgolly.scalajs.react.vdom.html_<^._
import no.uit.sfb.nqs.frontend.framework.comp.SwitchField
import no.uit.sfb.nqs.frontend.utils.DurationUtils
import no.uit.sfb.nqsshared.models.JobManifest

object TasksComp {

  case class Props(job: JobManifest, events: Seq[NfEvent])

  case class State(displayAllTasks: Boolean = false)

  case class TaskStats(pending: Int = 0,
                       running: Int = 0,
                       completed: Int = 0,
                       failed: Int = 0) {
    lazy val sum = pending + running + completed + failed
    lazy val pendingP = (100 * pending.toFloat / sum).toInt
    lazy val runningP = (100 * running.toFloat / sum).toInt
    lazy val completedP = (100 * completed.toFloat / sum).toInt
    lazy val failedP = (100 * failed.toFloat / sum).toInt
    lazy val isOfInterest = pending + running + failed > 0
  }

  class Backend(val $ : BackendScope[Props, State]) {

    def setDisplayAllTasks(b: Boolean) = $.modState(_.copy(displayAllTasks = b))

    protected def tasks(runName: String,
                        events: Seq[NfEvent],
                        allTasks: Boolean): VdomNode = {
      val groupedEvents = events
        .filter { e =>
          e.runName == runName && e.trace.nonEmpty
        }
        .groupBy(_.trace.get.name) //We group by name and not task_id because when a task is retried, it has the same name but different id
        .map { case (_, evs) => evs.last }
      val stats = groupedEvents
        .foldLeft(Map[String, TaskStats]()) {
          case (acc, event) =>
            val process = event.trace.get.process
            val ts = acc.getOrElse(process, TaskStats())
            val newTs = event.trace.get.status match {
              case "SUBMITTED" => ts.copy(pending = ts.pending + 1)
              case "RUNNING"   => ts.copy(running = ts.running + 1)
              case "COMPLETED" => ts.copy(completed = ts.completed + 1)
              case "FAILED"    => ts.copy(failed = ts.failed + 1)
              case _           => ts
            }
            acc + (process -> newTs)
        }
        .toSeq
        .sortBy(_._1)

      Container(fluid = true)(stats.collect {
        case (process, ts) if ts.isOfInterest || allTasks =>
          Row()(
            Col(sm = 8, lg = 6)(process),
            Col(lg = 6)(
              ProgressBar()(
                ProgressBar(
                  ts.failedP,
                  striped = true,
                  variant = Some("danger"),
                  label = Some(s"Failed: ${ts.failed}")
                )(),
                ProgressBar(
                  ts.completedP,
                  striped = true,
                  variant = Some("success"),
                  label = Some(s"Completed: ${ts.completed}")
                )(),
                ProgressBar(
                  ts.runningP,
                  true,
                  variant = Some("primary"),
                  label = Some(s"Running: ${ts.running}")
                )(),
                ProgressBar(
                  ts.pendingP,
                  true,
                  variant = Some("secondary"),
                  label = Some(s"Pending: ${ts.pending}")
                )()
              )
            )
          )
      }.toSeq: _*)
    }

    def render(p: Props, s: State): VdomNode = {
      <.div(
        <.div(
          ^.justifyContent.spaceBetween,
          ^.display.flex,
          SwitchField(
            checked = s.displayAllTasks,
            label = "Display complete tasks",
            onChange = setDisplayAllTasks
          )(),
          <.span(
            s"Active: ${DurationUtils.toDHM(p.job.active)}",
            " / ",
            s"Idle: ${DurationUtils.toDHM(p.job.idle)}"
          )
        ),
        <.div(tasks(p.job.runName, p.events, s.displayAllTasks))
      )
    }
  }

  private val component =
    ScalaComponent
      .builder[Props]
      .initialState(State())
      .renderBackend[Backend]
      .build

  def apply(job: JobManifest, events: Seq[NfEvent]) = {
    component(Props(job, events))
  }
}
