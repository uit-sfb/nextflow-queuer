# NQS Server

This is the backend for NQS.

## Requirements

- Java 11 or later

## Getting started

Run `sbt run` (from this location) and the UI should be available at `http://localhost:9000`.
SBT is able to detect code change and compile on-the-fly.
