package models.json

import org.mongodb.scala.bson.Document
import play.api.libs.json._
import io.circe.{Encoder, Json => CircleJson}
import io.circe.parser._
import io.circe.generic.extras.auto._
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import no.uit.sfb.nqsshared.jsonapi._

import scala.util.Try

object JsonApi {

  implicit private val customConfig: Configuration =
    Configuration.default.withDefaults

  import BsonImplicit._

  implicit val config = JsonConfiguration(
    optionHandlers = OptionHandlers.WritesNull
  )

  //Just to make circe happy
  implicit val dummyEncoder: Encoder[Document] = new Encoder[Document] {
    final def apply(doc: Document): CircleJson =
      parse(doc.toJson()).toTry.get
  }

  implicit val mapOptionFormat = new Format[Map[String, Option[String]]] {
    def writes(obj: Map[String, Option[String]]): JsValue =
      Json.parse(obj.asJson.noSpaces)

    def reads(json: JsValue): JsResult[Map[String, Option[String]]] = {
      JsResult.fromTry(
        decode[Map[String, Option[String]]](json.toString()).toTry
      )
    }
  }

  implicit val formatDocument = new Format[Document] {
    def writes(doc: Document): JsValue = Json.parse(doc.toJson())

    def reads(json: JsValue): JsResult[Document] = {
      JsResult.fromTry(Try {
        Document(json.toString())
      })
    }
  }
  implicit val formatRelationship =
    Json.using[Json.WithDefaultValues].format[Relationship]
  implicit val formatRelationships =
    Json.using[Json.WithDefaultValues].format[Relationships]
  implicit val formatErr = Json.using[Json.WithDefaultValues].format[Err]
  implicit val formatErrorResponse =
    Json.using[Json.WithDefaultValues].format[ErrorResponse]
  implicit val formatDocumentData =
    Json.using[Json.WithDefaultValues].format[Data[Document]]

  implicit val formatDocumentSingleResponse =
    Json.using[Json.WithDefaultValues].format[SingleResponse[Document]]

  implicit val formatDocumentMultipleResponse =
    Json.using[Json.WithDefaultValues].format[MultipleResponse[Document]]
}
