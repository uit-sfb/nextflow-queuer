package models.auth.builder

import com.nimbusds.jose.JWSAlgorithm
import org.pac4j.http.client.direct.ParameterClient
import org.pac4j.oidc.config.OidcConfiguration
import org.pac4j.oidc.credentials.authenticator.UserInfoOidcAuthenticator
import play.api.Configuration

object JwtParamClientBuilder extends ClientBuilderLike {
  lazy val direct = true
  override def name(cfg: Configuration) = s"JwtParam${super.name(cfg)}"
  def apply(cfg: Configuration) = {
    val config = new OidcConfiguration
    config.setClientId(cfg.get[String]("id"))
    config.setSecret(cfg.get[String]("secret"))
    config.setDiscoveryURI(cfg.get[String]("discovery"))
    config.setPreferredJwsAlgorithm(JWSAlgorithm.RS256)
    config.setExpireSessionWithToken(false)
    //Set clock skew to 5 minutes as it can be an issue with the default value (30s)
    config.setMaxClockSkew(300)
    val authenticator = new UserInfoOidcAuthenticator(config)
    val client = new ParameterClient("access-token", authenticator)
    client.setSupportGetRequest(true)
    client.setName(name(cfg))
    client
  }
}
