package models.auth

import com.nimbusds.jose.shaded.json.JSONArray
import org.pac4j.core.profile.CommonProfile
import org.pac4j.oidc.profile.OidcProfile

import scala.util.Try

case class UserDetails(_id: String = "",
                       username: String = "",
                       name: String = "",
                       email: String = "",
                       country: String = "",
                       accessToken: Option[String] = None) {
  val id = _id
}

object UserDetails {
  type UserId = String

  def apply(p: CommonProfile): UserDetails = {
    val tokens = p match {
      case oidcProfile: OidcProfile =>
        Some(oidcProfile.getAccessToken)
      case _ => None
    }
    UserDetails(
      Option(p.getAttribute("sub").asInstanceOf[String])
        .orElse(Option(p.getAttribute("id").asInstanceOf[String]))
        .getOrElse(""),
      Option(p.getUsername).getOrElse(""),
      Option(p.getDisplayName).getOrElse(""),
      Option(p.getEmail).getOrElse(""),
      Option(p.getAttribute("country").asInstanceOf[JSONArray])
        .flatMap { x =>
          Try { x.toArray()(0).asInstanceOf[String] }.toOption
        }
        .getOrElse(""),
      tokens.map {
        _.getValue
      }
    )
  }
}
