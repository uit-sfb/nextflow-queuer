package models.utils

import akka.http.scaladsl.model.DateTime
import org.bson.types.ObjectId
import play.api.libs.json.{JsNull, JsObject, JsString, JsValue}

import scala.concurrent.duration.DurationInt
import scala.util.Try

object NextflowMetadata {
  protected def parseDate(json: JsValue): String = {
    val offset = (json \ "offset" \ "totalSeconds").as[Int].seconds
    val year = (json \ "year").as[Int]
    val month = (json \ "monthValue").as[Int]
    val day = (json \ "dayOfMonth").as[Int]
    val hour = (json \ "hour").as[Int]
    val minute = (json \ "minute").as[Int]
    val second = (json \ "second").as[Int]
    val date = DateTime(year, month, day, hour, minute, second) - offset.toMillis
    date.toIsoDateTimeString() + "Z"
  }

  def sanitize(json: JsValue): JsValue = {
    val clean = Try {
      val metadata = json \ "metadata"
      val workflow = metadata \ "workflow"
      val updatedWorkflow = {
        val shrunk = workflow
          .as[JsObject] - "workflowStats"
        val start = (workflow \ "start").toOption
          .map {
            parseDate
          }
        val complete = (workflow \ "complete").toOption
          .flatMap {
            case JsNull => None
            case o      => Some(parseDate(o))
          }
        val tmp = start match {
          case Some(s) => shrunk + ("start" -> JsString(s))
          case None    => shrunk
        }
        complete match {
          case Some(c) => tmp + ("complete" -> JsString(c))
          case None    => tmp
        }
      }
      val updatedMetadata = metadata
        .as[JsObject] + ("workflow" -> updatedWorkflow)
      json.as[JsObject] + ("metadata" -> updatedMetadata)
    }.getOrElse(json.as[JsObject])
    val withoutId = (clean \ "runName").toOption.map {
      _.as[String].split("_").init.mkString("_")
    } match {
      case Some(jobName) => clean + ("jobId" -> JsString(jobName))
      case None          => clean
    }
    withoutId + ("_id" -> JsString(ObjectId.get().toString))
  }
}
