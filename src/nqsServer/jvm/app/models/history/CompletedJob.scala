package models.history

case class CompletedJob(_id: String,
                        timestamp: Long,
                        userId: String,
                        country: String)
