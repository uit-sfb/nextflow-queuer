package models.presigned

import no.uit.sfb.nqsshared.utils.Date

import java.time.ZoneOffset

case class PresignedUrlForm(
  _id: String,
  jobId: String,
  `type`: String,
  keys: List[String],
  timeout: Long, //timeout timestamp
  remaining: Int //Number of times this presigned url can be used
) {
  lazy val isValid = remaining > 0 && timeout > Date
    .now()
    .toEpochSecond(ZoneOffset.UTC)
}
