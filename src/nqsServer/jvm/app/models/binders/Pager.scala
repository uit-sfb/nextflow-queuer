package models.binders

import play.api.mvc.QueryStringBindable

case class Pager(offset: Int = 0, size: Int = 20)

object Pager {
  implicit def queryStringBinder(implicit intBinder: QueryStringBindable[Int])
    : QueryStringBindable[Pager] = new QueryStringBindable[Pager] {
    override def bind(
        key: String,
        params: Map[String, Seq[String]]): Option[Either[String, Pager]] = {
      (intBinder
         .bind(key + "[offset]", params)
         .getOrElse(Right(Pager().offset)),
       intBinder
         .bind(key + "[size]", params)
         .getOrElse(Right(Pager().size))) match {
        case (Right(offset), Right(size)) => Some(Right(Pager(offset, size)))
        case _                            => Some(Left("Unable to bind a Pager"))
      }
    }
    override def unbind(key: String, pager: Pager): String = {
      intBinder.unbind(key + "[offset]", pager.offset) + "&" + intBinder
        .unbind(key + "[size]", pager.size)
    }
  }
}
