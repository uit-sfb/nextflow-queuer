package modules

import akka.Done
import akka.actor.CoordinatedShutdown
import modules.mongodb.MongodbConnect

import javax.inject._
import org.mongodb.scala.MongoDatabase
import play.api.Configuration

import scala.concurrent.{ExecutionContext, Future, blocking}

@Singleton
class MongodbClient @Inject()(cfg: Configuration, cs: CoordinatedShutdown)(
  implicit executionContext: ExecutionContext
) {
  val connection = MongodbConnect(
    cfg.get[String]("mongodb.host"),
    cfg.get[String]("mongodb.userName"),
    cfg.get[String]("mongodb.userPassword"),
    ssl = cfg.get[Boolean]("mongodb.ssl")
  )

  def listDbs(): Future[Seq[String]] = {
    connection.listDb
  }

  def db(dbName: String): MongoDatabase = {
    connection.db(dbName)
  }

  cs.addTask(
    CoordinatedShutdown.PhaseServiceStop,
    "Close connection to MongoDb"
  ) { () =>
    Future {
      blocking {
        connection.close
        Done
      }
    }
  }

  def checkClient(): Future[Unit] = {
    Future {
      blocking {
        connection.listDb
        ()
      }
    }
  }
}
