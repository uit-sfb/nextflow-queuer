package modules

import akka.stream.alpakka.file.TarArchiveMetadata
import akka.stream.alpakka.file.scaladsl.Archive
import akka.stream.scaladsl.{FileIO, Source}
import akka.util.ByteString
import no.uit.sfb.nqsshared.com.error._
import no.uit.sfb.nqsshared.models.{InputFileHandle, JobManifest}
import no.uit.sfb.nqsshared.utils.FileUtils

import java.nio.file.{Files, Path}
import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@Singleton
class FilesModule @Inject()(cfg: ConfigModule, jobQueueModule: JobQueueModule)(
  implicit executionContext: ExecutionContext
) {
  protected val workdir = cfg.workdir

  protected def checkInput(key: String) = {
    Try {
      assert(
        key.startsWith("inputs/"),
        s"Input file should have a key starting with 'inputs/'."
      )
    }.recover { e =>
      throw BadRequestClientError(e.getMessage)
    }.get
  }

  //Needed by the executor
  def downloadInputFile(jobId: String, key: String): Path = {
    checkInput(key)
    val j = JobManifest(jobId)
    val dst = workdir.resolve(j.filePath(key))
    if (!FileUtils.exists(dst))
      throw BadRequestClientError(
        s"No input file found for job '$jobId' with key '$key' ($dst)."
      )
    else
      dst
  }

  //Needed for the inline mode
  def downloadSingleFile(jobId: String, key: String): Path = {
    val j = JobManifest(jobId)
    val dst = workdir.resolve(j.filePath(key))
    if (!FileUtils.exists(dst))
      throw BadRequestClientError(
        s"No file found for job '$jobId' with key '$key' ($dst)."
      )
    else
      dst
  }

  def uploadInputFile(
    jobId: String,
    key: String,
    originalUrl: Option[String] = None
  ): Future[(JobManifest, Path)] = {
    def registerInput(jm: JobManifest): JobManifest = {
      if (jm.inputs.exists { _.key == key })
        throw BadRequestClientError(
          s"Key '$key' already exists for job '$jobId'."
        )
      else
        jm.copy(inputs = jm.inputs :+ InputFileHandle(key, originalUrl))
    }
    checkInput(key)
    jobQueueModule.updateFn(jobId, registerInput).map { doc =>
      val j = JobManifest(jobId)
      doc -> workdir.resolve(j.filePath(key))
    }
  }

  def deleteInputFile(jobId: String, key: String): Future[JobManifest] = {
    def unregisterInput(jm: JobManifest): JobManifest = {
      if (jm.inputs.exists { _.key == key })
        jm.copy(inputs = jm.inputs.filter {
          _.key != key
        })
      else
        throw BadRequestClientError(
          s"Key '$key' does not exists for job '$jobId'."
        )
    }
    checkInput(key)
    jobQueueModule.updateFn(jobId, unregisterInput).map { doc =>
      val j = JobManifest(jobId)
      val dst = workdir.resolve(j.filePath(key))
      val _dst = workdir.resolve(j.tmpFilePath(key))
      Files.deleteIfExists(dst)
      Files.deleteIfExists(_dst)
      doc
    }
  }

  protected def checkOutput(key: String) = {
    Try {
      assert(
        key.startsWith("outputs/") || key.startsWith("reports/"),
        s"Output file should have a key starting with 'outputs/' or 'reports/'."
      )
    }.recover { e =>
      throw BadRequestClientError(e.getMessage)
    }.get
  }

  def downloadFiles(jobId: String, keys: Seq[String]): Source[ByteString, _] = {
    val j = JobManifest(jobId)
    val paths: Seq[Path] = if (keys.isEmpty) {
      val dir = workdir.resolve(j.filesPath)
      FileUtils.filesUnder(dir)
    } else if (keys.head.contains("inputs/*")) {
      val dir = workdir.resolve(j.inputsPath)
      FileUtils.filesUnder(dir)
    } else if (keys.head.contains("outputs/*")) {
      val dir = workdir.resolve(j.outputsPath)
      FileUtils.filesUnder(dir)
    } else if (keys.head.contains("reports/*")) {
      val dir = workdir.resolve(j.reportsPath)
      FileUtils.filesUnder(dir)
    } else {
      keys.map { key =>
        if (key.contains(".."))
          throw BadRequestClientError(
            s"A key may not contain '..', but found: '$key'."
          )
        workdir.resolve(j.filePath(key))
      }
    }

    val streams = paths.map { p =>
      TarArchiveMetadata(
        "",
        s"$jobId/${workdir.resolve(j.filesPath).relativize(p).toString}",
        Files.size(p),
        Files.getLastModifiedTime(p).toInstant
      ) -> FileIO.fromPath(p)
    }

    Source(streams)
      .log("Archiving")
      .via(Archive.tar().via(akka.stream.scaladsl.Compression.gzip))
  }

  def uploadOutputFile(jobId: String,
                       key: String): Future[(JobManifest, Path)] = {
    def registerOutput(jm: JobManifest): JobManifest = {
      if (jm.outputs.contains(key))
        jm
      else
        jm.copy(outputs = jm.outputs :+ key)
    }

    checkOutput(key)
    jobQueueModule.updateFn(jobId, registerOutput).map { doc =>
      val j = JobManifest(jobId)
      doc -> workdir.resolve(j.filePath(key))
    }
  }
}
