package modules

import akka.stream.scaladsl.Source
import akka.util.ByteString
import models.presigned.PresignedUrlForm
import no.uit.sfb.nqsshared.com.error.NotFoundClientError

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import org.mongodb.scala.model.{Updates, Filters => F}

import java.nio.file.Path

@Singleton
class PresignedModule @Inject()(cfg: ConfigModule, filesModule: FilesModule)(
  implicit executionContext: ExecutionContext
) {
  protected val presignedUrlColl = {
    val collName = "presigned"
    cfg.mdb.collection[PresignedUrlForm](collName)
  }

  def generatePresignedUrl(puf: PresignedUrlForm,
                           host: String): Future[String] = {
    presignedUrlColl.insert(puf).map { _ =>
      s"$host/api/v1/presigned/${puf._id}"
    }
  }

  def consumePresignedUrl(
    presignedRef: String
  ): Future[Either[Path, (String, String, Source[ByteString, _])]] = {
    presignedUrlColl.getOne(F.eq("_id", presignedRef)).flatMap {
      case Some(puf) if puf.isValid =>
        presignedUrlColl
          .update(F.eq("_id", presignedRef), Updates.inc("remaining", -1), true)
          .map { _ =>
            val keys =
              if (puf.keys.isEmpty)
                Seq(s"${puf.`type`}/*")
              else
                puf.keys.map { k =>
                  s"${puf.`type`}/$k"
                }
            if (keys.size == 1 && !keys.head.endsWith("*")) {
              Left(filesModule.downloadSingleFile(puf.jobId, keys.head))
            } else {
              Right(
                (
                  puf.jobId,
                  puf.`type`,
                  filesModule.downloadFiles(puf.jobId, keys)
                )
              )
            }
          }
      case _ =>
        Future.failed(
          NotFoundClientError(
            s"Presigned url with reference '$presignedRef' has either expired or been consumed."
          )
        )
    }
  }

  def deletePresignedUrl(ref: String): Future[Unit] = {
    presignedUrlColl.delete(F.eq("_id", ref)).map { _ =>
      ()
    }
  }

  def deleteAll(jobId: String): Future[Unit] = {
    presignedUrlColl.delete(F.eq("jobId", jobId)).map { _ =>
      ()
    }
  }
}
