package modules

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser._
import io.circe.syntax._
import models.auth.UserDetails
import models.utils.NextflowMetadata
import no.uit.sfb.nqsshared.com.error.{
  BadRequestClientError,
  InternalServerErrorError
}
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.{Filters => F}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class UserModule @Inject()(cfg: ConfigModule)(
  implicit executionContext: ExecutionContext
) {
  implicit val customConfig: Configuration = Configuration.default.withDefaults

  val collName = "users"
  val coll = cfg.mdb
    .collection[Document](collName)

  def upsert(user: UserDetails): Future[Unit] = {
    val doc = Document(user.copy(accessToken = None).asJson.noSpaces)
    coll.replace(F.eq("_id", user._id), doc, true).map { _ =>
      ()
    }
  }

  def get(userId: String): Future[Option[UserDetails]] = {
    coll.getOne(F.eq("_id", userId)).map { oDoc =>
      oDoc.map { doc =>
        decode[UserDetails](doc.toJson()).toOption.getOrElse(
          throw InternalServerErrorError(
            s"Failed decoding UserDetails id '$userId'"
          )
        )
      }
    }
  }

  def delete(userId: String): Future[Unit] = {
    coll.delete(F.eq("_id", userId), true).map { _ =>
      ()
    }
  }
}
