package modules.auth

import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.{SecureAction, Security}
import play.api.mvc.AnyContent

trait SecurityLike extends Security[CommonProfile] {
  def authorizer: Authorizer
  def clients: String

  lazy val secure
    : SecureAction[CommonProfile, AnyContent, AuthenticatedRequest] = {
    Secure.apply(
      clients,
      authorizers = Seq(authorizer.authMode, "isOwner").mkString(",")
    )
  }

  //Use this ActionBuilder instead of Action when no Authorization is strictly needed
  lazy val open =
    authorizer.openActionBuilder(secure, Secure("AnonymousClient"))
}
