package modules.auth

import com.typesafe.scalalogging.LazyLogging
import modules.ConfigModule
import no.uit.sfb.nqsshared.com.error.{
  InternalServerErrorError,
  NotFoundClientError
}
import no.uit.sfb.nqsshared.models.JobManifest
import org.mongodb.scala.bson.Document
import org.pac4j.core.authorization.authorizer.Authorizer
import org.pac4j.core.context.WebContext
import org.pac4j.core.profile.UserProfile

import scala.concurrent.{Await, ExecutionContext, Future}
import org.mongodb.scala.model.{Filters => F}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser._
import org.pac4j.core.context.session.SessionStore

import java.util
import scala.concurrent.duration.DurationInt

class ResourceAuthorizer(cfg: ConfigModule)(implicit ex: ExecutionContext)
    extends Authorizer
    with LazyLogging {
  protected val collName = "jobs"
  protected val coll = cfg.mdb.collection[Document](collName)
  implicit val customConfig: Configuration = Configuration.default.withDefaults
  protected def get(jobId: String): Future[Option[Document]] = {
    val filter = F.eq("_id", jobId)
    coll.getOne(filter)
  }

  protected def belongsTo(jobId: String,
                          userId: Option[String]): Future[Boolean] = {
    get(jobId)
      .map {
        case Some(sourceDoc) =>
          decode[JobManifest](sourceDoc.toJson()).toOption match {
            case Some(job) =>
              job.belongsTo(userId)
            case None =>
              throw InternalServerErrorError(
                s"Failed decoding JobManifest id '$jobId'"
              )
          }
        case None =>
          throw NotFoundClientError(s"Job id '$jobId' does not exist.")
      }
  }

  protected def preAllow(jobId: String, userId: Option[String]): Boolean = {
    val f = belongsTo(jobId, userId)
    Await.result(f, 30.seconds)
  }

  override def isAuthorized(context: WebContext,
                            sessionStore: SessionStore,
                            profiles: util.List[UserProfile]): Boolean = {
    val userId =
      if (profiles.isEmpty) None
      else
        Option(profiles.get(0).getAttribute("sub").asInstanceOf[String])
          .orElse(
            Option(profiles.get(0).getAttribute("id").asInstanceOf[String])
          )
    val param = context.getRequestParameter("jobId")
    if (param.isEmpty)
      true
    else {
      val jobId = param.get()
      val allowed = preAllow(jobId, userId)
      if (!allowed)
        logger.warn(
          s"User ${userId.getOrElse("anonymous")} does not own job $jobId."
        )
      allowed
    }
  }
}
