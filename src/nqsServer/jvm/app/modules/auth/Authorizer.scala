package modules.auth

import models.auth.UserDetails
import models.auth.builder.ClientListBuilder
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers
import org.pac4j.core.profile.CommonProfile
import org.pac4j.play.scala.Pac4jScalaTemplateHelper
import play.api.mvc._
import play.api.{Configuration, Logging}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class Authorizer @Inject()(
  parser: BodyParsers.Default,
  implicit val ec: ExecutionContext,
  implicit val pac4jTemplateHelper: Pac4jScalaTemplateHelper[CommonProfile],
  implicit val cfg: Configuration
) extends Logging {
  protected val clientBuilder = new ClientListBuilder(cfg)

  //Attention: setting header X-Requested-With: XMLHttpRequest disables direct clients
  //Attention: unless one has already logged in and a profile has been created, AnonymousClient will always have precedence over indirect clients
  //This means that if you want to force using a direct client, you need to remove AnonymousClient from the client list.
  val clients = clientBuilder.clients.mkString(",")
  val uiClients = clientBuilder.indirectClients.mkString(",")
  val apiClients = clientBuilder.directClients.mkString(",")

  val clientsWithoutDefault = {
    val tmp =
      clientBuilder.clients.filter { _ != "AnonymousClient" }.mkString(",")
    if (tmp.isEmpty)
      "AnonymousClient"
    else
      tmp
  }

  val bypassSecurity = clientBuilder.directClients == Seq("AnonymousClient")

  //An authorizer is always needed when using an indirect client, otherwise pac4j uses automatically csrf
  //Of course, if we were to handle csrf properly we would not need this hack
  val authMode =
    if (bypassSecurity)
      DefaultAuthorizers.NONE
    else
      DefaultAuthorizers.IS_AUTHENTICATED

  //Checks the header "Authorization" to decide whether to use Action or Secure
  def openActionBuilder(
    secureActionBuilder: ActionBuilder[Request, AnyContent],
    insecureActionBuilder: ActionBuilder[Request, AnyContent]
  ) =
    new ActionBuilderImpl[AnyContent](parser) with Logging {
      override def invokeBlock[A](
        request: Request[A],
        block: Request[A] => Future[Result]
      ): Future[Result] = {
        val actionBuilder =
          if (request.headers.hasHeader("Authorization"))
            secureActionBuilder
          else
            insecureActionBuilder
        actionBuilder.invokeBlock[A](request, block)
      }
    }

  def isLoggedIn(implicit request: RequestHeader) =
    user.nonEmpty

  def user(implicit request: RequestHeader): Option[UserDetails] = {
    pac4jTemplateHelper.getCurrentProfile
      .map { UserDetails(_) }
      .flatMap { user =>
        if (user._id.isEmpty) {
          if (bypassSecurity)
            Some(user.copy(_id = "anonymous"))
          else
            None
        } else
          Some(user)
      }
  }
}
