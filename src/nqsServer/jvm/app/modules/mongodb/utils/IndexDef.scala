package modules.mongodb.utils

import modules.mongodb.Collection

case class IndexDef(keyPath: String,
                    idx: Collection.IndexLike,
                    unique: Boolean = false)
