package modules.mongodb

import com.mongodb.MongoCredential.createCredential
import models.presigned.PresignedUrlForm
import org.bson.codecs.configuration.CodecRegistries.{
  fromProviders,
  fromRegistries
}
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.connection.SslSettings
import org.mongodb.scala.{
  MongoClient,
  MongoClientSettings,
  MongoCredential,
  MongoDatabase,
  ServerAddress
}

import scala.jdk.CollectionConverters._
import scala.concurrent.{Future, blocking}

class MongodbConnect(mongoClient: MongoClient) {
  val codecRegistry = fromRegistries(
    fromProviders(classOf[PresignedUrlForm]),
    DEFAULT_CODEC_REGISTRY
  )

  def listDb: Future[Seq[String]] =
    blocking {
      mongoClient.listDatabaseNames().toFuture()
    }

  def db(dbName: String): MongoDatabase = {
    mongoClient.getDatabase(dbName).withCodecRegistry(codecRegistry)
  }

  lazy val close: Unit = mongoClient.close()
}

object MongodbConnect {
  def apply(host: String = "localhost:27017",
            user: String = "admin",
            password: String = "salvador",
            adminDb: String = "admin",
            ssl: Boolean = false): MongodbConnect = {
    val credential: MongoCredential =
      createCredential(user, adminDb, password.toCharArray)
    val settings = MongoClientSettings
      .builder()
      .applyToSslSettings(
        (builder: SslSettings.Builder) => builder.enabled(ssl)
      )
      .applyToClusterSettings(
        b => b.hosts(List(new ServerAddress(host)).asJava)
      )
      .credential(credential)
      .applicationName("Datasets")
      .build()
    val mongoClient: MongoClient = MongoClient(settings)
    new MongodbConnect(mongoClient)
  }
}
