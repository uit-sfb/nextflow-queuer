package modules.mongodb

import com.mongodb.bulk.BulkWriteResult
import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.nqsshared.com.error.BadGatewayError
import modules.mongodb.utils.{BulkWriteResult2, IndexDef}
import org.bson.conversions.Bson
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.Indexes._
import org.mongodb.scala.model._
import org.mongodb.scala.result._
import org.mongodb.scala.{BulkWriteResult, FindObservable, MongoCollection}

import scala.collection.immutable.ListMap
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future, blocking}
import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag

class Collection[T: ClassTag](col: MongoCollection[T])(
  implicit ec: ExecutionContext
) extends LazyLogging {
  def listIndexes(): Future[Seq[Document]] = blocking {
    col.listIndexes().toFuture()
  }

  //Use replace() with upsert option if want to insert or upsert
  def insert(doc: T): Future[InsertOneResult] = {
    blocking {
      col.insertOne(doc).toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  def insert(docs: Seq[T]): Future[InsertManyResult] = {
    blocking {
      col.insertMany(docs).toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /** Counts the number of documents optionally satisfying a filter
    *
    * @param filter A Bson filter. If None, the estimatedDocumentCount method is used instead
    * @return
    */
  def count(filter: Bson = Document.empty): Future[Long] = {
    blocking {
      (if (filter == Document.empty)
         col.estimatedDocumentCount()
       else
         col.countDocuments(filter))
        .toFuture()
    }
  }

  /*
  Note: creating an already existing index is a no-op
   */
  def createCompoundIndex(keys: ListMap[String, Collection.IndexLike],
                          unique: Boolean = false,
                          name: Option[String] = None): Future[String] = {
    val bsonSpec: Bson = {
      val specs = keys map {
        case (k, indexType) =>
          indexType.bson(k)
      }
      compoundIndex(specs.toSeq: _*)
    }
    blocking {
      col
        .createIndex(
          bsonSpec,
          new IndexOptions().unique(unique).name(name.orNull)
        )
        .toFuture()
    }
  }

  def createIndexes(keys: Iterable[IndexDef]): Future[Seq[String]] = {
    Future
      .sequence(
        keys
          .map {
            case IndexDef(k, indexType, unique) =>
              val opt = new IndexOptions().unique(unique)
              indexType.bson(k) -> opt
          }
          .map {
            case (idx, opt) =>
              col.createIndex(idx, opt).toFuture()
          }
      )
      .map {
        _.toList
      }
  }

  //Attention, it is the index name that has to be used, not the key it is based on
  def dropIndex(indexName: String): Future[Any] = {
    blocking {
      col.dropIndex(indexName).toFuture()
    }
  }

  protected def getImpl(filter: Bson = Document.empty,
                        sort: Bson = Document.empty,
                        project: Bson = Document.empty,
                        collation: Collation = Collation.builder().build,
                        skip: Option[Int] = None,
                        limit: Option[Int] = None,
                        allowDiskUse: Boolean = false,
                        maxTime: Duration = 0.second): FindObservable[T] = {
    val tmp = col
      .find(filter)
      .sort(sort)

    val tmp2 = skip match {
      case Some(s) =>
        tmp.skip(s)
      case _ => tmp
    }

    val tmp3 = limit match {
      case Some(l) =>
        tmp2.limit(l)
      case _ => tmp2
    }

    tmp3
      .projection(project)
      .collation(collation)
      .allowDiskUse(allowDiskUse)
      .maxTime(maxTime)
  }

  def getAll(): Future[Seq[T]] = {
    get()
  }

  def getAllIt(): Iterator[T] = {
    getIt()
  }

  /**
    *
    * @param filter    Build with org.mongodb.scala.model.Filters
    * @param sort      Build with org.mongodb.scala.model.Sorts
    * @param project   Build with org.mongodb.scala.model.Projections
    * @param collation Build with org.mongodb.scala.model.PCollation
    * @param skip      Number of documents to skip
    * @param limit     Max number of documents returned
    * @return
    */
  def get(filter: Bson = Document.empty,
          sort: Bson = Document.empty,
          project: Bson = Document.empty,
          collation: Collation = Collation.builder().build,
          skip: Option[Int] = None,
          limit: Option[Int] = None,
          allowDiskUse: Boolean = false,
          maxTime: Duration = 0.second): Future[Seq[T]] = {
    blocking {
      getImpl(
        filter,
        sort,
        project,
        collation,
        skip,
        limit,
        allowDiskUse,
        maxTime
      ).toFuture()
    }
  }

  def getIt(filter: Bson = Document.empty,
            sort: Bson = Document.empty,
            project: Bson = Document.empty,
            collation: Collation = Collation.builder().build,
            skip: Option[Int] = None,
            limit: Option[Int] = None): Iterator[T] = {
    getItSeq(filter, sort, project, collation, skip, limit).flatten
  }

  //Do not even try to implement an Observer with Iterator!
  //Not only all the onXxx methods are asynchronous,
  //but I also get a memory leak I cannot fix!
  protected def getItSeq(filter: Bson = Document.empty,
                         sort: Bson = Document.empty,
                         project: Bson = Document.empty,
                         collation: Collation = Collation.builder().build,
                         skip: Option[Int] = None,
                         limit: Option[Int] = None,
                         batchSize: Int = 1000,
                         allowDiskUse: Boolean = false,
                         maxTime: Duration = 0.second): Iterator[Seq[T]] = {
    var start = 0
    Iterator
      .continually {
        val delta = limit.getOrElse(Int.MaxValue) - start
        if (delta <= 0)
          Seq()
        else {
          val l =
            if (delta > batchSize)
              batchSize
            else
              delta
          val res = blocking {
            Await.result(
              getImpl(
                filter,
                sort,
                project,
                collation,
                Some(start + skip.getOrElse(0)),
                Some(l),
                allowDiskUse,
                maxTime
              ).toFuture(),
              30.seconds
            )
          }
          start = start + l //If we get less items than batchSize it means that we reached the end of the list anyway, so it doesn't matter if we overshoot
          res
        }
      }
      .takeWhile { r =>
        r.nonEmpty
      }
  }

  def getOne(filter: Bson = Document.empty,
             sort: Bson = Document.empty,
             project: Bson = Document.empty,
             collation: Collation = Collation.builder().build(),
             skip: Option[Int] = None,
             limit: Option[Int] = None,
             allowDiskUse: Boolean = false,
             maxTime: Duration = 0.second): Future[Option[T]] = {
    blocking {
      getImpl(
        filter,
        sort,
        project,
        collation,
        skip,
        limit,
        allowDiskUse,
        maxTime
      ).headOption()
    }
  }

  //Build with import org.mongodb.scala.model.Aggregates
  //Limit of 16MB, unless allowDiskUse is set but then it is much slower, or use out to export data
  def aggregate(pipelines: Seq[Bson],
                allowDiskUse: Boolean = false,
                maxTime: Duration = 0.second): Future[Seq[T]] = {
    blocking {
      col
        .aggregate(pipelines)
        .allowDiskUse(allowDiskUse)
        .maxTime(maxTime)
        .toFuture()
    }
  }

  //Works better with very large datasets, but written in Javascript
  def mapReduce(mapFunction: String,
                reduceFunction: String,
                maxTime: Duration = 0.second): Future[Seq[T]] = {
    blocking {
      col
        .mapReduce(mapFunction, reduceFunction)
        .maxTime(maxTime)
        .toFuture()
    }
  }

  def drop(): Future[Any] = blocking {
    col.drop().toFuture()
  }

  /**
    * @param filter Build import org.mongodb.scala.model.Filters
    * @param single Apply to only one document
    * @return
    */
  def delete(filter: Bson, single: Boolean = false): Future[DeleteResult] = {
    val func =
      if (single)
        col.deleteOne(filter)
      else
        col.deleteMany(filter)
    blocking {
      func.toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /**
    *
    * @param filter Build with org.mongodb.scala.model.Filters
    * @param update Build with org.mongodb.scala.model.Updates
    * @param single Apply to only one document
    * @param upsert If true, inserts the document if it does not exist
    * @return
    */
  def update(filter: Bson,
             update: Bson,
             single: Boolean = false,
             upsert: Boolean = false): Future[UpdateResult] = {
    val func =
      if (single)
        col.updateOne(filter, update, UpdateOptions().upsert(upsert))
      else
        col.updateMany(filter, update, UpdateOptions().upsert(upsert))
    blocking {
      func.toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /**
    * Replace ONE
    *
    * @param filter      Build with org.mongodb.scala.model.Filters
    * @param replacement Replacement document
    * @param upsert      If true, inserts the document if it does not exist
    * @return
    */
  def replace(filter: Bson,
              replacement: T,
              upsert: Boolean = false): Future[UpdateResult] = {
    blocking {
      col
        .replaceOne(filter, replacement, ReplaceOptions().upsert(upsert))
        .toFuture()
    } map { res =>
      if (res.wasAcknowledged())
        logger.info(res.toString)
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  /** bulk write
    *
    * @param writes List built with XxxOneModel (ex: InsertOneModel)
    * @return
    */
  def bulkWrite(writes: Seq[WriteModel[_ <: T]],
                ordered: Boolean = false): Future[BulkWriteResult] = {
    val f =
      if (writes.nonEmpty) {
        blocking {
          col
            .bulkWrite(writes, BulkWriteOptions().ordered(ordered))
            .toFuture()
        }
      } else
        Future.successful(
          BulkWriteResult
            .acknowledged(0, 0, 0, 0, List().asJava, List().asJava)
        )
    f map { res =>
      if (res.wasAcknowledged())
        logger.info(new BulkWriteResult2(res).toString())
      else
        throw BadGatewayError(res.toString)
      res
    }
  }

  //findOneAndDelete/Update/Replace
}

object Collection {

  sealed trait IndexLike {
    def bson(str: String): Bson
  }

  case object Asc extends IndexLike {
    def bson(str: String) = ascending(str)
  }

  case object Desc extends IndexLike {
    def bson(str: String) = descending(str)
  }

  case object Text extends IndexLike {
    def bson(str: String) = text(str)
  }
}
