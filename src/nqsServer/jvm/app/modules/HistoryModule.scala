package modules

import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser._
import io.circe.syntax._
import models.auth.UserDetails
import models.history.CompletedJob
import no.uit.sfb.nqsshared.com.error.InternalServerErrorError
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.{Filters => F}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HistoryModule @Inject()(cfg: ConfigModule)(
  implicit executionContext: ExecutionContext
) {
  implicit val customConfig: Configuration = Configuration.default.withDefaults

  val collName = "history"
  val coll = cfg.mdb
    .collection[Document](collName)

  def insert(item: CompletedJob): Future[Unit] = {
    val doc = Document(item.asJson.noSpaces)
    coll.insert(doc).map { _ =>
      ()
    }
  }
}
