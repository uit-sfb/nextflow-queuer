package modules.actors

import akka.actor._
import io.circe.generic.extras.Configuration
import io.circe.parser.decode
import no.uit.sfb.nqsshared.com.error.{
  InternalServerErrorError,
  NotFoundClientError
}
import no.uit.sfb.nqsshared.models.JobManifest
import modules.mongodb.Database
import org.mongodb.scala.bson.Document
import io.circe.syntax._
import io.circe.generic.extras.auto._
import org.mongodb.scala.model.{Filters => F, Sorts}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.Try

object SyncActor {
  //ATTENTION: this seems to have limitations!
  def props(mdb: Database, ec: ExecutionContext) =
    Props[SyncActor](new SyncActor(mdb, ec))

  case class Pop(clientId: String)
  case class Update(jobId: String, fn: JobManifest => JobManifest)
}

class SyncActor(mdb: Database, implicit val ec: ExecutionContext)
    extends Actor {
  import SyncActor._

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  val collName = "jobs"
  val coll = mdb.collection[Document](collName)

  //NOTE: the actor should never throw an exception. Otherwise sender() is not called and the request simply times out.
  //That is why we return a Try
  def receive = {
    case Pop(clientId) =>
      val f: Future[Option[JobManifest]] =
        coll
          .getOne(
            F.eq("currentState", "pending"),
            sort = Sorts.ascending("timeline.0.timestamp")
          )
          .flatMap {
            case Some(doc) =>
              val jm = decode[JobManifest](doc.toJson()).toOption
                .getOrElse(
                  throw InternalServerErrorError(
                    s"Failed decoding JobManifest id '${doc.get("_id")}'"
                  )
                )
              //It is important to update to initializing here since it is what ensures that a job will not be pulled by two clients
              val updatedJm = jm
                .copy(lastClient = Some(clientId))
                .update("initializing", _ => ())
              coll
                .replace(
                  F.eq("_id", updatedJm.jobId),
                  Document(updatedJm.asJson.noSpaces)
                )
                .map { _ =>
                  Some(updatedJm)
                }
            case None => Future.successful(None)
          }
      sender() ! Try { Await.result(f, 30.seconds) } //We have no choice but wait here because of the synchronization
    case Update(jobId, fn) =>
      val f: Future[JobManifest] = {
        val filter = F.eq("_id", jobId)
        coll.getOne(filter).flatMap {
          case Some(doc) =>
            val updatedJm = decode[JobManifest](doc.toJson()).toOption match {
              case Some(jm) =>
                fn(jm)
              case None =>
                throw InternalServerErrorError(
                  s"Failed decoding JobManifest id '$jobId'"
                )
            }
            val updatedDoc = Document(updatedJm.asJson.noSpaces)
            coll
              .replace(F.eq("_id", updatedJm.jobId), updatedDoc)
              .map { _ =>
                updatedJm
              }
          case None =>
            throw NotFoundClientError(s"Job id '$jobId' does not exist.")
        }
      }
      sender() ! Try { Await.result(f, 30.seconds) } //We have no choice but wait here because of the synchronization
  }
}
