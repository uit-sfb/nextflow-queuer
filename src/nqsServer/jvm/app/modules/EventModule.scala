package modules

import models.utils.NextflowMetadata
import no.uit.sfb.nqsshared.com.error.BadRequestClientError
import org.mongodb.scala.bson.Document
import play.api.libs.json.JsValue
import org.mongodb.scala.model.{Filters => F}

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class EventModule @Inject()(cfg: ConfigModule)(
  implicit executionContext: ExecutionContext
) {
  val collName = "events"
  val coll = cfg.mdb
    .collection[Document](collName)

  def list(jobId: String, runName: Option[String]): Future[Seq[Document]] = {
    val filters = Seq(Some(F.eq("jobId", jobId)), runName.map {
      F.eq("runName", _)
    }).flatten
    val filter = if (filters.isEmpty) F.empty() else F.and(filters: _*)
    coll.get(filter)
  }

  def push(event: Option[JsValue]): Future[Unit] = {
    event match {
      case Some(json) =>
        val doc = Document(NextflowMetadata.sanitize(json).toString())
        coll.insert(doc).map { _ =>
          ()
        }
      case None => throw BadRequestClientError(s"JSON content expected.")
    }
  }

  def delete(jobId: String): Future[Unit] = {
    coll.delete(F.eq("jobId", jobId)).map { _ =>
      ()
    }
  }
}
