package modules

import com.typesafe.scalalogging.LazyLogging
import modules.mongodb.Database
import play.api.Configuration

import java.nio.file.Paths
import javax.inject._
import scala.concurrent.ExecutionContext

@Singleton
class ConfigModule @Inject()(cfg: Configuration, mongoClient: MongodbClient)(
  implicit executionContext: ExecutionContext
) extends LazyLogging {
  val id = cfg.get[String]("app.id")
  val dbName = cfg.get[String]("app.dbName")
  val workdir = Paths.get(cfg.get[String]("app.workdir"))
  val selfEndpoint = cfg.getOptional[String]("app.selfEndpoint").getOrElse("")
  val prefixPath = {
    val p = cfg.get[String]("play.http.context")
    if (p.endsWith("/"))
      p.dropRight(1)
    else
      p
  }
  val mailer = cfg.get[String]("play.mailer.host")
  val helpdesk = cfg.get[String]("app.helpdesk")
  val mailerFrom =
    if (mailer.isEmpty) ""
    else {
      val tmp = cfg.get[String]("app.emailFrom")
      if (tmp.isEmpty)
        (id +: mailer.split('.').tail).mkString(".")
      else
        tmp
    }
  val selfUrl = selfEndpoint + prefixPath
  lazy val mdb = new Database(mongoClient.connection.db(dbName))
  val maxDiskUsagePerUser =
    cfg
      .getOptional[Long]("app.maxDiskUsagePerUserGb")
      .map { _ * 1024 * 1024 * 1024 }
      .getOrElse(-1L)
  val maxDiskUsagePerUserGb =
    cfg.getOptional[Int]("app.maxDiskUsagePerUserGb").getOrElse(-1)
  logger.info(s"""
       |dbName: '$dbName'
       |workdir: '$workdir'
       |selfUrl: '$selfUrl'
       |mailer: '$mailer'
       |mailerFrom: '$mailerFrom'
       |maxDiskUsagePerUserGb: '$maxDiskUsagePerUserGb'
       |""".stripMargin)
}
