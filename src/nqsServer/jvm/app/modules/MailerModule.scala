package modules

import play.api.Logging
import play.api.libs.mailer._

import javax.inject.{Inject, Singleton}
import scala.util.Try

@Singleton
class MailerModule @Inject()(mailerClient: MailerClient, conf: ConfigModule)
    extends Logging {

  def send(receiver: String, title: String, content: String = "") = {
    if (conf.mailer.nonEmpty) {
      val email = Email(
        title,
        s"noreply@${conf.mailerFrom}",
        Seq(s"TO <$receiver>"),
        bodyText = Some(
          s"""Hi,
             |
             |$content
             |${if (conf.helpdesk.nonEmpty)
               s"\nYou cannot answer directly to this email, but you may contact us for assistance at mailto:${conf.helpdesk}."
             else ""} 
             |
             |Best regards,
             |UiT@elixir.no team
             |""".stripMargin
        )
      )
      Try {
        mailerClient.send(email)
      } recover {
        case e =>
          logger.warn("Could not send email", e)
      }
    }
  }
}
