package modules.tasks

import akka.Done
import akka.actor.{ActorSystem, CoordinatedShutdown}
import models.presigned.PresignedUrlForm
import modules.ConfigModule
import no.uit.sfb.nqsshared.utils.Date
import play.api.inject._

import javax.inject.Inject
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import org.mongodb.scala.model.{Filters => F}

import java.time.ZoneOffset

// a periodic process to do clean-up tasks
class JanitorTask @Inject()(cfg: ConfigModule,
                            actorSystem: ActorSystem,
                            executor: TasksCustomExecutionContext,
                            cs: CoordinatedShutdown) {
  protected val presignedUrlColl = {
    val collName = "presigned"
    cfg.mdb.collection[PresignedUrlForm](collName)
  }
  val handle = actorSystem.scheduler
    .scheduleWithFixedDelay(initialDelay = 10.seconds, delay = 10.seconds)({
      () =>
        //actorSystem.log.debug("Executing janitor task")
        presignedUrlColl.delete(
          F.or(
            F.lte("remaining", 0),
            F.lte("timeout", Date.now().toEpochSecond(ZoneOffset.UTC))
          )
        )
    })(executor) // using the custom execution context

  cs.addTask(CoordinatedShutdown.PhaseServiceUnbind, "stop-janitor-task") {
    () =>
      Future.successful {
        handle.cancel()
        Done
      }
  }
}

class JanitorTasksModule
    extends SimpleModule(bind[JanitorTask].toSelf.eagerly())
