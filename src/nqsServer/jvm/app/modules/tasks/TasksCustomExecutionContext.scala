package modules.tasks

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext

import javax.inject.Inject

class TasksCustomExecutionContext @Inject()(actorSystem: ActorSystem)
    extends CustomExecutionContext(
      actorSystem,
      "play.akka.actor.tasks-dispatcher"
    )
