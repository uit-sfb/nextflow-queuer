package modules

import no.uit.sfb.nqsshared.com.error._
import no.uit.sfb.nqsshared.models.{JobManifest, JobSpec}
import org.mongodb.scala.bson.Document
import org.mongodb.scala.model.{Filters => F}
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser._
import io.circe.syntax._
import models.history.CompletedJob
import no.uit.sfb.nqsshared.utils.{Date, FileUtils}
import org.bson.types.ObjectId
import play.api.Logging
import play.api.mvc._

import java.nio.file.Files
import java.time.ZoneOffset
import javax.inject._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

@Singleton
class JobModule @Inject()(cfg: ConfigModule,
                          eventModule: EventModule,
                          presignedModule: PresignedModule,
                          jobQueueModule: JobQueueModule,
                          userModule: UserModule,
                          historyModule: HistoryModule,
                          mailer: MailerModule,
)(implicit executionContext: ExecutionContext)
    extends Logging {
  implicit val customConfig: Configuration = Configuration.default.withDefaults
  protected val coll = {
    val collName = "jobs"
    cfg.mdb.collection[Document](collName)
  }

  protected val workdir = cfg.workdir

  def list(jobId: Option[String],
           state: Option[String],
           userId: Option[String]): Future[Seq[Document]] = {
    val filters = Seq(Some(F.ne("currentState", "deleting")), jobId.map {
      F.eq("_id", _)
    }, state.map {
      F.eq("currentState", _)
    }, userId.map {
      F.eq("submitter", _)
    }).flatten
    val filter = if (filters.isEmpty) F.empty() else F.and(filters: _*)
    coll.get(filter)
  }

  def listDeleting(clientId: String): Future[Seq[Document]] = {
    val filter =
      F.and(F.eq("lastClient", clientId), F.eq("currentState", "deleting"))
    coll.get(filter)
  }

  def detectOrphans(clientId: String): Future[Seq[JobManifest]] = {
    val filter =
      F.and(
        F.eq("lastClient", clientId),
        F.in("currentState", "initializing", "running", "finalizing")
      )
    coll
      .get(filter)
      .map {
        _.map {
          _.getString("_id")
        }
      }
      .flatMap { ids =>
        Future.sequence(ids.map { updateState(_, "pending") })
      }
  }

  def get(jobId: String): Future[Option[Document]] = {
    val filter = F.eq("_id", jobId)
    coll.getOne(filter)
  }

  protected def newId() =
    cfg.id + ObjectId
      .get()
      .toString //Needs to start with a small letter to make Nextflow happy

  //In case you wanted to recover the original ObjectId (to get the timestamp it encompasses for instance)
  protected def Id2ObjectId(id: String) = new ObjectId(id.stripPrefix(cfg.id))

  def belongsTo(jobId: String, userId: Option[String]): Future[Boolean] = {
    get(jobId)
      .map {
        case Some(sourceDoc) =>
          decode[JobManifest](sourceDoc.toJson()).toOption match {
            case Some(job) =>
              job.belongsTo(userId)
            case None =>
              throw InternalServerErrorError(
                s"Failed decoding JobManifest id '$jobId'"
              )
          }
        case None =>
          throw NotFoundClientError(s"Job id '$jobId' does not exist.")
      }
  }

  def newDraft(project: String, submittedFrom: String, userId: String)(
    implicit request: Request[AnyContent]
  ) = {
    val jm = JobManifest(
      newId(),
      spec = JobSpec(version = ""),
      project = project,
      submittedFrom =
        if (submittedFrom.isEmpty) JobManifest("").submittedFrom
        else submittedFrom,
      submitter = userId,
      //server = cfg.selfUrl not needed since the clients are able to fill this themselves
    )
    val doc = Document(jm.asJson.noSpaces)
    coll
      .insert(doc)
      .flatMap { res =>
        val id = res.getInsertedId.asString().getValue
        get(id)
      }
      .map {
        case Some(doc) => doc
        case None =>
          throw InternalServerErrorError(s"Failed to find newly created job.")
      }
  }

  def newDraftFromCopy(origJobId: String,
                       userId: String,
                       deleteOrig: Boolean): Future[Document] = {
    get(origJobId)
      .map {
        case Some(sourceDoc) =>
          decode[JobManifest](sourceDoc.toJson()).toOption match {
            case Some(orig) =>
              orig -> orig.copy(
                _id = newId(),
                currentState = "draft",
                outputs = Seq(),
                timeline = Seq(),
                submitter = userId
              )
            case None =>
              throw InternalServerErrorError(
                s"Failed decoding JobManifest id '$origJobId'"
              )
          }
        case None =>
          throw NotFoundClientError(s"Job id '$origJobId' does not exist.")
      }
      .flatMap {
        case (orig, jm) =>
          val doc = Document(jm.asJson.noSpaces)
          coll
            .insert(doc)
            .flatMap { res =>
              val id = res.getInsertedId.asString().getValue
              get(id)
            }
            .flatMap {
              case Some(doc) =>
                //Hard link inputs files
                jm.inputs
                  .map {
                    _.key
                  }
                  .foreach { k =>
                    val origPath = workdir.resolve(orig.filePath(k))
                    val newPath = workdir.resolve(jm.filePath(k))
                    Files.createDirectories(newPath.getParent)
                    Files.createLink(newPath, origPath)
                  }
                (if (deleteOrig)
                   markDelete(origJobId)
                 else
                   Future.successful(())).map { _ =>
                  doc
                }
              case None =>
                throw InternalServerErrorError(
                  s"Failed to find newly created job."
                )
            }
      }
  }

  def submit(jobId: String): Future[JobManifest] = {
    checkJobState(jobId, "draft")
    get(jobId)
      .flatMap {
        case Some(doc) =>
          decode[JobManifest](doc.toJson()).toOption match {
            case Some(jm) =>
              if (jm.currentState == "draft") {
                if (jm.readyForPush(workdir))
                  updateState(jm.jobId, "pending")
                else
                  throw ConflictClientError(
                    s"Cannot publish job '$jobId' as some inputs are not yet fully downloaded."
                  )
              } else
                throw ConflictClientError(
                  s"Cannot publish job '$jobId' as it is not a draft (state is '${jm.currentState}')."
                )
            case None =>
              throw InternalServerErrorError(
                s"Failed decoding JobManifest id '$jobId'"
              )
          }
        case None =>
          throw NotFoundClientError(s"Draft '$jobId' does not exist.")
      }
  }

  def updateDraftSpec(jobId: String, newSpec: Document): Future[JobManifest] = {
    decode[JobSpec](newSpec.toJson()).toTry match {
      case Success(newSpec) =>
        jobQueueModule.updateFn(jobId, { jm =>
          jm.copy(spec = newSpec)
        })
      case Failure(e) =>
        throw BadRequestClientError(
          s"Could not parse update data due to: ${e.getMessage}"
        )
    }
  }

  def updateState(jobId: String, state: String): Future[JobManifest] = {
    def action(jm: JobManifest) = {
      lazy val titlePrefix = s"${jm.project} job".capitalize
      lazy val url = s"${jm.submittedFrom}"
      userModule.get(jm.submitter).map {
        case Some(user) =>
          state match {
            //We do not want to notify orphans (i.e. current state is not draft)
            case "pending" if jm.currentState == "draft" =>
              val content =
                s"""$titlePrefix has been submitted to our queuing system. You can access it here: $url.
          |You will receive an email once the job has been executed (it may take some time).""".stripMargin
              mailer
                .send(user.email, s"$titlePrefix submitted".capitalize, content)
            case s @ "completed" =>
              val content =
                s"$titlePrefix was successfully executed. You can access it here: $url."
              historyModule.insert(
                CompletedJob(
                  jm.jobId,
                  Date.now().toEpochSecond(ZoneOffset.UTC),
                  jm.submitter,
                  user.country
                )
              )
              mailer.send(user.email, s"$titlePrefix $s".capitalize, content)
            case s @ "canceled" =>
              val content =
                s"$titlePrefix was canceled. You can access it here: $url."
              mailer.send(user.email, s"$titlePrefix $s".capitalize, content)
            case s @ "failed" =>
              val content =
                s"$titlePrefix failed. You can access it here: $url."
              mailer.send(user.email, s"$titlePrefix $s".capitalize, content)
            case _ =>
              logger
                .info(s"Could not find '${jm.submitter}' in users collection.")
          }
        case None => ()
      }
    }
    jobQueueModule.updateFn(jobId, _.update(state, action))
  }

  def checkJobState(jobId: String, state: String): Unit = {
    checkJobState(jobId, Seq(state))
  }

  def checkJobState(jobId: String, states: Seq[String]): Unit = {
    val f = get(jobId)
      .map {
        case Some(doc) =>
          decode[JobManifest](doc.toJson()).toOption match {
            case Some(jm) if states.contains(jm.currentState) =>
              ()
            case Some(jm) =>
              throw ConflictClientError(
                s"'$jobId' is in state '${jm.currentState}', which is not one of the required states for this action (one of: '${states
                  .mkString(",")}')."
              )
            case None =>
              throw InternalServerErrorError(
                s"Failed decoding JobManifest id '$jobId'"
              )
          }
        case None =>
          throw NotFoundClientError(s"Job id '$jobId' does not exist.")
      }
    Await.result(f, 30.seconds)
  }

  def isCanceled(jobId: String) = {
    get(jobId).map {
      _.exists { doc =>
        doc.getString("currentState") == "canceled"
      }
    }
  }

  def isDeleted(jobId: String) = {
    get(jobId).map {
      _.isEmpty
    }
  }

  def deleteDraft(jobId: String): Future[Unit] = deleteJob(jobId)

  def markDelete(jobId: String): Future[Unit] = {
    eventModule.delete(jobId).flatMap { _ =>
      val jm = JobManifest(jobId)
      FileUtils.deleteDirIfExists(workdir.resolve(jm.jobPath))
      updateState(jobId, "deleting").map { _ =>
        ()
      }
    }
  }

  def deleteJob(jobId: String): Future[Unit] = {
    eventModule.delete(jobId).zip(presignedModule.deleteAll(jobId)).flatMap {
      _ =>
        val jm = JobManifest(jobId)
        FileUtils.deleteDirIfExists(workdir.resolve(jm.jobPath))
        coll.delete(F.eq("_id", jobId)).map { r =>
          if (r.getDeletedCount <= 0)
            throw NotFoundClientError(s"Job '$jobId' does not exist.")
          ()
        }
    }
  }
}
