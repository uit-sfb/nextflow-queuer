package modules

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import modules.actors.SyncActor
import modules.actors.SyncActor.{Pop, Update}
import no.uit.sfb.nqsshared.models.JobManifest
import javax.inject._
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

@Singleton
class JobQueueModule @Inject()(cfg: ConfigModule, system: ActorSystem)(
  implicit executionContext: ExecutionContext
) {

  protected val syncActor =
    system.actorOf(SyncActor.props(cfg.mdb, executionContext), "pop-actor")
  implicit protected val timeout: Timeout = 30.seconds

  def updateFn(jobId: String,
               fn: JobManifest => JobManifest): Future[JobManifest] = {
    (syncActor ? Update(jobId, fn)).mapTo[Try[JobManifest]].map {
      _.get
    }
  }

  def pop(clientId: String): Future[Option[JobManifest]] = {
    (syncActor ? Pop(clientId))
      .mapTo[Try[Option[JobManifest]]]
      .map {
        _.get
      }
  }
}
