package controllers

import io.circe.generic.extras.Configuration
import models.presigned.PresignedUrlForm
import modules.auth.Authorizer
import modules.auth.SecurityLike
import modules.{ConfigModule, PresignedModule}
import no.uit.sfb.nqsshared.com.error.BadRequestClientError
import no.uit.sfb.nqsshared.utils.Date
import org.pac4j.play.scala.SecurityComponents
import play.api.Logging
import play.api.mvc._

import java.time.ZoneOffset
import java.util.UUID
import javax.inject._
import scala.concurrent.ExecutionContext

@Singleton
class PresignedController @Inject()(
  val controllerComponents: SecurityComponents,
  val authorizer: Authorizer,
  val presignedModule: PresignedModule,
  val cm: ConfigModule,
  implicit val cfg: ConfigModule,
  implicit val ec: ExecutionContext
) extends SecurityLike
    with Logging {

  val clients = authorizer.apiClients

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  def createPresignedUrl(jobId: String,
                         `type`: String,
                         key: List[String],
                         timeout: Int, //Duration in s
                         instances: Int) = secure.async {
    implicit request: Request[_] =>
      val allowedTypes = Seq("inputs", "outputs", "reports")
      if (!allowedTypes.contains(`type`))
        throw BadRequestClientError(
          s"Type must be one of: ${allowedTypes.mkString(", ")}"
        )
      val id = UUID.randomUUID().toString
      val puf = PresignedUrlForm(
        id,
        jobId,
        `type`,
        key,
        if (timeout <= 0)
          Long.MaxValue
        else
          Date.now().plusSeconds(timeout).toEpochSecond(ZoneOffset.UTC),
        if (instances <= 0)
          Int.MaxValue
        else
          instances
      )
      val scheme = request.headers.get("X-Forwarded-Proto").getOrElse("http")
      presignedModule
        .generatePresignedUrl(puf, s"$scheme://${request.host}${cm.prefixPath}")
        .map {
          Ok(_)
        }
  }

  //Do not protect!
  def consumePresignedUrl(ref: String, inline: Boolean) = Action.async {
    implicit request: Request[_] =>
      presignedModule.consumePresignedUrl(ref).map {
        case Left(p) =>
          Ok.sendFile(content = p.toFile, inline = inline)
        case Right((jobId, t, source)) =>
          Ok.streamed(
            content = source,
            None,
            inline = false,
            Some(s"${jobId}_$t.tgz")
          )
      }
  }

  def deletePresignedUrl(ref: String) = secure.async {
    implicit request: Request[_] =>
      presignedModule.deletePresignedUrl(ref).map { _ =>
        NoContent
      }
  }
}
