package controllers

import modules.{FilesModule, JobModule, JobQueueModule}
import no.uit.sfb.nqsshared.jsonapi.{Data, MultipleResponse, SingleResponse}
import no.uit.sfb.nqsshared.models.JobManifest
import play.api.libs.concurrent.Futures
import play.api.mvc._
import play.api.Logging
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import io.circe.generic.extras.auto._
import akka.util.Timeout
import models.json.JsonApi._
import org.mongodb.scala.bson.Document
import no.uit.sfb.nqsshared.utils.FileUtils
import play.api.libs.Files.{TemporaryFile, TemporaryFileCreator}

import java.io.File
import java.nio.file.{Files, Path}
import javax.inject._
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

@Singleton
class ExecutorController @Inject()(cc: ControllerComponents,
                                   jobQueueModule: JobQueueModule,
                                   jobModule: JobModule,
                                   filesModule: FilesModule,
                                   implicit val f: Futures,
                                   implicit val ec: ExecutionContext)
    extends AbstractController(cc)
    with Logging {

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  implicit val timeout: Timeout = 5.seconds
  val collName = "jobs"

  def pop(clientId: String) = Action.async {
    implicit request: Request[AnyContent] =>
      jobQueueModule
        .pop(clientId)
        .map { newJm =>
          val resp = SingleResponse[JobManifest](newJm.map { jm =>
            Data(collName, jm._id, jm)
          })
          Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
        }
  }

  def updateState(jobId: String, state: String) = Action.async {
    implicit request: Request[AnyContent] =>
      if (state == "deleted") {
        jobModule.deleteJob(jobId).map { _ =>
          NoContent
        }
      } else {
        //It is perfectly ok to get error messages in the log when a task is canceled and the executor still reports progress advancement
        jobModule.checkJobState(
          jobId,
          Seq("pending", "initializing", "running", "finalizing")
        )
        jobModule.updateState(jobId, state).map { jm =>
          val resp =
            SingleResponse[JobManifest](Some(Data(collName, jm.jobId, jm)))
          Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
        }
      }
  }

  def saveOutputFile(jobId: String, key: String) = {
    lazy val ntf = new TemporaryFile {
      def path: Path = null;

      def file: File = null;

      def temporaryFileCreator: TemporaryFileCreator = null
    }

    Try {
      jobModule.checkJobState(jobId, "finalizing")
    } match {
      case Success(_) =>
        Action.async(parse.temporaryFile) { implicit request =>
          filesModule.uploadOutputFile(jobId, key).map {
            case (jm, dst) =>
              Files.createDirectories(dst.getParent)
              request.body.atomicMoveWithFallback(dst) //Replace existing files
              val resp =
                SingleResponse[JobManifest](Some(Data(collName, jm.jobId, jm)))
              Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
          }
        }
      case Failure(e) =>
        //The parser cannot be parse.empty as it should be of type File.
        //So we just use a random file
        Action.async(parse.ignore(ntf)) { implicit request =>
          throw e
        }
    }
  }

  //Use in proximity mode
  def registerOutput(jobId: String, key: String) = {
    Try {
      jobModule.checkJobState(jobId, "finalizing")
    } match {
      case Success(_) =>
        Action.async { implicit request =>
          filesModule.uploadOutputFile(jobId, key).map {
            case (jm, dst) =>
              if (FileUtils.isSymbolicLink(dst)) {
                val realPath = dst.toRealPath()
                Files.delete(dst)
                Files.createLink(dst, realPath)
              }
              val resp =
                SingleResponse[JobManifest](Some(Data(collName, jm.jobId, jm)))
              Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
          }
        }
      case Failure(e) =>
        //The parser cannot be parse.empty as it should be of type File.
        //So we just use a random file
        Action.async { implicit request =>
          throw e
        }
    }
  }

  def retrieveInputFile(jobName: String, key: String) = Action {
    implicit request: Request[AnyContent] =>
      val dst = filesModule.downloadInputFile(jobName, key)
      Ok.sendFile(content = dst.toFile, inline = false)
  }

  def isCancelled(jobId: String) = Action.async {
    implicit request: Request[AnyContent] =>
      jobModule.isCanceled(jobId).map { b =>
        Ok(b.toString)
      }
  }

  def listDeleting(clientId: String) = Action.async {
    implicit request: Request[AnyContent] =>
      jobModule.listDeleting(clientId).map { all =>
        val resp =
          MultipleResponse[Document](all.map { doc =>
            Data(collName, doc.getString("_id"), doc)
          })
        Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
      }
  }

  def detectOrphans(clientId: String) = Action.async {
    implicit request: Request[AnyContent] =>
      jobModule.detectOrphans(clientId).map { _ =>
        NoContent
      }
  }
}
