package controllers

import modules.ConfigModule
import modules.auth.Authorizer
import modules.auth.SecurityLike
import org.pac4j.play.scala.SecurityComponents
import play.api.Logging
import play.api.mvc._

import javax.inject._

@Singleton
class WidgetsController @Inject()(val controllerComponents: SecurityComponents,
                                  val authorizer: Authorizer,
                                  implicit val cfg: ConfigModule)
    extends SecurityLike
    with Logging {

  val clients = authorizer.uiClients

  def nqsWidget(sec: Boolean) = {
    (if (sec)
       secure
     else
       open) { implicit request: Request[AnyContent] =>
      Ok(views.html.NqsWidget(cfg.selfEndpoint, authorizer.user.flatMap {
        _.accessToken
      }))
    }
  }
}
