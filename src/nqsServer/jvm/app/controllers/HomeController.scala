package controllers

import modules.auth.Authorizer
import modules.auth.SecurityLike

import javax.inject._
import modules.{ConfigModule, MongodbClient}
import play.api.libs.json.Json
import play.api.Logging
import play.api.mvc._
import no.uit.sfb.info.nqsserver.BuildInfo
import no.uit.sfb.nqsshared.models.JobManifest
import no.uit.sfb.nqsshared.models.auth.UserInfo
import no.uit.sfb.nqsshared.models.generic.Info
import no.uit.sfb.nqsshared.utils.FileUtils
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures

import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration.DurationInt
import scala.util.Try

@Singleton
class HomeController @Inject()(val controllerComponents: SecurityComponents,
                               mongoClient: MongodbClient,
                               implicit val authorizer: Authorizer,
                               implicit val f: Futures,
                               implicit val cfg: ConfigModule,
                               implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  val clients = authorizer.clients

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(BuildInfo.version))
  }

  def info() = Action {
    val conn =
      Try(Await.result(mongoClient.listDbs(), 45.seconds)).toOption match {
        case Some(_) => true
        case None    => false
      }
    val res = Info(BuildInfo.name, BuildInfo.version, BuildInfo.gitCommit, conn)
    implicit val writer = Json.writes[Info]
    Ok(Json.toJson(res))
  }

  def identity() = secure { implicit request: Request[AnyContent] =>
    Ok(views.html.identity())
  }

  def userInfo(): Action[AnyContent] =
    open { implicit request: Request[AnyContent] =>
      implicit val writer = Json.writes[UserInfo]
      Ok(Json.toJson(authorizer.user match {
        case Some(user) =>
          UserInfo(user._id, user.email)
        case None =>
          UserInfo("", role = "anonymous")
      }))
    }

  //Do NOT use secure!
  def connected(): Action[AnyContent] = open {
    implicit request: Request[AnyContent] =>
      {
        Ok(authorizer.isLoggedIn.toString)
      }
  }

  def isSharedVolume(key: String) = Action {
    implicit request: Request[AnyContent] =>
      val p = cfg.workdir.resolve(JobManifest("proximity").filePath(key))
      Ok(FileUtils.exists(p).toString)
  }

  def api() = Action { implicit request: Request[AnyContent] =>
    logger.info("Redirecting to swagger generated API")

    Redirect(
      routes.Assets.at(s"index.html?url=${cfg.prefixPath}/assets/swagger.json")
    )
  }
}
