package controllers

import akka.stream.Materializer
import io.circe.generic.extras.Configuration
import io.circe.syntax._
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import models.json.JsonApi._
import modules.auth.Authorizer
import modules.auth.SecurityLike
import modules.{ConfigModule, FilesModule, JobModule}
import no.uit.sfb.nqsshared.com.error.BadRequestClientError
import no.uit.sfb.nqsshared.jsonapi.{Data, MultipleResponse, SingleResponse}
import no.uit.sfb.nqsshared.models.JobManifest
import org.mongodb.scala.bson.Document
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures
import play.api.mvc._
import play.api.Logging

import javax.inject._
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class JobController @Inject()(val controllerComponents: SecurityComponents,
                              jobModule: JobModule,
                              filesModule: FilesModule,
                              val authorizer: Authorizer,
                              implicit val f: Futures,
                              implicit val cfg: ConfigModule,
                              implicit val ec: ExecutionContext,
                              implicit val mat: Materializer)
    extends SecurityLike
    with Logging {

  val clients = authorizer.apiClients

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  val collName = "jobs"
  val coll = cfg.mdb.collection[Document](collName)

  def list(jobId: Option[String], state: Option[String]) = secure.async {
    implicit request: Request[AnyContent] =>
      jobModule
        .list(jobId, state, authorizer.user.map { _._id })
        .map { docs =>
          if (jobId.nonEmpty) {
            SingleResponse[Document](docs.headOption.map { jm =>
              Data(collName, jm.getString("_id"), jm)
            }).asJson.noSpaces
          } else {
            MultipleResponse[Document](docs.map { jm =>
              Data(collName, jm.getString("_id"), jm)
            }).asJson.noSpaces
          }
        }
        .map { resp =>
          Ok(resp).as("application/vnd.api+json")
        }
  }

  //Keys should be without prefix
  def downloadFiles(jobId: String,
                    key: List[String],
                    `type`: String,
                    inline: Boolean) = secure {
    implicit request: Request[AnyContent] =>
      val allowedTypes = Seq("inputs", "outputs", "reports")
      if (!allowedTypes.contains(`type`))
        throw BadRequestClientError(
          s"Type must be one of: ${allowedTypes.mkString(", ")}"
        )
      val keys =
        if (key.isEmpty)
          Seq(s"${`type`}/*")
        else {
          key.map { k =>
            s"${`type`}/$k"
          }
        }
      if (keys.size == 1 && !keys.head.endsWith("*")) {
        val dst = filesModule.downloadSingleFile(jobId, keys.head)
        Ok.sendFile(content = dst.toFile, inline = inline)
      } else {
        val source = filesModule.downloadFiles(jobId, keys)
        Ok.streamed(
          content = source,
          None,
          inline = false,
          Some(s"${jobId}_${`type`}.tgz")
        )
      }
  }

  protected def changeState(jobId: String, newState: String): Future[Result] = {
    jobModule.updateState(jobId, newState).map { jm =>
      val resp =
        SingleResponse[JobManifest](Some(Data(collName, jm.jobId, jm)))
      Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
    }
  }

  def editJob(jobId: String) = secure.async { implicit request =>
    jobModule.checkJobState(jobId, Seq("failed", "canceled"))
    changeState(jobId, "draft")
  }

  def retryJob(jobId: String) = secure.async { implicit request =>
    jobModule.checkJobState(jobId, Seq("failed", "canceled"))
    changeState(jobId, "pending")
  }

  def cancelJob(jobId: String) = secure.async { implicit request =>
    jobModule.checkJobState(
      jobId,
      Seq("pending", "initializing", "running", "finalizing")
    )
    changeState(jobId, "canceled")
  }

  def deleteJob(jobId: String) = secure.async { implicit request =>
    jobModule
      .get(jobId)
      .map {
        _.flatMap { doc =>
          decode[JobManifest](doc.toJson()).toOption
        }
      }
      .flatMap {
        case Some(jm) if jm.hasBeenPulled =>
          jobModule.markDelete(jobId)
        case _ =>
          jobModule.deleteJob(jobId)
      }
      .map { _ =>
        NoContent
      }
  }
}
