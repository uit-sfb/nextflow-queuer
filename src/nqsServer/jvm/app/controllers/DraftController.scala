package controllers

import akka.stream.Materializer
import akka.stream.scaladsl.FileIO
import io.circe.generic.extras.Configuration
import io.circe.generic.extras.auto._
import io.circe.parser.decode
import io.circe.syntax._
import models.json.JsonApi._
import modules.auth.Authorizer
import modules.auth.SecurityLike
import modules.{ConfigModule, FilesModule, JobModule, UserModule}
import no.uit.sfb.nqsshared.com.error.{
  BadRequestClientError,
  ForbiddenClientError,
  NotFoundClientError
}
import no.uit.sfb.nqsshared.jsonapi.{Data, MultipleResponse, SingleResponse}
import no.uit.sfb.nqsshared.models.{JobManifest, UrlHandle}
import no.uit.sfb.nqsshared.utils.FileUtils
import org.mongodb.scala.bson.{BsonString, Document}
import org.pac4j.play.scala.SecurityComponents
import play.api.libs.concurrent.Futures
import play.api.libs.ws.WSClient
import play.api.mvc._
import play.api.Logging
import play.api.libs.Files.TemporaryFile

import java.nio.file.Files
import javax.inject._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

@Singleton
class DraftController @Inject()(val controllerComponents: SecurityComponents,
                                jobModule: JobModule,
                                filesModule: FilesModule,
                                userModule: UserModule,
                                ws: WSClient,
                                val authorizer: Authorizer,
                                implicit val mat: Materializer,
                                implicit val f: Futures,
                                implicit val cfg: ConfigModule,
                                implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  val clients = authorizer.apiClients

  implicit val customConfig: Configuration = Configuration.default.withDefaults

  val workdir = cfg.workdir
  val collName = "jobs"

  def listDraft(jobId: Option[String]) = secure.async {
    implicit request: Request[AnyContent] =>
      jobModule
        .list(jobId, Some("draft"), authorizer.user.map { _._id })
        .map { docs =>
          val resp = MultipleResponse[Document](docs.map { doc =>
            Data(collName, doc.getString("_id"), doc)
          })
          Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
        }
  }

  def newDraft(project: String) = secure.async {
    implicit request: Request[AnyContent] =>
      val submittedFrom = request.headers.get("origin").getOrElse("")
      val upsertUser = authorizer.user match {
        case Some(user) =>
          userModule.upsert(user)
        case None =>
          Future.successful(())
      }
      upsertUser.flatMap { _ =>
        val userId = authorizer.user
          .map {
            _._id
          }
          .getOrElse("anonymous")
        jobModule
          .newDraft(project, submittedFrom, userId)
          .map { doc =>
            val resp = SingleResponse[Document](
              Some(Data(collName, doc.getString("_id"), doc))
            )
            Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
          }
      }
  }

  def newDraftFromCopy(jobId: String, deleteOrig: Boolean) = secure.async {
    implicit request: Request[AnyContent] =>
      val upsertUser = authorizer.user match {
        case Some(user) =>
          userModule.upsert(user)
        case None =>
          Future.successful(())
      }
      upsertUser.flatMap { _ =>
        val userId = authorizer.user
          .map {
            _._id
          }
          .getOrElse("anonymous")
        jobModule
          .newDraftFromCopy(jobId, userId, deleteOrig)
          .map { doc =>
            val resp = SingleResponse[Document](
              Some(
                Data(
                  collName,
                  doc
                    .get[BsonString]("_id")
                    .getOrElse(BsonString(""))
                    .getValue,
                  doc
                )
              )
            )
            Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
          }
      }
  }

  def updateDraftSpec(jobId: String) = secure.async {
    implicit request: Request[AnyContent] =>
      jobModule
        .updateDraftSpec(
          jobId,
          Document(request.body.asJson.map { _.toString() }.getOrElse(""))
        )
        .map { jm =>
          val resp =
            SingleResponse[JobManifest](Some(Data(collName, jm.jobId, jm)))
          Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
        }
  }

  //Checks that the job does belong to the requesting user
  //Note: the body parser will be applied before the check takes place.
  def preAllow[A](jobId: String) = { implicit request: Request[A] =>
    val oId = authorizer.user(request).map {
      _._id
    }
    val allowDiskUsage =
      if (cfg.maxDiskUsagePerUser >= 0)
        jobModule
          .list(None, None, userId = oId)
          .map { docs =>
            val dirs = docs
              .map { _.getString("_id") }
              .map { jobId =>
                workdir.resolve(JobManifest(jobId).inputsPath)
              }
            val allFiles = dirs.flatMap { d =>
              if (FileUtils.exists(d))
                FileUtils.filesUnder(d, true)
              else
                Seq()
            }
            val diskUsage = allFiles.foldLeft(0L) {
              case (acc, f) =>
                acc + Files.size(f)
            }
            if (cfg.maxDiskUsagePerUser < diskUsage) {
              throw ForbiddenClientError(
                s"Your disk quota of '${cfg.maxDiskUsagePerUserGb} Gb' is exceeded. Please delete some unused jobs before creating a new one."
              )
            }
          } else
        Future.successful(())
    allowDiskUsage.flatMap { _ =>
      jobModule.belongsTo(jobId, oId).map { allowed =>
        if (!allowed)
          throw ForbiddenClientError(
            s"User ${oId.getOrElse("anonymous")} does not own job $jobId."
          )
        else
          jobModule.checkJobState(jobId, "draft")
      }
    }
  }

  def uploadInputFile(jobId: String, key: String) = {
    logger.info(s"Received upload request for job '$jobId' (key: '$key')")
    secure.async(parse.temporaryFile(cfg.maxDiskUsagePerUser)) {
      implicit request: Request[TemporaryFile] =>
        preAllow(jobId)(request)
          .flatMap { _ =>
            filesModule.uploadInputFile(jobId, key).map {
              case (jm, dst) =>
                Files.createDirectories(dst.getParent)
                request.body.atomicMoveWithFallback(dst)
                val resp =
                  SingleResponse[JobManifest](
                    Some(Data(collName, jm.jobId, jm))
                  )
                Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
            }
          }
          .recover { e =>
            request.body.delete()
            throw e
          }
    }
  }

  def addInputFromUrl(jobId: String, key: String) = secure.async {
    implicit request: Request[AnyContent] =>
      Try { jobModule.checkJobState(jobId, "draft") } match {
        case Success(_) =>
          request.body.asJson.flatMap { js =>
            decode[UrlHandle](js.toString()).toOption
          } match {
            case Some(handle) =>
              val tmpDst = workdir.resolve(JobManifest(jobId).tmpFilePath(key))
              Files.createDirectories(tmpDst.getParent)
              val sink = FileIO.toPath(tmpDst)
              val tmpReq = ws.url(handle.url)
              (handle.bearerToken match {
                case Some(bt) if bt.nonEmpty =>
                  tmpReq.addHttpHeaders(
                    "Authorization" -> s"Bearer ${handle.bearerToken}"
                  )
                case _ =>
                  tmpReq
              }).withMethod("GET")
                .stream()
                .flatMap { response =>
                  response.bodyAsSource
                    .runWith(sink)
                }
              filesModule.uploadInputFile(jobId, key, Some(handle.url)).map {
                case (jm, dst) =>
                  Files.createDirectories(dst.getParent)
                  Files.move(tmpDst, dst)
                  val resp =
                    SingleResponse[JobManifest](
                      Some(Data(collName, jm.jobId, jm))
                    )
                  Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
              }
            case None =>
              throw BadRequestClientError(s"Could not parse UrlHandle.")
          }
        case Failure(e) =>
          //The parser cannot be parse.empty as it should be of type File.
          //So we just use a random file
          throw e
      }
  }

  def deleteInputFile(jobId: String, key: String) = secure.async {
    implicit request: Request[AnyContent] =>
      Try {
        jobModule.checkJobState(jobId, "draft")
      }.map { _ =>
        filesModule.deleteInputFile(jobId, key).map { jm =>
          val resp =
            SingleResponse[JobManifest](Some(Data(collName, jm.jobId, jm)))
          Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
        }
      }.get
  }

  def submit(jobId: String) = secure.async {
    implicit request: Request[AnyContent] =>
      jobModule
        .submit(jobId)
        .map { jm =>
          val resp =
            SingleResponse[JobManifest](Some(Data(collName, jm._id, jm)))
          Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
        }
  }

  def deleteDraft(jobId: String) = secure.async { implicit request =>
    jobModule
      .list(Some(jobId), Some("draft"), authorizer.user.map(_._id))
      .flatMap { docs =>
        if (docs.isEmpty)
          throw NotFoundClientError(s"Draft '$jobId' does not exist.")
        else
          jobModule.deleteDraft(jobId).map { _ =>
            NoContent
          }
      }
  }
}
