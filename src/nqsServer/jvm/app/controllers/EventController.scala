package controllers

import modules.EventModule
import no.uit.sfb.nqsshared.jsonapi.{Data, MultipleResponse}
import play.api.libs.concurrent.Futures
import play.api.mvc._
import play.api.Logging
import org.mongodb.scala.bson.Document

import javax.inject._
import scala.concurrent.ExecutionContext
import models.json.JsonApi._
import io.circe.syntax._
import io.circe.generic.auto._
import modules.auth.Authorizer
import modules.auth.SecurityLike
import org.pac4j.play.scala.SecurityComponents

@Singleton
class EventController @Inject()(val controllerComponents: SecurityComponents,
                                eventModule: EventModule,
                                val authorizer: Authorizer,
                                implicit val f: Futures,
                                implicit val ec: ExecutionContext)
    extends SecurityLike
    with Logging {

  val clients = authorizer.apiClients

  def list(jobId: String, runName: Option[String]) = secure.async {
    implicit request: Request[AnyContent] =>
      eventModule.list(jobId, runName).map { docs =>
        val resp = MultipleResponse[Document](docs.map { doc =>
          Data(eventModule.collName, doc.getString("_id"), doc)
        })
        Ok(resp.asJson.noSpaces).as("application/vnd.api+json")
      }
  }

  def push() = Action.async { implicit request: Request[AnyContent] =>
    eventModule.push(request.body.asJson).map { _ =>
      NoContent
    }
  }
}
