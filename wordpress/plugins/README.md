# Wordpress plugins

## How to load a plugin to Wordpress?

- Download the
  plugin: `curl https://gitlab.com/api/v4/projects/28943833/packages/generic/nqs_wordpress/<version>/nqs_wordpress.zip --output nqs_wordpress.zip`
  , where `<version>` is one of the versions listed [here](https://gitlab.com/uit-sfb/nextflow-queuer/-/releases)
- Log in to Wordpress and go to `Plugins` -> `Add New` -> `Upload Plugin` -> `Install Now`
- Select `nqs_wordpress.zip`
- Click on `Activate`

## Troubleshooting

If the URL are not resolved properly, it might be that the rules defined by the plugin were not picked up by Wordpress.
To fix this, go to `Settings` -> `Permalinks` then click on `Save` without doing any modifications.