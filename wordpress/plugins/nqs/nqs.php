<?php
/**
 * Plugin Name: NQS
 * Plugin URI: https://gitlab.com/uit-sfb/nqs
 * Description: Add shortcodes to display NQS widgets as well as setting up an API route.
 * Version: 1.0
 **/

include(plugin_dir_path(__FILE__) . 'settings.php');

function nqs_add_scripts() {
    global $wpdb;
    $arr = $wpdb->get_results('SELECT ID FROM ' . $wpdb->prefix . 'posts WHERE post_content LIKE "%[NqsJobs]%" AND post_parent = 0');
    $fn = function($n) { return $n->ID; };
    $id_list = array_map($fn, $arr);
    $page_id = get_the_ID();
    wp_register_style('nqs_stylesheet', plugin_dir_url(__FILE__) . 'css/main.css');
    wp_register_style('nqs_stylesheet_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css');
    if (in_array($page_id, $id_list)) {
        wp_enqueue_style('nqs_stylesheet');
        wp_enqueue_style('nqs_stylesheet_bootstrap');
        wp_enqueue_script('nqs_bundle', plugin_dir_url(__FILE__) . 'js/bundle.js');
    }
}

add_action( 'wp_enqueue_scripts', 'nqs_add_scripts' );


function NqsJobs($atts = [], $content = null, $tag = '')
{
    $nqs_options = get_option('nqs_options'); // Array of All Options
    $endpoint = $nqs_options['nqs_server_url'];
    $config_override = $nqs_options['nqs_pipeline_config'];
    $user_id = get_current_user_id();
    $user_info = get_userdata($user_id);
    $user_login = $user_info->user_login;
    $user_email = $user_info->user_email;
    $access_token = get_user_meta($user_id, 'openid-connect-generic-last-token-response', true)["access_token"];
    $project = "default";
    $openid_settings = get_option('openid_connect_generic_settings', array());
    $openid_end_session = $openid_settings['endpoint_end_session'];
    $openid_callback_url = do_shortcode('[openid_connect_generic_auth_url]');
    return <<<EOD
    <div id="entrypoint"></div>
    <script>
        new NqsWidget("$openid_callback_url", "$openid_end_session").renderInto("entrypoint", "$project", `$config_override`, "$endpoint", "$access_token", "$user_login", "$user_email")
    </script>
EOD;
}

add_shortcode('NqsJobs', 'NqsJobs');
