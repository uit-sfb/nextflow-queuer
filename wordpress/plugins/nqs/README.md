# Nextflow queuing Widget

Add shortcodes to display NQS widgets.

## Getting started

- Install [OpenID Connect Generic Client](https://wordpress.org/plugins/daggerhart-openid-connect-generic/) plugin and configure it via Settings/OpenID Connect Client.
- Install and activate the plugin (see [here](../README.md)).
- Set up the plugin: Go to NQS and fill up the fields:
    - `NQS service URL`: URL of the NQS service endpoint
- Add the shortcodes described below where appropriate.

## Shortcodes

### [NqsJobs]

Display the job list widget.

#### Attributes

None

Example: `[NqsJobs]`
